# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from datetime import *
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateAction, Button, StateReport
from trytond.pool import Pool

class CreateConsultationPaludismReportEbomeStart(ModelView):
    'Patient Evaluation Report Create Start'
    __name__ = 'gnuhealth.consultation.paludism.report.create.start'
    
    start_date = fields.Date('Start Date', required=True)
    
    end_date = fields.Date('End Date', required=True)

class CreateConsultationPaludismReportEbome(Wizard):
    'Patient Evaluation Report Create'
    __name__ = 'gnuhealth.consultation.paludism.report.create'

    start = StateView('gnuhealth.consultation.paludism.report.create.start',
        'health_ebome.view_create_consultation_paludism_ebome_start', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_ebome.report_consultation_paludism_ebome_from_wizard')

    def default_start(self, fields):
        res = { }
        return res

    def get_state(self, ev):
        res = ""

        if ev.state == "signed" or ev.state == "finished":
            res = "signé"
        elif ev.state == "done":
            res = "effectué"
        elif ev.state == "in_progress":
            if ev.evaluation_summary or ev.supplementary_tests or ev.present_illness or ev.diagnosis:
                res = "effectué"
            else:
                res = "en cours"
        return res

    def get_visit_type(self, ev):
        res = ""
        if ev.visit_type == "followup":
            res = 'Suivi'
        else:
            res = 'Nouvel état de santé'
        return res

    def get_gender(self, ev):
        gender = ev.patient.gender
        res = ""
        if gender == "m":
            res = 'Homme'
        elif gender == "f":
            res = 'Femme'
        elif gender == "u":
            res = 'Inconue'
        return res

    def get_age(self, ev):
        try:
            age = int(ev.computed_age.split('y')[0])
        except:
            return 'without'
        if age <= 5:
            return 'young'
        else:
            return 'old'

    def get_pregnant(self, ev):
        pregnant = ev.pregnant
        res = ""
        if pregnant == "pregnant":
            res = 'Enceinte'
        elif pregnant == "not pregnant":
            res = 'Pas enceinte'
        else:
            if ev.patient.gender == 'm':
                res = 'Pas enceinte'
            else:
                res = 'Inconue'
        return res

    def get_tests(self, ev, current_date):
        Lab = Pool().get('gnuhealth.lab')
        Critearea = Pool().get('gnuhealth.lab.test.critearea')
        Party=Pool().get('party.party')

        tests = {'tratar' : False, 'doc_name' : ' ', 'TDR' : ' ', 'GE' : ' ', 'error' : ' ', 'detail' : ' '}

        if ev.state == "signed":
            doc_id = ev.signed_by.name.id
            tests['doc_name'] = ev.signed_by.rec_name
        else:
            if ev.write_uid:
                party, = Party.search([
                    ('internal_user.id', '=', ev.write_uid.id),
                    ])
                if party:
                    doc_id = party.id
                    tests['doc_name'] = party.rec_name
                else:
                    tests['error'] = 'no doctor'
                    tests['detail'] = str(ev.write_uid.id)
                    return tests
            else:
                doc_id = ev.healthprof.name.id
                tests['doc_name'] = ev.healthprof.rec_name

        selected_labs = Lab.search([
            ('date_analysis', '>=', ev.evaluation_start),
            ('date_analysis', '<=', current_date),
            ('patient', '=', ev.patient),
            ('test', 'in', [104, 123]),
            ])

        if selected_labs:
            for lab in selected_labs:
                if lab.requestor:
                    if lab.requestor.name.id != doc_id:
                        tests['error'] = 'unequal doctor'
                        tests['detail'] = tests['doc_name']
                        tests['doc_name'] = lab.requestor.rec_name
                else:
                    tests['error'] = 'No requestor'
                    tests['detail'] = lab.name
                cr = Critearea.search([
                    ('gnuhealth_lab_id', '=', lab.id),
                    ])
                if cr:
                    tests['tratar'] = True
                    if lab.test.id == 123:
                        if cr[0].result_text:
                            if 'neg' in cr[0].result_text\
                            or 'NEG' in cr[0].result_text\
                            or 'Neg' in cr[0].result_text\
                            or 'nég' in cr[0].result_text\
                            or 'Nég' in cr[0].result_text:
                                result_text = 'négative'
                            else:
                                result_text = cr[0].result_text
                        else:
                            result_text = 'négative'
                        tests['TDR'] = result_text
                    elif lab.test.id == 104:
                        if cr[0].result_text:
                            if 'neg' in cr[0].result_text\
                            or 'NEG' in cr[0].result_text\
                            or 'Neg' in cr[0].result_text\
                            or 'nég' in cr[0].result_text\
                            or 'Nég' in cr[0].result_text:
                                result_text = 'négative'
                            else:
                                result_text = cr[0].result_text
                        elif cr[0].remarks:
                            if 'neg' in cr[0].remarks\
                            or 'NEG' in cr[0].remarks\
                            or 'Neg' in cr[0].remarks\
                            or 'nég' in cr[0].remarks\
                            or 'Nég' in cr[0].remarks:
                                result_text = 'négative'
                            else:
                                result_text = cr[0].remarks
                        else:
                            result_text = 'négative'
                        tests['GE'] = result_text
                else:
                    tests['error'] = 'no critearea'
                    tests['detail'] = lab.name

        return tests

    def get_hospitalized(self, ev):
        res = ""
        if ev.hospitalized == True:
            res = 'Oui'
        else:
            res = 'No'
        return res


    def do_create_report(self, action):
        data = {
            'start_date': self.start.start_date.strftime("%d/%m/%Y"),
            'end_date': self.start.end_date.strftime("%d/%m/%Y"),
            'title' : "RAPPORT CONSULTATION PALUDISME"
            }

        t = self.start.start_date
        datetime_start = datetime(t.year, t.month, t.day)
        t = self.start.end_date
        datetime_end = datetime(t.year, t.month, t.day) + timedelta(days = 1)
        # Get evaluation between dates
        pool = Pool()
        Evaluation = pool.get('gnuhealth.patient.evaluation')
        selected_evaluations = Evaluation.search([
            ('evaluation_start','>=',datetime_start),
            ('evaluation_start','<',datetime_end)],
            order=[('patient', 'ASC'), ('evaluation_start', 'DESC')])

        results = {}
        errors = []
        not_doc = []

        # Extract information from evaluations
        current_patient = ' '
        for ev in selected_evaluations:
            if ev.patient != current_patient:
                current_patient = ev.patient
                current_date = datetime_end
            tests = self.get_tests(ev, current_date)
            current_date = ev.evaluation_start
            if tests['tratar'] == True:
                results[ev.code] = {'date' : 0, 'visit_type' : ' ', 'patient' : ' ', 'gender' : ' ', 'age_young' : ' ', 'age_old' : ' ', 'pregnant' : ' ', 'patient_code' : ' ', 'present_illness' : ' ', 'evaluation_summary' : ' ',
                                    'TDR' : ' ', 'GE' : ' ', 'diagnosis' : ' ', 'directions': ' ', 'hospitalized' : ' ', 'healthprof' : ' ', 'state' : ' '}
                age = self.get_age(ev)
                doc_name = tests['doc_name']
                results[ev.code]['date'] = ev.evaluation_start.strftime("%d/%m/%Y")
                results[ev.code]['visit_type'] = self.get_visit_type(ev)
                results[ev.code]['patient'] = ev.patient.rec_name
                results[ev.code]['gender'] = self.get_gender(ev)
                if age == 'young':
                    results[ev.code]['age_young'] = int(ev.computed_age.split('y')[0])
                elif age == 'old':
                    results[ev.code]['age_old'] = int(ev.computed_age.split('y')[0])
                else:
                    results[ev.code]['age_young'] = 0
                    results[ev.code]['age_old'] = 0
                results[ev.code]['pregnant'] = self.get_pregnant(ev)
                results[ev.code]['patient_code'] = ev.patient_code
                results[ev.code]['present_illness'] = ev.present_illness
                results[ev.code]['evaluation_summary'] = ev.evaluation_summary
                results[ev.code]['TDR'] = tests['TDR']
                results[ev.code]['GE'] = tests['GE']
                if ev.diagnosis:
                    results[ev.code]['diagnosis'] = ev.diagnosis.rec_name
                results[ev.code]['directions'] = ev.directions
                results[ev.code]['hospitalized'] = self.get_hospitalized(ev)
                results[ev.code]['healthprof'] = doc_name
                results[ev.code]['state'] = self.get_state(ev)
                if tests['error'] == 'unequal doctor':
                    errors.append("{0} {1} {2} {3} {4} {5}".format('Docteur évaluateur et solicitant ne sont pas le même', ev.code,ev.state, ev.evaluation_start, doc_name, tests['detail']))
                elif tests['error'] == 'no requestor':
                    errors.append("{0} {1} {2} {3} {4} {5}".format('Test laboratoire sans demandeur', ev.code,ev.state, ev.evaluation_start, doc_name, tests['detail']))
            else:
                if tests['error'] == 'no doctor':
                    errors.append("{0} {1} {2} {3} {4} {5}".format('Évaluation sans médecin assigné', ev.code,ev.state, ev.evaluation_start, ev.patient, tests['detail']))
                elif tests['error'] == 'no critearea':
                    errors.append("{0} {1} {2} {3} {4} {5}".format('Test laboratoire sans détail', ev.code,ev.state, ev.evaluation_start, ev.patient, tests['detail']))

        data['results'] = results
        data['errors'] = errors
        data['not_doc'] = not_doc
        return action, data
