from datetime import datetime, date
from trytond.model import ModelView, fields
from trytond.pyson import Eval, Or
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateAction, Button, StateReport
from trytond.transaction import Transaction
from pprint import pprint
from decimal import Decimal

class CreatePricesPerTarifTypeStart(ModelView):
    'Prices Per Tarif Type Create Start'
    __name__ = 'prices.per.tarif.type.start'

    tarif_type = fields.Selection([
         (None, ''),
         ('individuals', 'Particulars'),
         ('insurances', 'Insurances'),
         ('visits', 'Systematic visits'),
        ], 'Tarif Type', sort=False, required=True)

    report_type = fields.Selection([
        (None, ''),
        ('allcat', 'All Categories'),
        ('category', 'By Category')
        ], 'Report Type', sort=False, required=True)

    category = fields.Many2One('product.category', 'Category', help="Category to generate its prices report.",
                                domain=[('accounting', '=', True), ('code', '!=', '30')],
                                states={
                                    'required': Eval('report_type') == 'category',
                                    'invisible': Eval('report_type') != 'category',
                                        }
                                )

    @classmethod
    def default_report_type(cls):
        return 'allcat'


class CreatePricesPerTarifType(Wizard):
    'Prices Per Tarif Type Create'
    __name__ = 'prices.per.tarif.type.create'

    start = StateView('prices.per.tarif.type.start',
        'health_ebome.view_create_prices_per_tarif_type', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_ebome.report_prices_per_tarif_type_from_wizard')

    def processAllCategories(self, company, tarif, catalog, data):
        results = {}

        pool = Pool()
        Products = pool.get('product.template')
        Tarif_types = pool.get('party.party')
        Plans = pool.get('gnuhealth.insurance.plan')
        Discounts = pool.get('gnuhealth.insurance.plan.product.policy')
        Categories = pool.get('product.category')

        if tarif == 'individuals':
            data['title'] = 'Liste des prix pour particuliers'
            tarif_types = Tarif_types.search([
                ('name', 'in', ['CAMEROUNAIS', 'EXPATRIES', 'Personnel HEK']),
                ('is_insurance_company', '=', True),
                ('insurance_company_type', '=', 't')])
        elif tarif == 'visits':
            data['title'] = 'Liste des prix pour visites systématiques'
            tarif_types = Tarif_types.search([
                ('name', '=', 'Visite systématique'),
                ('is_insurance_company', '=', True),
                ('insurance_company_type', '=', 'v')])
        else:
            data['title'] = 'Liste des prix pour assurances'
            tarif_types = Tarif_types.search([
                ('name', '=', 'Assurances'),
                ('is_insurance_company', '=', True),
                ('insurance_company_type', '=', 'x')])

        regimes = []
        for tarif_type in tarif_types:
            plans = Plans.search([
                ('company', '=', tarif_type.id)])
            for plan in plans:
                if tarif == 'visits':
                    tarif_name = plan.rec_name
                elif tarif_type.rec_name == 'Personnel HEK':
                    tarif_name = 'Personnel'
                else:
                    tarif_name = tarif_type.rec_name + ' ' + plan.rec_name
                regimes.append({'id' : plan.id, 'tarif': tarif_name})

        categories = Categories.search([
            ('accounting', '=', True),
            ('code', '!=', '30')],
            order=[('name', 'ASC')])

        cat_list = []
        for categori in categories:
            cat_list.append(categori.name)

        for i in range(len(cat_list)):
            selected_products = Products.search([
                ('account_category.rec_name', '=', cat_list[i]),
                (catalog, '=', True)],
                order=[('name', 'ASC')])
            for product in selected_products:
                results[product.name] = {'category' : ' ', 'prices' : []}
                results[product.name]['category'] = cat_list[i]
                for r in range(len(regimes)):
                    discounts = Discounts.search([
                        ('product.template', '=', product.id),
                        ('plan', '=', regimes[r]['id'])
                        ])
                    if discounts:
                        discount = discounts[0].discount
                        if discount == 0:
                            if tarif == 'visits':
                                results[product.name]['prices'].append({'insurance' : regimes[r]['tarif'], 'price' : int(product.list_price)})
                            else:
                                results[product.name]['prices'].append(int(product.list_price))
                        else:
                            final_price = product.list_price * (1 - Decimal(str(discount/100)))
                            final_price = company.currency.round(final_price)
                            if tarif == 'visits':
                                results[product.name]['prices'].append({'insurance' : regimes[r]['tarif'], 'price' : int(final_price)})
                            else:
                                results[product.name]['prices'].append(int(final_price))
                    else:
                        if tarif == 'visits':
                            results[product.name]['prices'].append({'insurance' : regimes[r]['tarif'], 'price' : int(product.list_price)})
                        else:
                            results[product.name]['prices'].append(int(product.list_price))

        data['regimes'] = regimes
        data['results'] = results
        data['categories'] = cat_list
        return data

    def processByCategory(self, company, tarif, catalog, category, data):
        data['title'] = 'Liste des prix de ' + str(category.rec_name) + ' pour particuliers'
        results = {}

        pool = Pool()
        Products = pool.get('product.template')
        Tarif_types = pool.get('party.party')
        Plans = pool.get('gnuhealth.insurance.plan')
        Discounts = pool.get('gnuhealth.insurance.plan.product.policy')

        if tarif == 'individuals':
            data['title'] = 'Liste des prix de ' + str(category.rec_name) + ' pour particuliers'
            tarif_types = Tarif_types.search([
                ('name', 'in', ['CAMEROUNAIS', 'EXPATRIES', 'Personnel HEK']),
                ('is_insurance_company', '=', True),
                ('insurance_company_type', '=', 't')])
        elif tarif == 'visits':
            data['title'] = 'Liste des prix de ' + str(category.rec_name) + ' pour visites systématiques'
            tarif_types = Tarif_types.search([
                ('name', '=', 'Visite systématique'),
                ('is_insurance_company', '=', True),
                ('insurance_company_type', '=', 'v')])
        else:
            data['title'] = 'Liste des prix de ' + str(category.rec_name) + ' pour assurances'
            tarif_types = Tarif_types.search([
                ('name', '=', 'Assurances'),
                ('is_insurance_company', '=', True),
                ('insurance_company_type', '=', 'x')])

        regimes = []
        for tarif_type in tarif_types:
            plans = Plans.search([
                ('company', '=', tarif_type.id)])
            for plan in plans:
                if tarif == 'visits':
                    tarif_name = plan.rec_name
                elif tarif_type.rec_name == 'Personnel HEK':
                    tarif_name = 'Personnel'
                else:
                    tarif_name = tarif_type.rec_name + ' ' + plan.rec_name
                regimes.append({'id' : plan.id, 'tarif': tarif_name})

        products = Products.search([
            ('account_category.rec_name', '=', category.rec_name),
            (catalog, '=', True)],
            order=[('name', 'ASC')])

        for product in products:
            results[product.name] = {'prices' : []}
            for r in range(len(regimes)):
                discounts = Discounts.search([
                    ('product.template', '=', product.id),
                    ('plan', '=', regimes[r]['id'])
                    ])
                if discounts:
                    discount = discounts[0].discount
                    if discount == 0:
                        if tarif == 'visits':
                            results[product.name]['prices'].append({'insurance' : regimes[r]['tarif'], 'price' : int(product.list_price)})
                        else:
                            results[product.name]['prices'].append(int(product.list_price))
                    else:
                        final_price = product.list_price * (1 - Decimal(str(discount/100)))
                        final_price = company.currency.round(final_price)
                        if tarif == 'visits':
                            results[product.name]['prices'].append({'insurance' : regimes[r]['tarif'], 'price' : int(final_price)})
                        else:
                            results[product.name]['prices'].append(int(final_price))
                else:
                    if tarif == 'visits':
                        results[product.name]['prices'].append({'insurance' : regimes[r]['tarif'], 'price' : int(product.list_price)})
                    else:
                        results[product.name]['prices'].append(int(product.list_price))

        data['regimes'] = regimes
        data['results'] = results
        return data

    def default_start(self, fields):
        res = { }
        return res

    def do_create_report(self, action):
        tarif = self.start.tarif_type

        if self.start.tarif_type == 'individuals':
            catalog = 'is_for_particular'
        elif self.start.tarif_type == 'visits':
            catalog = 'is_for_visit'
        else:
            catalog = 'is_for_insured'

        pool = Pool()
        Company = pool.get('company.company')
        Date = pool.get('ir.date')
        date_today = Date.today()

        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)

        data = {
            'date' : date_today.strftime("%d/%m/%Y"),
            'tarif' : tarif,
            'report_type' : self.start.report_type,
            }

        if self.start.report_type == 'category':
            data = self.processByCategory(company, tarif, catalog, self.start.category, data)
        elif self.start.report_type == 'allcat':
            data = self.processAllCategories(company, tarif, catalog, data)

        return action, data


class CreatePricesForInsurancesStart(ModelView):
    'Prices For Insurances Create Start'
    __name__ = 'prices.for.insurances.start'

    report_type = fields.Selection([
        (None, ''),
        ('allcat', 'All Categories'),
        ('category', 'By Category')
        ], 'Report Type', sort=False, required=True)

    category = fields.Many2One('product.category', 'Category', help="Category to generate its prices report.",
                                domain=[('accounting', '=', True), ('code', '!=', '30')],
                                states={
                                    'required': Eval('report_type') == 'category',
                                    'invisible': Eval('report_type') != 'category',
                                        }
                                )

    @classmethod
    def default_report_type(cls):
        return 'allcat'


class CreatePricesForInsurances(Wizard):
    'Prices For Insurances Create'
    __name__ = 'prices.for.insurances.create'

    start = StateView('prices.for.insurances.start',
        'health_ebome.view_create_prices_for_insurances', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_ebome.report_prices_per_tarif_type_from_wizard')

    def default_start(self, fields):
        res = { }
        return res

    def do_create_report(self, action):
        tarif = 'insurances'
        catalog = 'is_for_insured'

        pool = Pool()
        Company = pool.get('company.company')
        Date = pool.get('ir.date')
        date_today = Date.today()

        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)

        data = {
            'date' : date_today.strftime("%d/%m/%Y"),
            'tarif' : tarif,
            'report_type' : self.start.report_type,
            }

        if self.start.report_type == 'category':
            data = CreatePricesPerTarifType.processByCategory(self, company, tarif, catalog, self.start.category, data)
        elif self.start.report_type == 'allcat':
            data = CreatePricesPerTarifType.processAllCategories(self, company, tarif, catalog, data)

        return action, data
