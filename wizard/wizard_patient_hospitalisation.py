# -*- coding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView
from trytond.wizard import Wizard, StateTransition, StateAction, StateView, Button
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import PYSONEncoder
from trytond.i18n import gettext

from ..exceptions import NoPatient, NoRecordSelected


# Wizard para acción de hospitalización de un paciente desde 'Appointment'
class CreatePatientHospitalisation(Wizard):
    'Create Inpatient Hospitalisation'
    __name__ = 'wizard.gnuhealth.patient.hospitalisation'

    start_state = 'patient_hospitalisation'
    patient_hospitalisation = StateAction('health_ebome.act_app_patient_hospitalisation')

    def do_patient_hospitalisation(self, action):

        appointment = Transaction().context.get('active_id')

        try:
            app_id = \
                Pool().get('gnuhealth.appointment').browse([appointment])[0]
        except:
            raise NoRecordSelected(
                gettext('health_ebome.msg_no_record_selected'))

        patient = app_id.patient.id


        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient', '=', patient)
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': patient
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': app_id.patient.rec_name,
            }
        action['views'].reverse()

        return action, {}

# Wizard para acción de hospitalización de un paciente desde 'Evaluation'
class CreateEvalPatientHospitalisation(Wizard):
    'Create Inpatient Hospitalisation'
    __name__ = 'wizard.gnuhealth.eval.patient.hospitalisation'

    start_state = 'eval_patient_hospitalisation'
    eval_patient_hospitalisation = StateAction('health_ebome.act_eval_patient_hospitalisation')

    def do_eval_patient_hospitalisation(self, action):

        evaluation = Transaction().context.get('active_id')

        try:
            eval_id = \
                Pool().get('gnuhealth.patient.evaluation').browse([evaluation])[0]
        except:
            raise NoRecordSelected(
                gettext('health_ebome.msg_no_record_selected'))

        patient = eval_id.patient.id


        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient', '=', patient)
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': patient
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': eval_id.patient.rec_name,
            }
        action['views'].reverse()

        return action, {}
