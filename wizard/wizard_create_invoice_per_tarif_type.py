from datetime import datetime, date
from trytond.model import ModelView, fields
from trytond.pyson import Eval, Or, And
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateAction, Button, StateReport
from trytond.transaction import Transaction
from pprint import pprint
from decimal import Decimal

class CreateInvoicePerTarifTypeStart(ModelView):
    'Invoice Per Tarif Type Create Start'
    __name__ = 'invoice.per.tarif.type.start'

    start_date = fields.Date('Start Date', required=True)

    end_date = fields.Date('End Date', required=True)

    tarif_type = fields.Many2One('party.party', 'Tarif Type',
        domain=[('is_insurance_company', '=', True),
                ('insurance_company_type', 'in', ['t', 'v']),
                ('name', '!=', 'Assurance')],
        search_order=[('insurance_company_type', 'ASC'),
                      ('name', 'ASC')],
        required=True)

    plan = fields.Many2One('gnuhealth.insurance.plan', 'Plan',
        domain=[('company', '=', Eval('tarif_type'))],
        depends=['tarif_type'],
        required=True)

    report_type = fields.Selection([
        (None, ''),
        ('category', 'By Category'),
        ('products', 'By Products'),
        ('all', 'All Products')
        ], 'Report Type', sort=False, required=True)

    category = fields.Many2One('product.category', 'Category', help="Category to generate its products report.",
                                domain=[('accounting', '=', True), ('code', '!=', '30')],
                                states={
                                    'required': Eval('report_type') == 'products',
                                    'invisible': Eval('report_type') != 'products',
                                        }
                                )

    report_detail = fields.Selection([
        (None, ''),
        ('total', 'Totals'),
        ('bydates', 'By Dates'),
        ('bypatients', 'By Patients'),
        ], 'Report Detail', sort=False, required=True)

    @classmethod
    def default_start_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def default_end_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def default_report_type(cls):
        return 'category'

    @classmethod
    def default_report_detail(cls):
        return 'total'


class CreateInvoicePerTarifType(Wizard):
    'Invoice Per Tarif Type Create'
    __name__ = 'invoice.per.tarif.type.create'

    start = StateView('invoice.per.tarif.type.start',
        'health_ebome.view_create_invoice_per_tarif_type', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_ebome.report_invoice_per_tarif_type_from_wizard')

# otener el plan CAMEROUNAIS-Interne
#
    def plan_standard(self):

        pool = Pool()
        Companies = pool.get('party.party')
        Plans = pool.get('product.product')
        Products = pool.get('product.template')
        Insurance_plans = pool.get('gnuhealth.insurance.plan')
        Discounts = pool.get('gnuhealth.insurance.plan.product.policy')

        companies = Companies.search([('is_insurance_company', '=', True),
                                      ('name', '=', 'CAMEROUNAIS')])

        if companies:
            company = companies[0].id
        else:
            return 0

        insurance_plans = Insurance_plans.search([('company', '=', company)])

        if insurance_plans:
            for insurance_plan in insurance_plans:
#                plan, = Plans.search([('id', '=', insurance_plan.name)])
#                product, = Products.search([('id', '=', plan.template)])
#                if product.name == 'Interne':
                if insurance_plan.name.template.name == 'Interne':
                    return insurance_plan

# calcular el precio para camerunés de los productos de la factura
#
    def price_for_cameroonian(self, product, plan):

        pool = Pool()
        Discounts = pool.get('gnuhealth.insurance.plan.product.policy')
        Company = pool.get('company.company')

        discounts = Discounts.search([('product', '=', product.id),
                                      ('plan', '=', plan.id)])

        if discounts:
            if discounts[0].discount == 0:
                price = product.list_price
            else:
                price = product.list_price * (1 - Decimal(discounts[0].discount)/100)
        else:
            price = product.list_price

        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)

        price = company.currency.round(price)
        return price

    def processByCategoryByPatients(self, start, end, tarif, regime, data):
        data['title'] = 'Facturation crée par categorie pour le type de tarif ' + str(tarif.rec_name) + '-' + str(regime.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for categori in categories:
            cat_list.append(categori.name)

        if tarif.rec_name in ('Pygmée', 'Personnel HEK'):
            plan_standard = self.plan_standard()

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == tarif.rec_name and\
                        service[0].insurance_plan.plan_id.rec_name == regime.rec_name:
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                                and line.amount == 0:
                                                    results[first_invoice]['concepts'][i]['amount'] +=\
                                                        int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                                    results[first_invoice]['total_invoice']['amount'] +=\
                                                        int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                                else:
                                                    results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                    results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                            else:
                                current_date = invoice.invoice_date
                                current_patient = invoice.patient.rec_name
                                first_invoice = invoice.number
                                invoices_number = 1
                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                results[first_invoice]['invoices_number'] = 1
                                results[first_invoice]['invoice_date'] = invoice.invoice_date
                                for i in range(len(cat_list)):
                                    results[first_invoice]['concepts'].append({'concept' : cat_list[i], 'quantity' : 0, 'amount' : 0})
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                                and line.amount == 0:
                                                    results[first_invoice]['concepts'][i]['amount'] +=\
                                                        int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                                    results[first_invoice]['total_invoice']['amount'] +=\
                                                        int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                                else:
                                                    results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                    results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = cat_list
        data['errors'] = errors
        return data

    def processByProductsByPatients(self, start, end, tarif, regime, category, data):
        data['title'] = 'Facturation crée par produits de ' + str(category.rec_name) + ' pour le type de tarif ' + str(tarif.rec_name) + '-' + str(regime.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if product.account_category.rec_name == category.rec_name:
                    if tarif.insurance_company_type == 't'\
                    and product.is_for_particular == True:
                        prod_list.append(product.template.name)
                    elif tarif.insurance_company_type in ('i', 'c')\
                    and product.is_for_insured == True:
                        prod_list.append(product.template.name)
                    elif tarif.insurance_company_type == 'v'\
                    and product.is_for_visit == True:
                        prod_list.append(product.template.name)

        if tarif.rec_name in ('Pygmée', 'Personnel HEK'):
            plan_standard = self.plan_standard()

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == tarif.rec_name and\
                        service[0].insurance_plan.plan_id.rec_name == regime.rec_name:
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                            and line.amount == 0:
                                                results[first_invoice]['concepts'][i]['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                   int(line.quantity)))
                                                results[first_invoice]['total_invoice']['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                     int(line.quantity)))
                                            else:
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                current_patient = invoice.patient.rec_name
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' :    0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                            and line.amount == 0:
                                                results[first_invoice]['concepts'][i]['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                   int(line.quantity)))
                                                results[first_invoice]['total_invoice']['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                     int(line.quantity)))
                                            else:
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processByAllProductsByPatients(self, start, end, tarif, regime, data):
        data['title'] = 'Facturation crée par produits pour le type de tarif ' + str(tarif.rec_name) + '-' + str(regime.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if tarif.insurance_company_type == 't'\
                and product.is_for_particular == True:
                    prod_list.append(product.template.name)
                elif tarif.insurance_company_type in ('i', 'c')\
                and product.is_for_insured == True:
                    prod_list.append(product.template.name)
                elif tarif.insurance_company_type == 'v'\
                and product.is_for_visit == True:
                    prod_list.append(product.template.name)

        if tarif.rec_name in ('Pygmée', 'Personnel HEK'):
            plan_standard = self.plan_standard()

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == tarif.rec_name and\
                        service[0].insurance_plan.plan_id.rec_name == regime.rec_name:
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                            and line.amount == 0:
                                                results[first_invoice]['concepts'][i]['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                   int(line.quantity)))
                                                results[first_invoice]['total_invoice']['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                     int(line.quantity)))
                                            else:
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                current_patient = invoice.patient.rec_name
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'category' : ' ','concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                            and line.amount == 0:
                                                results[first_invoice]['concepts'][i]['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                   int(line.quantity)))
                                                results[first_invoice]['total_invoice']['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                     int(line.quantity)))
                                            else:
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processByCategoryByDates(self, start, end, tarif, regime, data):
        data['title'] = 'Facturation crée par categorie pour le type de tarif ' + str(tarif.rec_name) + '-' + str(regime.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for categori in categories:
            cat_list.append(categori.name)

        if tarif.rec_name in ('Pygmée', 'Personnel HEK'):
            plan_standard = self.plan_standard()

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == tarif.rec_name and\
                        service[0].insurance_plan.plan_id.rec_name == regime.rec_name:
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                                and line.amount == 0:
                                                    results[first_invoice]['concepts'][i]['amount'] +=\
                                                        int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                                    results[first_invoice]['total_invoice']['amount'] +=\
                                                        int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                                else:
                                                    results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                    results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                            else:
                                current_date = invoice.invoice_date
                                first_invoice = invoice.number
                                invoices_number = 1
                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                results[first_invoice]['invoices_number'] = 1
                                results[first_invoice]['invoice_date'] = invoice.invoice_date
                                for i in range(len(cat_list)):
                                    results[first_invoice]['concepts'].append({'concept' : cat_list[i], 'quantity' : 0, 'amount' : 0})
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                                and line.amount == 0:
                                                    results[first_invoice]['concepts'][i]['amount'] +=\
                                                        int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                                    results[first_invoice]['total_invoice']['amount'] +=\
                                                        int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                                else:
                                                    results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                    results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = cat_list
        data['errors'] = errors
        return data

    def processByProductsByDates(self, start, end, tarif, regime, category, data):
        data['title'] = 'Facturation crée par produits de ' + str(category.rec_name) + ' pour le type de tarif ' + str(tarif.rec_name) + '-' + str(regime.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if product.account_category.rec_name == category.rec_name:
                    if tarif.insurance_company_type == 't'\
                    and product.is_for_particular == True:
                        prod_list.append(product.template.name)
                    elif tarif.insurance_company_type in ('i', 'c')\
                    and product.is_for_insured == True:
                        prod_list.append(product.template.name)
                    elif tarif.insurance_company_type == 'v'\
                    and product.is_for_visit == True:
                        prod_list.append(product.template.name)

        if tarif.rec_name in ('Pygmée', 'Personnel HEK'):
            plan_standard = self.plan_standard()

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == tarif.rec_name and\
                        service[0].insurance_plan.plan_id.rec_name == regime.rec_name:
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                            and line.amount == 0:
                                                results[first_invoice]['concepts'][i]['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                   int(line.quantity)))
                                                results[first_invoice]['total_invoice']['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                     int(line.quantity)))
                                            else:
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                            and line.amount == 0:
                                                results[first_invoice]['concepts'][i]['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                   int(line.quantity)))
                                                results[first_invoice]['total_invoice']['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                     int(line.quantity)))
                                            else:
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processByAllProductsByDates(self, start, end, tarif, regime, data):
        data['title'] = 'Facturation crée par produits pour le type de tarif ' + str(tarif.rec_name) + '-' + str(regime.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if tarif.insurance_company_type == 't'\
                and product.is_for_particular == True:
                    prod_list.append(product.template.name)
                elif tarif.insurance_company_type in ('i', 'c')\
                and product.is_for_insured == True:
                    prod_list.append(product.template.name)
                elif tarif.insurance_company_type == 'v'\
                and product.is_for_visit == True:
                    prod_list.append(product.template.name)

        if tarif.rec_name in ('Pygmée', 'Personnel HEK'):
            plan_standard = self.plan_standard()

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == tarif.rec_name and\
                        service[0].insurance_plan.plan_id.rec_name == regime.rec_name:
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                            and line.amount == 0:
                                                results[first_invoice]['concepts'][i]['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                   int(line.quantity)))
                                                results[first_invoice]['total_invoice']['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                     int(line.quantity)))
                                            else:
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'category' : ' ','concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                            and line.amount == 0:
                                                results[first_invoice]['concepts'][i]['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                   int(line.quantity)))
                                                results[first_invoice]['total_invoice']['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                                     int(line.quantity)))
                                            else:
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processByCategoryTotals(self, start, end, tarif, regime, data):
        data['title'] = 'Facturation crée par categorie pour le type de tarif ' + str(tarif.rec_name) + '-' + str(regime.rec_name)
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        zero_dict = {'quantity' : 0, 'amount' : 0}
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for category in categories:
            cat_list.append(category.rec_name)
            results[category.rec_name] = {'amount' : 0, 'quantity' : 0}

        if tarif.rec_name in ('Pygmée', 'Personnel HEK'):
            plan_standard = self.plan_standard()

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == tarif.rec_name and\
                        service[0].insurance_plan.plan_id.rec_name == regime.rec_name:
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    if line.product.account_category:
                                        if line.product.account_category.rec_name in cat_list:
                                            results[line.product.account_category.rec_name]['quantity'] += int(line.quantity)
                                            total_quantity += line.quantity
                                            if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                            and line.amount == 0:
                                                results[line.product.account_category.rec_name]['amount'] +=\
                                                    int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                                total_amount += int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                            else:
                                                results[line.product.account_category.rec_name]['amount'] += int(line.amount)
                                                total_amount += line.amount
                                        else:
                                            errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def processByProductsTotals(self, start, end, tarif, regime, category, data):
        data['title'] = 'Facturation crée par produits de ' + str(category.rec_name) + ' pour le type de tarif ' + str(tarif.rec_name) + '-' + str(regime.rec_name)
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if product.account_category.rec_name == category.rec_name:
                    if tarif.insurance_company_type == 't'\
                    and product.is_for_particular == True:
                        results[product.template.name] = {'amount' : 0, 'quantity' : 0}
                    elif tarif.insurance_company_type in ('i', 'c')\
                    and product.is_for_insured == True:
                        results[product.template.name] = {'amount' : 0, 'quantity' : 0}
                    elif tarif.insurance_company_type == 'v'\
                    and product.is_for_visit == True:
                        results[product.template.name] = {'amount' : 0, 'quantity' : 0}

        if tarif.rec_name in ('Pygmée', 'Personnel HEK'):
            plan_standard = self.plan_standard()

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == tarif.rec_name and\
                         service[0].insurance_plan.plan_id.rec_name == regime.rec_name:
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    if line.product.account_category.rec_name == category.rec_name:
                                        results[line.product.template.name]['quantity'] += int(line.quantity)
                                        total_quantity += line.quantity
                                        if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                        and line.amount == 0:
                                            results[line.product.template.name]['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                         int(line.quantity)))
                                            total_amount += int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                        else:
                                            results[line.product.template.name]['amount'] += int(line.amount)
                                            total_amount += line.amount
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def processByAllProductsTotals(self, start, end, tarif, regime, data):
        data['title'] = 'Facturation crée par produits pour le type de tarif ' + str(tarif.rec_name) + '-' + str(regime.rec_name)
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                    if tarif.insurance_company_type == 't'\
                    and product.is_for_particular == True:
                        results[product.template.name] = {'category' : product.account_category.rec_name, 'amount' : 0, 'quantity' : 0}
                    elif tarif.insurance_company_type in ('i', 'c')\
                    and product.is_for_insured == True:
                        results[product.template.name] = {'category' : product.account_category.rec_name, 'amount' : 0, 'quantity' : 0}
                    elif tarif.insurance_company_type == 'v'\
                    and product.is_for_visit == True:
                        results[product.template.name] = {'category' : product.account_category.rec_name, 'amount' : 0, 'quantity' : 0}

        if tarif.rec_name in ('Pygmée', 'Personnel HEK'):
            plan_standard = self.plan_standard()

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == tarif.rec_name and\
                        service[0].insurance_plan.plan_id.rec_name == regime.rec_name:
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    results[line.product.template.name]['quantity'] += int(line.quantity)
                                    total_quantity += line.quantity
                                    if tarif.rec_name in ('Pygmée', 'Personnel HEK')\
                                    and line.amount == 0:
                                        results[line.product.template.name]['amount'] += int((self.price_for_cameroonian(line.product, plan_standard) *
                                                                                    int(line.quantity)))
                                        total_amount += int((self.price_for_cameroonian(line.product, plan_standard) * int(line.quantity)))
                                    else:
                                        results[line.product.template.name]['amount'] += int(line.amount)
                                        total_amount += line.amount
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def default_start(self, fields):
        res = { }
        return res

    def do_create_report(self, action):
        start = self.start.start_date
        end = self.start.end_date
        tarif = self.start.tarif_type
        regime = self.start.plan

        data = {
            'start_date' : start.strftime("%d/%m/%Y"),
            'end_date' : end.strftime("%d/%m/%Y"),
            'report_type' : self.start.report_type,
            'report_detail' : self.start.report_detail,
            }

        if self.start.report_detail == 'bydates':
            if self.start.report_type == 'category':
                data = self.processByCategoryByDates(start, end, tarif, regime, data)
            elif self.start.report_type == 'products':
                data = self.processByProductsByDates(start, end, tarif, regime, self.start.category, data)
            elif self.start.report_type == 'all':
                data = self.processByAllProductsByDates(start, end, tarif, regime, data)
        elif self.start.report_detail == 'bypatients':
            if self.start.report_type == 'category':
                data = self.processByCategoryByPatients(start, end, tarif, regime, data)
            elif self.start.report_type == 'products':
                data = self.processByProductsByPatients(start, end, tarif, regime, self.start.category, data)
            elif self.start.report_type == 'all':
                data = self.processByAllProductsByPatients(start, end, tarif, regime, data)
        elif self.start.report_detail == 'total':
            if self.start.report_type == 'category':
                data = self.processByCategoryTotals(start, end, tarif, regime, data)
            elif self.start.report_type == 'products':
                data = self.processByProductsTotals(start, end, tarif, regime, self.start.category, data)
            elif self.start.report_type == 'all':
                data = self.processByAllProductsTotals(start, end, tarif, regime, data)

        return action, data


class CreateInvoiceForInsurancesStart(ModelView):
    'Invoice For Insurances Create Start'
    __name__ = 'invoice.for.insurances.start'

    start_date = fields.Date('Start Date', required=True)

    end_date = fields.Date('End Date', required=True)

    grouping_level = fields.Selection([
        (None, ''),
        ('all_insurances', 'All insurances'),
        ('one_insurance', 'One insurance'),
        ('one_subscriber', 'One subscriber'),
        ('one_plan', 'One plan')
        ], 'Grouping level', sort=False, required=True)

    insurance = fields.Many2One('party.party', 'Insurance',
        domain=[('is_insurance_company', '=', True),
                ('insurance_company_type', 'in', ['i', 'c'])],
        search_order=[('insurance_company_type', 'DESC'),
                      ('name', 'ASC')],
        states={
            'required': Or(Eval('grouping_level') == 'one_insurance',
                           Eval('grouping_level') == 'one_plan'),
            'invisible': And(Eval('grouping_level') != 'one_insurance',
                             Eval('grouping_level') != 'one_plan')
                })

    plan = fields.Many2One('gnuhealth.insurance.plan', 'Plan',
        domain=[('company', '=', Eval('insurance'))],
        depends=['insurance'],
        states={
            'required': Eval('grouping_level') == 'one_plan',
            'invisible': Eval('grouping_level') != 'one_plan',
                })

    subscriber = fields.Many2One('party.party', 'Subscriber',
        domain=[('insurance_company_type', '=', 's')],
        states={
            'required': Eval('grouping_level') == 'one_subscriber',
            'invisible': Eval('grouping_level') != 'one_subscriber',
                })

    report_type = fields.Selection([
        (None, ''),
        ('category', 'By Category'),
        ('products', 'By Products'),
        ('all', 'All Products')
        ], 'Report Type', sort=False, required=True)

    category = fields.Many2One('product.category', 'Category', help="Category to generate its products report.",
                                domain=[('accounting', '=', True), ('code', '!=', '30')],
                                states={
                                    'required': Eval('report_type') == 'products',
                                    'invisible': Eval('report_type') != 'products',
                                        }
                                )

    report_detail = fields.Selection([
        (None, ''),
        ('total', 'Totals'),
        ('bydates', 'By Dates'),
        ('bypatients', 'By Patients'),
        ], 'Report Detail', sort=False, required=True)

    @classmethod
    def default_start_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def default_end_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def default_grouping_level(cls):
        return 'all_insurances'

    @classmethod
    def default_report_type(cls):
        return 'category'

    @classmethod
    def default_report_detail(cls):
        return 'total'

class CreateInvoiceForInsurances(Wizard):
    'Invoice For Insurances Create'
    __name__ = 'invoice.for.insurances.create'

    start = StateView('invoice.for.insurances.start',
        'health_ebome.view_create_invoice_for_insurances', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_ebome.report_invoice_for_insurances_from_wizard')

    def processOneByCategoryByPatients(self, start, end, insurance, data):
        data['title'] = "Facturation crée par categorie pour l'Assurance " + str(insurance.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for categori in categories:
            cat_list.append(categori.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == insurance.rec_name:
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                            else:
                                current_date = invoice.invoice_date
                                current_patient = invoice.patient.rec_name
                                first_invoice = invoice.number
                                invoices_number = 1
                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                results[first_invoice]['invoices_number'] = 1
                                results[first_invoice]['invoice_date'] = invoice.invoice_date
                                for i in range(len(cat_list)):
                                    results[first_invoice]['concepts'].append({'concept' : cat_list[i], 'quantity' : 0, 'amount' : 0})
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = cat_list
        data['errors'] = errors
        return data

    def processOneByProductsByPatients(self, start, end, insurance, category, data):
        data['title'] = "Facturation crée par produits de " + str(category.rec_name) + " pour l'Assurance " + str(insurance.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if product.account_category.rec_name == category.rec_name\
                and product.is_for_insured == True:
                    prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == insurance.rec_name:
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                current_patient = invoice.patient.rec_name
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' :    0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processOneByAllProductsByPatients(self, start, end, insurance, data):
        data['title'] = "Facturation crée par produits pour l'Assurance " + str(insurance.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == insurance.rec_name:
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                current_patient = invoice.patient.rec_name
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'category' : ' ','concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processOneByCategoryByDates(self, start, end, insurance, data):
        data['title'] = "Facturation crée par categorie pour l'Assurance " + str(insurance.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for categori in categories:
            cat_list.append(categori.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == insurance.rec_name:
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                            else:
                                current_date = invoice.invoice_date
                                first_invoice = invoice.number
                                invoices_number = 1
                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                results[first_invoice]['invoices_number'] = 1
                                results[first_invoice]['invoice_date'] = invoice.invoice_date
                                for i in range(len(cat_list)):
                                    results[first_invoice]['concepts'].append({'concept' : cat_list[i], 'quantity' : 0, 'amount' : 0})
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = cat_list
        data['errors'] = errors
        return data

    def processOneByProductsByDates(self, start, end, insurance, category, data):
        data['title'] = "Facturation crée par produits de " + str(category.rec_name) + " pour l'Assurance " + str(insurance.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if product.account_category.rec_name == category.rec_name\
                and product.is_for_insured == True:
                    prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == insurance.rec_name:
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processOneByAllProductsByDates(self, start, end, insurance, data):
        data['title'] = "Facturation crée par produits pour l'Assurance " + str(insurance.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == insurance.rec_name:
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'category' : ' ','concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processOneByCategoryTotals(self, start, end, insurance, data):
        data['title'] = "Facturation crée par categorie pour l'Assurance " + str(insurance.rec_name)
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        zero_dict = {'quantity' : 0, 'amount' : 0}
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for category in categories:
            cat_list.append(category.rec_name)
            results[category.rec_name] = {'amount' : 0, 'quantity' : 0}

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == insurance.rec_name:
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    if line.product.account_category:
                                        if line.product.account_category.rec_name in cat_list:
                                            results[line.product.account_category.rec_name]['quantity'] += int(line.quantity)
                                            total_quantity += line.quantity
                                            results[line.product.account_category.rec_name]['amount'] += int(line.amount)
                                            total_amount += line.amount
                                        else:
                                            errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def processOneByProductsTotals(self, start, end, insurance, category, data):
        data['title'] = "Facturation crée par produits de " + str(category.rec_name) + " pour l'Assurance " + str(insurance.rec_name)
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                if product.account_category.rec_name == category.rec_name:
                    results[product.template.name] = {'amount' : 0, 'quantity' : 0}

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == insurance.rec_name:
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    if line.product.account_category.rec_name == category.rec_name:
                                        results[line.product.template.name]['quantity'] += int(line.quantity)
                                        total_quantity += line.quantity
                                        results[line.product.template.name]['amount'] += int(line.amount)
                                        total_amount += line.amount
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def processOneByAllProductsTotals(self, start, end, insurance, data):
        data['title'] = "Facturation crée par produits pour l'Assurance " + str(insurance.rec_name)
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                results[product.template.name] = {'category' : product.account_category.rec_name, 'amount' : 0, 'quantity' : 0}

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.name == insurance.rec_name:
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    results[line.product.template.name]['quantity'] += int(line.quantity)
                                    total_quantity += line.quantity
                                    results[line.product.template.name]['amount'] += int(line.amount)
                                    total_amount += line.amount
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def processSubByCategoryByPatients(self, start, end, subscriber, data):
        data['title'] = "Facturation crée par categorie pour le Suscripteur " + str(subscriber.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for categori in categories:
            cat_list.append(categori.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan and service[0].insurance_plan.plan_id.insurance_subscriber:
                        if service[0].insurance_plan.plan_id.insurance_subscriber.name == subscriber.rec_name:
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                            else:
                                current_date = invoice.invoice_date
                                current_patient = invoice.patient.rec_name
                                first_invoice = invoice.number
                                invoices_number = 1
                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                results[first_invoice]['invoices_number'] = 1
                                results[first_invoice]['invoice_date'] = invoice.invoice_date
                                for i in range(len(cat_list)):
                                    results[first_invoice]['concepts'].append({'concept' : cat_list[i], 'quantity' : 0, 'amount' : 0})
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = cat_list
        data['errors'] = errors
        return data

    def processSubByProductsByPatients(self, start, end, subscriber, category, data):
        data['title'] = "Facturation crée par produits de " + str(category.rec_name) + " pour le Suscripteur " + str(subscriber.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if product.account_category.rec_name == category.rec_name\
                and product.is_for_insured == True:
                    prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan and service[0].insurance_plan.plan_id.insurance_subscriber:
                        if service[0].insurance_plan.plan_id.insurance_subscriber.name == subscriber.rec_name:
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                current_patient = invoice.patient.rec_name
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' :    0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processSubByAllProductsByPatients(self, start, end, subscriber, data):
        data['title'] = "Facturation crée par produits pour le Suscripteur " + str(subscriber.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan and service[0].insurance_plan.plan_id.insurance_subscriber:
                        if service[0].insurance_plan.plan_id.insurance_subscriber.name == subscriber.rec_name:
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                current_patient = invoice.patient.rec_name
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'category' : ' ','concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processSubByCategoryByDates(self, start, end, subscriber, data):
        data['title'] = "Facturation crée par categorie pour le Suscripteur " + str(subscriber.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for categori in categories:
            cat_list.append(categori.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan and service[0].insurance_plan.plan_id.insurance_subscriber:
                        if service[0].insurance_plan.plan_id.insurance_subscriber.name == subscriber.rec_name:
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                            else:
                                current_date = invoice.invoice_date
                                first_invoice = invoice.number
                                invoices_number = 1
                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                results[first_invoice]['invoices_number'] = 1
                                results[first_invoice]['invoice_date'] = invoice.invoice_date
                                for i in range(len(cat_list)):
                                    results[first_invoice]['concepts'].append({'concept' : cat_list[i], 'quantity' : 0, 'amount' : 0})
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = cat_list
        data['errors'] = errors
        return data

    def processSubByProductsByDates(self, start, end, subscriber, category, data):
        data['title'] = "Facturation crée par produits de " + str(category.rec_name) + " pour le Suscripteur " + str(subscriber.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if product.account_category.rec_name == category.rec_name\
                and product.is_for_insured == True:
                    prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan and service[0].insurance_plan.plan_id.insurance_subscriber:
                        if service[0].insurance_plan.plan_id.insurance_subscriber.name == subscriber.rec_name:
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processSubByAllProductsByDates(self, start, end, subscriber, data):
        data['title'] = "Facturation crée par produits pour le Suscripteur " + str(subscriber.rec_name)
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                     if service[0].insurance_plan and service[0].insurance_plan.plan_id.insurance_subscriber:
                       if service[0].insurance_plan.plan_id.insurance_subscriber.name == subscriber.rec_name:
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'category' : ' ','concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                     else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processSubByCategoryTotals(self, start, end, subscriber, data):
        data['title'] = "Facturation crée par categorie pour le Suscripteur " + str(subscriber.rec_name)
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        zero_dict = {'quantity' : 0, 'amount' : 0}
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for category in categories:
            cat_list.append(category.rec_name)
            results[category.rec_name] = {'amount' : 0, 'quantity' : 0}

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan and service[0].insurance_plan.plan_id.insurance_subscriber:
                        if service[0].insurance_plan.plan_id.insurance_subscriber.name == subscriber.rec_name:
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    if line.product.account_category:
                                        if line.product.account_category.rec_name in cat_list:
                                            results[line.product.account_category.rec_name]['quantity'] += int(line.quantity)
                                            total_quantity += line.quantity
                                            results[line.product.account_category.rec_name]['amount'] += int(line.amount)
                                            total_amount += line.amount
                                        else:
                                            errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def processSubByProductsTotals(self, start, end, subscriber, category, data):
        data['title'] = "Facturation crée par produits de " + str(category.rec_name) + " pour le Suscripteur " + str(subscriber.rec_name)
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                if product.account_category.rec_name == category.rec_name:
                    results[product.template.name] = {'amount' : 0, 'quantity' : 0}

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan and service[0].insurance_plan.plan_id.insurance_subscriber:
                        if service[0].insurance_plan.plan_id.insurance_subscriber.name == subscriber.rec_name:
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    if line.product.account_category.rec_name == category.rec_name:
                                        results[line.product.template.name]['quantity'] += int(line.quantity)
                                        total_quantity += line.quantity
                                        results[line.product.template.name]['amount'] += int(line.amount)
                                        total_amount += line.amount
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def processSubByAllProductsTotals(self, start, end, subscriber, data):
        data['title'] = "Facturation crée par produits pour le Suscripteur " + str(subscriber.rec_name)
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                results[product.template.name] = {'category' : product.account_category.rec_name, 'amount' : 0, 'quantity' : 0}

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan and service[0].insurance_plan.plan_id.insurance_subscriber:
                        if service[0].insurance_plan.plan_id.insurance_subscriber.name == subscriber.rec_name:
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    results[line.product.template.name]['quantity'] += int(line.quantity)
                                    total_quantity += line.quantity
                                    results[line.product.template.name]['amount'] += int(line.amount)
                                    total_amount += line.amount
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def processAllByCategoryByPatients(self, start, end, data):
        data['title'] = "Facturation crée par categorie pour les Assurances"
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for categori in categories:
            cat_list.append(categori.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.insurance_company_type in ('i', 'c'):
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                            else:
                                current_date = invoice.invoice_date
                                current_patient = invoice.patient.rec_name
                                first_invoice = invoice.number
                                invoices_number = 1
                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                results[first_invoice]['invoices_number'] = 1
                                results[first_invoice]['invoice_date'] = invoice.invoice_date
                                for i in range(len(cat_list)):
                                    results[first_invoice]['concepts'].append({'concept' : cat_list[i], 'quantity' : 0, 'amount' : 0})
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = cat_list
        data['errors'] = errors
        return data

    def processAllByProductsByPatients(self, start, end, category, data):
        data['title'] = "Facturation crée par produits de " + str(category.rec_name) + " pour les Assurances"
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if product.account_category.rec_name == category.rec_name\
                and product.is_for_insured == True:
                    prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.insurance_company_type in ('i', 'c'):
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                current_patient = invoice.patient.rec_name
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' :    0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processAllByAllProductsByPatients(self, start, end, data):
        data['title'] = "Facturation crée par produits pour les Assurances"
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        current_patient = ' '
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC'), ('party', 'ASC')])
        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.insurance_company_type in ('i', 'c'):
                            if invoice.invoice_date == current_date and\
                            invoice.patient.rec_name == current_patient:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                current_patient = invoice.patient.rec_name
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_patient' : ' ', 'invoice_patient_code' : ' ', 'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'category' : ' ','concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processAllByCategoryByDates(self, start, end, data):
        data['title'] = "Facturation crée par categorie pour les Assurances"
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for categori in categories:
            cat_list.append(categori.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.insurance_company_type in ('i', 'c'):
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                            else:
                                current_date = invoice.invoice_date
                                first_invoice = invoice.number
                                invoices_number = 1
                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                results[first_invoice]['invoices_number'] = 1
                                results[first_invoice]['invoice_date'] = invoice.invoice_date
                                for i in range(len(cat_list)):
                                    results[first_invoice]['concepts'].append({'concept' : cat_list[i], 'quantity' : 0, 'amount' : 0})
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.account_category:
                                            if line.product.account_category.rec_name in cat_list:
                                                i = cat_list.index(line.product.account_category.rec_name)
                                                results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                                results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                                results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                                results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                                            else:
                                                errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = cat_list
        data['errors'] = errors
        return data

    def processAllByProductsByDates(self, start, end, category, data):
        data['title'] = "Facturation crée par produits de " + str(category.rec_name) + " pour les Assurances"
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category:
                if product.account_category.rec_name == category.rec_name\
                and product.is_for_insured == True:
                    prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.insurance_company_type in ('i', 'c'):
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processAllByAllProductsByDates(self, start, end, data):
        data['title'] = "Facturation crée par produits pour les Assurances"
        results = {}
        errors = []

        zero_dict = {'quantity' : 0, 'amount' : 0}
        current_date = date(2000, 1, 1)
        first_invoice = 0
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        prod_list = []
        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                prod_list.append(product.template.name)

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])],
            order=[('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.insurance_company_type in ('i', 'c'):
                            if invoice.invoice_date == current_date:
                                results[first_invoice]['invoices_number'] += 1
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                            else:
                                invoices_number = 0
                                for line in invoice.lines:
                                    if line.type == 'line' and line.product:
                                        if line.product.template.name in prod_list:
                                            if invoices_number == 0:
                                                invoices_number = 1
                                                current_date = invoice.invoice_date
                                                first_invoice = invoice.number
                                                results[first_invoice] = {'invoice_date' : 0, 'invoices_number' : 0, 'total_invoice' : zero_dict.copy(), 'concepts' : []}
                                                for i in range(len(prod_list)):
                                                    results[first_invoice]['concepts'].append({'category' : ' ','concept' : prod_list[i], 'quantity' : 0, 'amount' : 0})
                                                results[first_invoice]['invoice_patient'] = invoice.patient.rec_name
                                                results[first_invoice]['invoice_patient_code'] = invoice.patient_code
                                                results[first_invoice]['invoices_number'] = 1
                                                results[first_invoice]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                                            i = prod_list.index(line.product.template.name)
                                            results[first_invoice]['concepts'][i]['category'] = line.product.account_category.rec_name
                                            results[first_invoice]['concepts'][i]['quantity'] += int(line.quantity)
                                            results[first_invoice]['total_invoice']['quantity'] += int(line.quantity)
                                            results[first_invoice]['concepts'][i]['amount'] += int(line.amount)
                                            results[first_invoice]['total_invoice']['amount'] += int(line.amount)
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['concepts'] = prod_list
        data['errors'] = errors
        return data

    def processAllByCategoryTotals(self, start, end, data):
        data['title'] = "Facturation crée par categorie pour les Assurances"
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        zero_dict = {'quantity' : 0, 'amount' : 0}
        invoices_number = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Categories = pool.get('product.category')

        categories = Categories.search([('accounting', '=', True),
                                        ('code', '!=', '30')])
        cat_list = []
        for category in categories:
            cat_list.append(category.rec_name)
            results[category.rec_name] = {'amount' : 0, 'quantity' : 0}

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.insurance_company_type in ('i', 'c'):
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    if line.product.account_category:
                                        if line.product.account_category.rec_name in cat_list:
                                            results[line.product.account_category.rec_name]['quantity'] += int(line.quantity)
                                            total_quantity += line.quantity
                                            results[line.product.account_category.rec_name]['amount'] += int(line.amount)
                                            total_amount += line.amount
                                        else:
                                            errors.append('Catégorie non couverte: ' + str(line.product.account_category.rec_name))
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def processAllByProductsTotals(self, start, end, category, data):
        data['title'] = "Facturation crée par produits de " + str(category.rec_name) + " pour les Assurances"
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                if product.account_category.rec_name == category.rec_name:
                    results[product.template.name] = {'amount' : 0, 'quantity' : 0}

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.insurance_company_type in ('i', 'c'):
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    if line.product.account_category.rec_name == category.rec_name:
                                        results[line.product.template.name]['quantity'] += int(line.quantity)
                                        total_quantity += line.quantity
                                        results[line.product.template.name]['amount'] += int(line.amount)
                                        total_amount += line.amount
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def processAllByAllProductsTotals(self, start, end, data):
        data['title'] = "Facturation crée par produits pour les Assurances"
        results = {}
        errors = []
        total_quantity = 0
        total_amount = 0

        pool = Pool()
        Services = pool.get('gnuhealth.health_service')
        Invoices = pool.get('account.invoice')
        Products = pool.get('product.product')

        products = Products.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ])
        for product in products:
            if product.account_category\
            and product.is_for_insured == True:
                results[product.template.name] = {'category' : product.account_category.rec_name, 'amount' : 0, 'quantity' : 0}

        selected_invoices = Invoices.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state', 'in', ['posted', 'paid'])])

        for invoice in selected_invoices:
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        if service[0].insurance_plan.company.insurance_company_type in ('i', 'c'):
                            for line in invoice.lines:
                                if line.type == 'line' and line.product:
                                    results[line.product.template.name]['quantity'] += int(line.quantity)
                                    total_quantity += line.quantity
                                    results[line.product.template.name]['amount'] += int(line.amount)
                                    total_amount += line.amount
                    else:
                        errors.append('Service sans type de tarif: ' + str(service[0].rec_name))
                else:
                    errors.append('Facture sans service associé: ' + str(invoice.reference))
            else:
                errors.append('Facture sans reference: ' + str(invoice.rec_name))

        data['results'] = results
        data['errors'] = errors
        data['total_quantity'] = int(total_quantity)
        data['total_amount'] = int(total_amount)
        return data

    def default_start(self, fields):
        res = { }
        return res

    def do_create_report(self, action):
        start = self.start.start_date
        end = self.start.end_date
        level = self.start.grouping_level
        insurance = self.start.insurance
        regime = self.start.plan
        subscriber = self.start.subscriber

        data = {
            'start_date' : start.strftime("%d/%m/%Y"),
            'end_date' : end.strftime("%d/%m/%Y"),
            'grouping_level' : self.start.grouping_level,
            'report_type' : self.start.report_type,
            'report_detail' : self.start.report_detail,
            }

        if level == 'one_plan':
            if self.start.report_detail == 'bydates':
                if self.start.report_type == 'category':
                    data = CreateInvoicePerTarifType.processByCategoryByDates(self, start, end, insurance, regime, data)
                elif self.start.report_type == 'products':
                    data = CreateInvoicePerTarifType.processByProductsByDates(self, start, end, insurance, regime, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = CreateInvoicePerTarifType.processByAllProductsByDates(self, start, end, insurance, regime, data)
            elif self.start.report_detail == 'bypatients':
                if self.start.report_type == 'category':
                    data = CreateInvoicePerTarifType.processByCategoryByPatients(self, start, end, insurance, regime, data)
                elif self.start.report_type == 'products':
                    data = CreateInvoicePerTarifType.processByProductsByPatients(self, start, end, insurance, regime, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = CreateInvoicePerTarifType.processByAllProductsByPatients(self, start, end, insurance, regime, data)
            elif self.start.report_detail == 'total':
                if self.start.report_type == 'category':
                    data = CreateInvoicePerTarifType.processByCategoryTotals(self, start, end, insurance, regime, data)
                elif self.start.report_type == 'products':
                    data = CreateInvoicePerTarifType.processByProductsTotals(self, start, end, insurance, regime, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = CreateInvoicePerTarifType.processByAllProductsTotals(self, start, end, insurance, regime, data)
        elif level == 'one_insurance':
            if self.start.report_detail == 'bydates':
                if self.start.report_type == 'category':
                    data = self.processOneByCategoryByDates(start, end, insurance, data)
                elif self.start.report_type == 'products':
                    data = self.processOneByProductsByDates(start, end, insurance, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = self.processOneByAllProductsByDates(start, end, insurance, data)
            elif self.start.report_detail == 'bypatients':
                if self.start.report_type == 'category':
                    data = self.processOneByCategoryByPatients(start, end, insurance, data)
                elif self.start.report_type == 'products':
                    data = self.processOneByProductsByPatients(start, end, insurance, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = self.processOneByAllProductsByPatients(start, end, insurance, data)
            elif self.start.report_detail == 'total':
                if self.start.report_type == 'category':
                    data = self.processOneByCategoryTotals(start, end, insurance, data)
                elif self.start.report_type == 'products':
                    data = self.processOneByProductsTotals(start, end, insurance, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = self.processOneByAllProductsTotals(start, end, insurance, data)
        elif level == 'one_subscriber':
            if self.start.report_detail == 'bydates':
                if self.start.report_type == 'category':
                    data = self.processSubByCategoryByDates(start, end, subscriber, data)
                elif self.start.report_type == 'products':
                    data = self.processSubByProductsByDates(start, end, subscriber, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = self.processSubByAllProductsByDates(start, end, subscriber, data)
            elif self.start.report_detail == 'bypatients':
                if self.start.report_type == 'category':
                    data = self.processSubByCategoryByPatients(start, end, subscriber, data)
                elif self.start.report_type == 'products':
                    data = self.processSubByProductsByPatients(start, end, subscriber, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = self.processSubByAllProductsByPatients(start, end, subscriber, data)
            elif self.start.report_detail == 'total':
                if self.start.report_type == 'category':
                    data = self.processSubByCategoryTotals(start, end, subscriber, data)
                elif self.start.report_type == 'products':
                    data = self.processSubByProductsTotals(start, end, subscriber, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = self.processSubByAllProductsTotals(start, end, subscriber, data)
        elif level == 'all_insurances':
            if self.start.report_detail == 'bydates':
                if self.start.report_type == 'category':
                    data = self.processAllByCategoryByDates(start, end, data)
                elif self.start.report_type == 'products':
                    data = self.processAllByProductsByDates(start, end, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = self.processAllByAllProductsByDates(start, end, data)
            elif self.start.report_detail == 'bypatients':
                if self.start.report_type == 'category':
                    data = self.processAllByCategoryByPatients(start, end, data)
                elif self.start.report_type == 'products':
                    data = self.processAllByProductsByPatients(start, end, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = self.processAllByAllProductsByPatients(start, end, data)
            elif self.start.report_detail == 'total':
                if self.start.report_type == 'category':
                    data = self.processAllByCategoryTotals(start, end, data)
                elif self.start.report_type == 'products':
                    data = self.processAllByProductsTotals(start, end, self.start.category, data)
                elif self.start.report_type == 'all':
                    data = self.processAllByAllProductsTotals(start, end, data)

        return action, data
