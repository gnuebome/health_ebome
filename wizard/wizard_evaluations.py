# -*- coding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView
from trytond.wizard import Wizard, StateTransition, StateAction, StateView, Button
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import PYSONEncoder
from trytond.i18n import gettext

from ..exceptions import NoPatient, NoRecordSelected

# Consulta de todas las evaluaciones del paciente, independientemente del origen de las mismas (rendez-vous o hospitalisation)
class OpenInpatientEvaluations(Wizard):
    'Open Inpatient Evaluation'
    __name__ = 'wizard.gnuhealth.inpatient.evaluations'

    start_state = 'inpatient_evaluations'
    inpatient_evaluations = StateAction('health_ebome.act_inpatient_evaluations')

    def do_inpatient_evaluations(self, action):
        pool = Pool()
        Evaluations = pool.get('gnuhealth.patient.evaluation')
        inpatient = Transaction().context.get('active_id')
        try:
            reg_id = \
                Pool().get('gnuhealth.inpatient.registration').browse([inpatient])[0]
        except:
            raise NoRecordSelected(gettext('health_ebome.msg_no_record_selected'))

        try:
            patient_id = reg_id.patient.id
        except:
            raise NoPatient(gettext('health_ebome.msg_no_patient'))

        evaluations = Evaluations.search(['patient','=',patient_id])

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',patient_id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': patient_id,
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': reg_id.patient.rec_name,
            }
        data = {'res_id': [x.id for x in evaluations]}

        return action, data

# Wizard idéntico al original con la diferencia de que se fuerza la apertura de la vista formulario para una nueva evaluación
class CreateInpatientEvaluation(Wizard):
    'Create Inpatient Evaluation'
    __name__ = 'wizard.gnuhealth.inpatient.evaluation'

    start_state = 'inpatient_evaluation'
    inpatient_evaluation = StateAction('health_ebome.act_inpatient_evaluation')

    def do_inpatient_evaluation(self, action):

        inpatient_registration = Transaction().context.get('active_id')

        try:
            reg_id = \
                Pool().get('gnuhealth.inpatient.registration').browse([inpatient_registration])[0]
        except:
            raise NoRecordSelected(
                gettext('health_inpatient.msg_no_record_selected'))

        patient = reg_id.patient.id


        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient', '=', patient),
            ('inpatient_registration_code', '=', reg_id.id),
            ('evaluation_type', '=', 'inpatient'),
            ('hospitalized', '=', True),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': patient,
            'inpatient_registration_code': reg_id.id,
            'evaluation_type': 'inpatient',
            'hospitalized': True,
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': reg_id.patient.rec_name,
            }
        action['views'].reverse()

        return action, {}

# Wizard idéntico al original con la diferencia de que se muestra el nombre del paciente en la cabecera de la vista
class OpenAppointmentEvaluations(Wizard):
    'Open Appointment Evaluations'
    __name__ = 'wizard.gnuhealth.appointment.evaluation'

    start_state = 'appointment_evaluations'
    appointment_evaluations = StateAction('health_cameroun.act_app_evaluations')

    def do_appointment_evaluations(self, action):
        pool = Pool()
        Evaluations = pool.get('gnuhealth.patient.evaluation')
        appointment = Transaction().context.get('active_id')
        try:
            app_id = \
                Pool().get('gnuhealth.appointment').browse([appointment])[0]
        except:
            raise NoRecordSelected(gettext('health_cameroun.msg_no_record_selected'))

        try:
            patient_id = app_id.patient.id
        except:
            raise NoPatient(gettext('health_cameroun.msg_no_patient'))

        evaluations = Evaluations.search(['patient','=',patient_id])
        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',app_id.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': app_id.patient.id,
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': app_id.patient.rec_name,
            }
        data = {'res_id': [x.id for x in evaluations]}
        return action, data

# Wizard idéntico al original con la diferencia de que se fuerza la apertura de la vista formulario para una nueva evaluación
class CreateAppointmentEvaluation(Wizard):
    'Create Appointment Evaluation'
    __name__ = 'wizard.gnuhealth.app.evaluation'

    start_state = 'appointment_evaluation'
    appointment_evaluation = StateAction('health_ebome.act_app_evaluation')

    def do_appointment_evaluation(self, action):

        appointment = Transaction().context.get('active_id')

        try:
            app_id = \
                Pool().get('gnuhealth.appointment').browse([appointment])[0]
        except:
            raise NoAppointmentSelected(gettext(
                'health.msg_no_appointment_selected')
                )

        patient = app_id.patient.id

        if (app_id.speciality):
            specialty = app_id.speciality.id
        else:
            specialty = None
        urgency = str(app_id.urgency)
        evaluation_type = str(app_id.appointment_type)
        if evaluation_type == 'inpatient':
            hospitalized = True
        else:
            hospitalized = False
        visit_type = str(app_id.visit_type)

        action['pyson_domain'] = PYSONEncoder().encode([
            ('appointment', '=', appointment),
            ('patient', '=', patient),
            ('specialty', '=', specialty),
            ('urgency', '=', urgency),
            ('evaluation_type', '=', evaluation_type),
            ('hospitalized', '=', hospitalized),
            ('visit_type', '=', visit_type),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'appointment': appointment,
            'patient': patient,
            'specialty': specialty,
            'urgency': urgency,
            'evaluation_type': evaluation_type,
            'hospitalized': hospitalized,
            'visit_type': visit_type,
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': app_id.patient.rec_name,
            }
        action['views'].reverse()

        return action, {}
