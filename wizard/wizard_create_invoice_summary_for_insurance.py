from datetime import *
from dateutil.relativedelta import relativedelta

from trytond.i18n import gettext
from trytond.model import ModelView, fields
from trytond.pyson import Eval, Or
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateAction, Button, StateReport
from trytond.transaction import Transaction
from pprint import pprint

from ..exceptions import DraftInvoicesExist

class CreateInvoiceSummaryStart(ModelView):
    'Invoice Summary Report Create Start'
    __name__ = 'gnuhealth.invoice.summary.create.start'

    summary_month = fields.Many2One('ir.calendar.month', "Month", required=True)

    summary_year = fields.Numeric('Year', required=True,
                                  digits=(4, 0),
                                  domain=['OR',
                                        ('summary_year', '=', None),
                                        [('summary_year', '>=', 2024), ('summary_year', '<=', 2099)],
                                        ])

    insurance = fields.Many2One('party.party', 'Insurance', required=True,
                                 domain=[('insurance_company_type', 'in', ['i', 'c', 's'])],
                                 search_order=[('insurance_company_type', 'ASC'),
                                               ('name', 'ASC')])

    @classmethod
    def default_summary_month(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        mes = Date.today().month
        if mes == 1:
            return 12
        else:
            return mes - 1

    @classmethod
    def default_summary_year(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        mes = Date.today().month
        year = Date.today().year
        if mes == 1:
            return year - 1
        else:
            return year

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        Month = pool.get('ir.calendar.month')

class CreateInvoiceSummary(Wizard):
    'Invoice Summary Report Create'
    __name__ = 'gnuhealth.invoice.summary.create'

    start = StateView('gnuhealth.invoice.summary.create.start',
        'health_ebome.view_create_invoice_summary', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_ebome.report_invoice_summary_for_insurance_from_wizard')

    def processByInsurance(self, start, end, insurance, data):
        pool = Pool()
        Plans = pool.get('gnuhealth.insurance.plan')
        Invoices = pool.get('account.invoice')

        results = {}
        subscribers_list = []
        coverages_list = []
        counters_list = []
        plans = Plans.search([('company', '=', self.start.insurance.id)])
        for plan in plans:
            subscribers_list.append(plan.insurance_subscriber.rec_name)
            counters_list.append(1)

        total = 0
        total_insurance = 0
        total_insured = 0

        invoices = Invoices.search([('invoice_date','>=',start),
                                    ('invoice_date','<=',end),
                                    ('party','=',self.start.insurance.id),
                                    ('state','=', 'posted')])


        for invoice in invoices:
            subscriber = invoice.lines[0].origin.name.insurance_plan.plan_id.insurance_subscriber.rec_name
#            coverage = invoice.lines[0].origin.name.insurance_plan.plan_id.coverage
            n = subscribers_list.index(subscriber)
            if counters_list[n] == 1:
                results[subscriber] = {'total' : 0, 'total_insurance' : 0, 'total_insured' : 0,
                                       'invoices' : []}
            results[subscriber]['total'] += invoice.base_total_amount
            results[subscriber]['total_insurance'] += invoice.total_amount
            results[subscriber]['total_insured'] += (invoice.base_total_amount - invoice.total_amount)
            results[subscriber]['invoices'].append({'invoice_number' : invoice.number, 'counter' : counters_list[n], 'invoice_date' : invoice.invoice_date,
#                          'insurance_holder' : invoice.insurance_holder.rec_name, 'patient' : invoice.patient.rec_name,
                          'patient' : invoice.patient.rec_name,
                          'total_amount' : invoice.base_total_amount, 'coverage' : invoice.insurance_coverage, 'insurance_amount' : invoice.total_amount,
                          'insured_amount' : (invoice.base_total_amount - invoice.total_amount)})
            counters_list[n] += 1
            total += invoice.base_total_amount
            total_insurance += invoice.total_amount
            total_insured += (invoice.base_total_amount - invoice.total_amount)

        data['results'] = results
        data['total'] = total
        data['total_insurance'] = total_insurance
        data['total_insured'] = total_insured
        return data

    def processByOther(self, start, end, insurance, data):
        pool = Pool()
        Invoices = pool.get('account.invoice')

        results = {}
        total_subscriber = {}
        total = 0
        total_insurance = 0
        total_insured = 0

        invoices = Invoices.search([('invoice_date','>=',start),
                                    ('invoice_date','<=',end),
                                    ('party','=',self.start.insurance.id),
                                    ('state','=', 'posted')])


        n = 1
        for invoice in invoices:
            results[n] = {'subscriber' : '', 'invoice_number' : invoice.number, 'invoice_date' : invoice.invoice_date,
                          'insurance_holder' : invoice.party.rec_name, 'patient' : invoice.patient.rec_name,
                          'total_amount' : invoice.base_total_amount, 'insurance_amount' : invoice.total_amount,
                          'insured_amount' : invoice.base_total_amount - invoice.total_amount}
            n += 1
            total += invoice.base_total_amount
            total_insurance += invoice.total_amount
            total_insured += (invoice.base_total_amount - invoice.total_amount)

        data['results'] = results
        data['total'] = total
        data['total_insurance'] = total_insurance
        data['total_insured'] = total_insured
        return data

    def default_start(self, fields):
        res = { }
        return res

    def do_create_report(self, action):
        pool = Pool()
        Invoices = pool.get('account.invoice')
        Company = pool.get('company.company')
        Address = pool.get('party.address')

        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)

        # obtener el primer día del mes
        month=int(self.start.summary_month.index)
        start = date(self.start.summary_year, month, 1)

        # obtener el último día del mes. Suma 4 al día 28 del mes actual para obtener un día del próximo mes
        # y resta los días de la fecha resultante para obtener el último día del mes actual
        next_month = start.replace(day=28) + timedelta(days=4)
        end = next_month - timedelta(days=next_month.day)

        invoice_number = str(self.start.insurance.id) + "-" + str(month) + "/" + str(self.start.summary_year)

        address = Address.search(['party', '=', self.start.insurance.id])
        if address:
            if address[0].party_name:
                if address[0].party_name == '':
                    insurance = self.start.insurance.rec_name
                else:
                    insurance = address[0].party_name
            else:
                insurance = self.start.insurance.rec_name
            if address[0].full_address:
                if address[0].full_address == '':
                    insurance_address = ''
                else:
                    insurance_address = address[0].full_address
            else:
                insurance_address = ''
        else:
            insurance = self.start.insurance.rec_name
            insurance_address = ''

        data = {
            'date' : date.today(),
            'summary_month' : self.start.summary_month.abbreviation,
            'summary_year' : self.start.summary_year,
            'insurance' : insurance,
            'insurance_address' : insurance_address,
            'invoice_number' : invoice_number,
            'lang' : self.start.insurance.lang,
            'currency_digits' : company.currency.digits
            }

        invoices = Invoices.search([('invoice_date','>=',start),
                                    ('invoice_date','<=',end),
                                    ('party','=',self.start.insurance.id),
                                    ('state','=', 'draft')])

        if invoices:
            raise DraftInvoicesExist(
                    gettext('health_ebome.msg_draft_invoices_exist'))
        else:
            data['insurance_company_type'] = self.start.insurance.insurance_company_type

        if self.start.insurance.insurance_company_type == 'i':
            data = self.processByInsurance(start, end, self.start.insurance.id, data)
        else:
            data = self.processByOther(start, end, self.start.insurance.id, data)

        return action, data
