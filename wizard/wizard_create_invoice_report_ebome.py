from datetime import datetime
from trytond.model import ModelView, fields
from trytond.pyson import Eval, Or
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateAction, Button, StateReport
from pprint import pprint

class CreateInvoiceReportEbomeStart(ModelView):
    'Invoice Report Create Start'
    __name__ = 'invoice.report.create.start'

    start_date = fields.Date('Start Date', required=True)

    end_date = fields.Date('End Date', required=True)

    report_type = fields.Selection([
        (None, ''),
        ('category', 'By Category'),
        ('products', 'By Products'),
        ], 'Report Type', sort=False, required=True)

    category = fields.Many2One('product.category', 'Category', help="Category to generate its products report.",
                                domain=[('accounting', '=', True), ('code', '!=', '30')],
                                states={
                                    'required': Eval('report_type') == 'products',
                                    'invisible': Eval('report_type') != 'products',
                                        }
                                )

    @classmethod
    def default_report_type(cls):
        return 'category'

    @classmethod
    def default_start_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def default_end_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

class CreateInvoiceReportEbome(Wizard):
    'Invoice Report Create'
    __name__ = 'invoice.report.create'

    start = StateView('invoice.report.create.start',
        'health_ebome.view_create_invoice_ebome_start', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_ebome.report_invoice_ebome_from_wizard')

    def processByCategory(self, start, end, data):
        pool = Pool()
        Category = pool.get('product.category')
        Tarif_types = pool.get('gnuhealth.insurance.plan')
        Invoice = pool.get('account.invoice')
        Services = pool.get('gnuhealth.health_service')

        data['report_type'] = self.start.report_type
        data['title'] = 'Facturation Crée par categorie'
        data['title_table'] = 'Categorie'
        results = {}
        total = {}
        total_with_values = {}
        errors = []
        total_amount = 0
        total_quantity = 0
        tarif_total_amount = 0
        tarif_total_quantity = 0

        categories = Category.search([
            ('accounting', '=', True),
            ('code', '!=', '30')
            ], order=[('name', 'ASC')])
        cat_list = []
        for category in categories:
            cat_list.append(category.name)
            results[category.rec_name] = {'amount' : 0, 'quantity' : 0}

        selected_tarif_types = Tarif_types.search([])
        for tarif_type in selected_tarif_types:
            total[tarif_type.id] = {'tarif_type' : tarif_type.company.name + ':' + tarif_type.name.template.name, 'amount' : 0, 'quantity' : 0}

        # Get invoices between dates in state posted or payed
        selected_invoices = Invoice.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state','in', ['posted','paid'])
            ])

        for invoice in selected_invoices:
            p_type = False
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        p_type = service[0].insurance_plan.plan_id
                    else:
                        errors.append("Without Insurance " + str(invoice.insurance_holder.code))
                else:
                    errors.append("Without Service " + str(invoice.reference))
            else:
                errors.append("Without Reference " + str(invoice.rec_name))
            for line in invoice.lines:
                if line.type == 'line' and line.product:
                    if line.product.account_category:
                        if line.product.account_category.rec_name in cat_list:
                            results[line.product.account_category.rec_name]['amount'] += line.amount
                            results[line.product.account_category.rec_name]['quantity'] += line.quantity
                            if p_type:
                                total[p_type.id]['amount'] += line.amount
                                total[p_type.id]['quantity'] += line.quantity
                                tarif_total_amount += line.amount
                                tarif_total_quantity += line.quantity
                            total_amount += line.amount
                            total_quantity += line.quantity

        for element in total:
            if total[element]['amount'] > 0 or\
               total[element]['quantity'] > 0:
                   total_with_values[element] = {'tarif_type' : total[element]['tarif_type'],
                                                 'amount' : total[element]['amount'],
                                                 'quantity' : total[element]['quantity']}

        data['results'] = results
        data['total'] = total_with_values
        data['total_amount'] = total_amount
        data['total_quantity'] = total_quantity
        data['tarif_total_amount'] = tarif_total_amount
        data['tarif_total_quantity'] = tarif_total_quantity
        data['errors'] = errors
        return data

    def processByProducts(self, start, end, category, data):
        pool = Pool()
        Product = pool.get('product.product')
        Tarif_types = pool.get('gnuhealth.insurance.plan')
        Invoice = pool.get('account.invoice')
        Services = pool.get('gnuhealth.health_service')

        data['report_type'] = self.start.report_type
        data['title'] = 'Facturation Crée par produit dans ' +str(category.rec_name)
        data['title_table'] = 'Produits'
        results = {}
        products_total = {}
        total = {}
        total_with_values = {}
        total_amount = 0
        total_quantity = 0
        tarif_total_amount = 0
        tarif_total_quantity = 0
        errors = []

        # Get products in selected category
        products = Product.search([
            ('active', 'in', [True, False]),
            ('is_insurance_plan', '=', False)
            ], order=[('name', 'ASC')])
        for product in products:
            if product.account_category:
                if product.account_category.rec_name == category.rec_name:
                    products_total[product.id] = {'name' : product.name, 'amount' : 0, 'quantity' : 0}

        selected_tarif_types = Tarif_types.search([])
        for tarif_type in selected_tarif_types:
            total[tarif_type.id] = {'tarif_type' : tarif_type.company.name + ':' + tarif_type.name.template.name, 'amount' : 0, 'quantity' : 0}

        # Get invoices between dates in state posted or payed
        selected_invoices = Invoice.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('reference', 'like', 'SERV%'),
            ('state','in', ['posted','paid'])
            ])

        for invoice in selected_invoices:
            p_type = False
            if invoice.reference:
                service = Services.search([
                    ('name', '=', invoice.reference)
                    ])
                if service:
                    if service[0].insurance_plan:
                        p_type = service[0].insurance_plan.plan_id
                    else:
                        errors.append("Without Insurance " + str(invoice.insurance_holder.code))
                else:
                    errors.append("Without Service " + str(invoice.reference))
            else:
                errors.append("Without Reference " + str(invoice.rec_name))
            for line in invoice.lines:
                if line.type == 'line' and line.product:
                    if line.product.account_category:
                        if line.product.account_category.rec_name == category.rec_name:
                            products_total[line.product.id]['amount'] += line.amount
                            products_total[line.product.id]['quantity'] += line.quantity
                            if p_type:
                                total[p_type.id]['amount'] += line.amount
                                total[p_type.id]['quantity'] += line.quantity
                                tarif_total_amount += line.amount
                                tarif_total_quantity += line.quantity
                            total_amount += line.amount
                            total_quantity += line.quantity

        for element in products_total:
            if products_total[element]['amount'] >0 or \
               products_total[element]['quantity'] > 0:
                   results[products_total[element]['name']] = {'amount' : products_total[element]['amount'],
                                                               'quantity' : products_total[element]['quantity']}

        for element in total:
            if total[element]['amount'] > 0 or\
               total[element]['quantity'] > 0:
                   total_with_values[element] = {'tarif_type' : total[element]['tarif_type'],
                                                 'amount' : total[element]['amount'],
                                                 'quantity' : total[element]['quantity']}

        data['results'] = results
        data['total_amount'] = total_amount
        data['total_quantity'] = total_quantity
        data['total'] = total_with_values
        data['tarif_total_amount'] = tarif_total_amount
        data['tarif_total_quantity'] = tarif_total_quantity
        data['errors'] = errors
        return data

    def default_start(self, fields):
        res = { }
        return res

    def do_create_report(self, action):
        start = self.start.start_date
        end = self.start.end_date

        data = {
            'start_date' : start,
            'end_date' : end,
            }

        if self.start.report_type == 'category':
            data = self.processByCategory(start, end, data)
        elif self.start.report_type == 'products':
            data = self.processByProducts(start, end, self.start.category, data)

        return action, data
