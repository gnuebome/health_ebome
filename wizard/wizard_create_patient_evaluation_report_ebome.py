# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from datetime import *
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateAction, Button, StateReport
from trytond.pool import Pool

class CreatePatientEvaluationReportEbomeStart(ModelView):
    'Patient Evaluation Report Create Start'
    __name__ = 'gnuhealth.patient.evaluation.report.create.start'
    
    start_date = fields.Date('Start Date', required=True)
    
    end_date = fields.Date('End Date', required=True)

    doctors = fields.Many2Many('gnuhealth.healthprofessional', None,
        None, 'Health Professional', required=True,
        domain=[('main_specialty.specialty.code', 'in', ['GENERAL', 'GYN', 'Maternité', 'PEDIATRICS', 'INTERNAL'])]
        )

class CreatePatientEvaluationReportEbome(Wizard):
    'Patient Evaluation Report Create'
    __name__ = 'gnuhealth.patient.evaluation.report.create'

    start = StateView('gnuhealth.patient.evaluation.report.create.start',
        'health_ebome.view_create_patient_evaluation_ebome_start', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_ebome.report_patient_evaluation_ebome_from_wizard')
    #create_report = StateReport('gnuhealth.patient.evaluation.report')

    def default_start(self, fields):
        res = { }
        return res

    def get_doctor(self, ev):
        Party=Pool().get('party.party')

        doctor = {'id': ' ', 'name': ' '}

        if ev.state == "signed":
            doctor['id'] = ev.signed_by.name.id
            doctor['name'] = ev.signed_by.rec_name
            return doctor
        else:
            if ev.write_uid:
                party, = Party.search([
                    ('internal_user.id', '=', ev.write_uid.id),
                    ])
                if party:
                    doctor['id'] = party.id
                    doctor['name'] = party.rec_name
                    return doctor
                else:
                    return doctor
            else:
                doctor['id'] = ev.healthprof.name.id
                doctor['name'] = ev.healthprof.rec_name
                return doctor

    def get_content(self, ev):
        res = ""
        if ev.evaluation_summary:
            res += " evaluation_summary " + ev.evaluation_summary
        if ev.supplementary_tests:
            res += " supplementary_tests " + ev.supplementary_tests
        if ev.present_illness:
            res += " present_illness " + ev.present_illness
        if ev.diagnosis:
            res += " diagnosis " + ev.diagnosis.rec_name
        if res == "":
            res = "Consultation vide"
        return res

    def get_gender(self, ev):
        gender = ev.patient.gender
        res = ""
        if gender == "m":
            res = 'Homme'
        elif gender == "f":
            res = 'Femme'
        elif gender == "u":
            res = 'Inconue'
        return res


    def get_age(self, ev):
        try:
            age = int(ev.computed_age.split('y')[0])
        except:
            return 'without'
        if age <= 15:
            return 'young'
        else:
            return 'old'

    def get_visit_type(self, ev):
        res = ""
        if ev.visit_type == "followup":
            res = 'Suivi'
        else:
            res = 'Nouvel état de santé'
        return res

    def get_state(self, ev):
        res = ""

        if ev.state == "signed":
            res = "signé"
        elif ev.state == "done":
            res = "effectué"
        elif ev.state == "in_progress":
            if ev.evaluation_summary or ev.supplementary_tests or ev.present_illness or ev.diagnosis:
                res = "effectué"
            else:
                res = "en cours"
        return res


    def do_create_report(self, action):
        data = {
            'start_date': self.start.start_date.strftime("%d/%m/%Y"),
            'end_date': self.start.end_date.strftime("%d/%m/%Y"),
            'title' : "REPORT CONSULTATIONS"
            }

        t = self.start.start_date
        datetime_start = datetime(t.year, t.month, t.day)
        t = self.start.end_date
        datetime_end = datetime(t.year, t.month, t.day) + timedelta(days = 1)

        # Get evaluation between dates
        pool = Pool()
        Evaluation = pool.get('gnuhealth.patient.evaluation')
        selected_evaluations = Evaluation.search([
            ('evaluation_start','>=',datetime_start),('evaluation_start','<=',datetime_end),
            ])

        errors = []
        not_doc = {}

        # List of doctors to sumup evaluations
        statistics = {}
        users = {}
        for d in self.start.doctors:
            statistics[d.name.id] = {'name': d.name.rec_name, 'Total' : 0, 'Homme' : 0,'Femme' : 0,'Inconue' : 0,'young' : 0,'old' : 0,'without' : 0}

        total = {'Total' : 0, 'Homme' : 0,'Femme' : 0,'Inconue' : 0,'young' : 0,'old' : 0,'without' : 0}

        # Extract information from evaluations
        for ev in selected_evaluations:
            doctor = self.get_doctor(ev)
            doc_id = doctor['id']
            gender = self.get_gender(ev)
            age = self.get_age(ev)
            if doc_id in statistics:
                statistics[doc_id][gender] += 1
                statistics[doc_id][age] += 1
                statistics[doc_id]['Total'] += 1
                total[gender] += 1
                total[age] += 1
                total['Total'] += 1
            elif doc_id == "":
                errors.append("{0} {1} {2}".format(ev.code,ev.state, self.get_content(ev)))
            else:
                not_doc[ev.code] = {'date' : 0, 'visit_type' : ' ', 'patient' : ' ', 'patient_code' : ' ', 'healthprof' : ' ', 'state' : ' '}
                not_doc[ev.code]['date'] = ev.evaluation_start.strftime("%d/%m/%Y")
                not_doc[ev.code]['visit_type'] = self.get_visit_type(ev)
                not_doc[ev.code]['patient'] = ev.patient.rec_name
                not_doc[ev.code]['patient_code'] = ev.patient_code
                not_doc[ev.code]['healthprof'] = doctor['name']
                not_doc[ev.code]['state'] = self.get_state(ev)

        data['statistics'] = statistics
        data['total'] = total
        data['errors'] = errors
        data['not_doc'] = not_doc
        return action, data
