from datetime import datetime
from trytond.model import ModelView, fields
from trytond.pyson import Eval, Or
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateAction, Button, StateReport
from pprint import pprint

class CreateInpatientReportEbome(Wizard):
    'Debtors Report Create'
    __name__ = 'inpatient.report.create'

    def get_gender(self, inpatient):
        gender = inpatient.patient.gender
        res = ""
        if gender == "m":
            res = 'Homme'
        elif gender == "f":
            res = 'Femme'
        elif gender == "u":
            res = 'Inconue'
        return res

    start_state = 'create_report'
    create_report = StateAction('health_ebome.report_inpatient_ebome_from_wizard')

    def do_create_report(self, action):
        pool = Pool()

        data = {}
        data['title'] = 'Rapport de patients hospitalisé(e)s'
        data['date'] = Pool().get('ir.date').today().strftime("%d/%m/%Y")

        results = {}
        total_hospitalized = 0
        total_done = 0

        Inpatient = pool.get('gnuhealth.inpatient.registration')

        selected_inpatient = Inpatient.search([
            ('state','in',['hospitalized', 'done'])],
            order=[('bed.ward', 'ASC'), ('bed.name', 'ASC')])

        for inpatient in selected_inpatient:
            results[inpatient.id] = {'inpatient_patient' : ' ', 'inpatient_code' : ' ', 'inpatient_gender' : ' ', 'inpatient_age' : ' ', 'inpatient_hospitalization_date' : 0, 'inpatient_physician' : ' ', 'inpatient_ward' : ' ', 'inpatient_bed' : ' '}
            results[inpatient.id]['inpatient_patient'] = inpatient.patient.rec_name
            results[inpatient.id]['inpatient_code'] = inpatient.patient.code
            results[inpatient.id]['inpatient_gender'] = self.get_gender(inpatient)
            if inpatient.patient.age:
                results[inpatient.id]['inpatient_age'] = inpatient.patient.age
            results[inpatient.id]['inpatient_hospitalization_date'] = inpatient.hospitalization_date.strftime("%d/%m/%Y")
            results[inpatient.id]['inpatient_physician'] = inpatient.attending_physician.rec_name
            results[inpatient.id]['inpatient_ward'] = inpatient.bed.ward.name
            results[inpatient.id]['inpatient_bed'] = inpatient.bed.rec_name
            results[inpatient.id]['inpatient_state'] = inpatient.state
            if inpatient.state == 'done':
                results[inpatient.id]['inpatient_state'] = 'en attende de facturation'
                total_done += 1
            else:
                if inpatient.patient.gender == 'f':
                    results[inpatient.id]['inpatient_state'] = 'hospitalisée'
                else:
                    results[inpatient.id]['inpatient_state'] = 'hospitalisé'
                total_hospitalized += 1

        data['results'] = results
        data['total_hospitalized'] = total_hospitalized
        data['total_done'] = total_done

        return action, data
