# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2022 Luis Falcon <lfalcon@gnuhealth.org>
#    Copyright (C) 2013  Sebastián Marro <smarro@thymbra.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import datetime

from trytond.i18n import gettext
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateAction, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.pyson import Eval, PYSONEncoder
from trytond.pool import Pool

from trytond.modules.health.core import get_health_professional

from ..exceptions import NoPatient, NoRecordSelected
from trytond.modules.health_lab.exceptions import (LabOrderExists)

# Añadimos nuevo campo de texto libre "symptoms"
# Añadimos nuevo campo de texto libre "origin_request"
# El campo "servicio" es obligatorio
# Ponemos el campo "ungroup" como readonly
# Presentamos los test ordenados por nombre
class RequestPatientLabTestStart(ModelView):
    'Request Patient Lab Test Start'
    __name__ = 'gnuhealth.patient.lab.test.request.start'

    symptoms = fields.Char("Symptoms", required = True)

    origin_request = fields.Char("Origin of the request")

    @staticmethod
    def default_patient():
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        Surgery = pool.get('gnuhealth.surgery')
        Inpatient = pool.get('gnuhealth.inpatient.registration')

        if Transaction().context.get('active_model') == 'gnuhealth.patient':
            patient_id = Transaction().context.get('active_id')
            return patient_id

        if Transaction().context.get('active_model') == 'gnuhealth.appointment':
            appointment_id = Transaction().context.get('active_id')
            appointment = Appointment.search([('id','=',appointment_id)])[0]
            if appointment.patient:
                return appointment.patient.id

        if Transaction().context.get('active_model') == 'gnuhealth.surgery':
            surgery_id = Transaction().context.get('active_id')
            surgery = Surgery.search([('id','=',surgery_id)])[0]
            if surgery.patient:
                return surgery.patient.id

        if Transaction().context.get('active_model') == 'gnuhealth.inpatient.registration':
            inpatient_id = Transaction().context.get('active_id')
            inpatient = Inpatient.search([('id','=',inpatient_id)])[0]
            if inpatient.patient:
                return inpatient.patient.id

    @staticmethod
    def default_origin_request():

        if Transaction().context.get('active_model') == 'gnuhealth.patient':
            return 'Secrétariat'

        if Transaction().context.get('active_model') == 'gnuhealth.appointment':
            if Transaction().context.get('urgency') == 'c':
                return 'Urgences'
            else:
                return 'Consultation'

        if Transaction().context.get('active_model') == 'gnuhealth.surgery':
            return 'Chirurgie'

        if Transaction().context.get('active_model') == 'gnuhealth.inpatient.registration':
            return 'Hospitalisation'

        return 'Consultation'

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.date.required = True
        cls.tests.search_order=[('name', 'ASC')]
        cls.ungroup_tests.readonly = True
        cls.service.required = True

        cls.tests.context.update({
            'modelo': 'gnuhealth.patient.lab.test.request.start',
            'patient': Eval('patient', 0)
            })
        cls.tests.depends.append('patient')

        cls.tests.domain=[('product_id.template.account_category', '>', 0),
                          ('OR',
                           [('catalog_name', '=', 'Assurance'),
                            ('product_id.template.is_for_insured', '=', True)
                           ],
                           [('catalog_name', '=', 'Visite'),
                            ('product_id.template.is_for_visit', '=', True)
                           ],
                           [('catalog_name', '=', 'Particular'),
                            ('product_id.template.is_for_particular', '=', True)
                           ]
                          )
                         ]

class RequestPatientLabTest(Wizard):
    'Request Patient Lab Test'
    __name__ = 'gnuhealth.patient.lab.test.request'

    start = StateView(
        'gnuhealth.patient.lab.test.request.start',
        'health_lab.patient_lab_test_request_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Request', 'request', 'tryton-ok', default=True),
            ])
    request = StateTransition()

    def generate_code(self, **pattern):
        Config = Pool().get('gnuhealth.sequences')
        config = Config(1)
        sequence = config.get_multivalue(
            'lab_request_sequence', **pattern)
        if sequence:
            return sequence.get()

    def append_services(self, labtest, service):
        """ If the ungroup flag is not set, append the lab test
            to the associated health service
        """
        HealthService = Pool().get('gnuhealth.health_service')

        hservice = []

        service_data = {}
        service_lines = []

        # Add the labtest to the service document

        service_lines.append(('create', [{
            'product': labtest.product_id.id,
            'desc': labtest.product_id.rec_name,
            'qty': 1
            }]))

        hservice.append(service)
        service_data['service_line'] = service_lines

        HealthService.write(hservice, service_data)

    def transition_request(self):
        PatientLabTest = Pool().get('gnuhealth.patient.lab.test')
        request_number = self.generate_code()
        lab_tests = []
        for test in self.start.tests:
            lab_test = {}
            lab_test['request'] = request_number
            lab_test['name'] = test.id
            lab_test['patient_id'] = self.start.patient.id
            if self.start.doctor:
                lab_test['doctor_id'] = self.start.doctor.id
            lab_test['symptoms'] = self.start.symptoms
            lab_test['origin_request'] = self.start.origin_request
            lab_test['date'] = self.start.date
            lab_test['urgent'] = self.start.urgent

            if self.start.service:
                lab_test['service'] = self.start.service.id
                # Append the test directly to the health service document
                # if the Ungroup flag is not set (default).
                if not self.start.ungroup_tests:
                    self.append_services(test, self.start.service)
            lab_tests.append(lab_test)

        PatientLabTest.create(lab_tests)

        return 'end'

# Se modifica para pasar los nuevos campos de la solicitud al resultado del test
class CreateLabTestOrder(Wizard):
    'Create Lab Test Report'
    __name__ = 'gnuhealth.lab.test.create'

    start = StateView(
        'gnuhealth.lab.test.create.init',
        'health_lab.view_lab_make_test', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create Test Order', 'create_lab_test', 'tryton-ok', True),
            ])

    create_lab_test = StateTransition()

    def transition_create_lab_test(self):
        TestRequest = Pool().get('gnuhealth.patient.lab.test')
        Lab = Pool().get('gnuhealth.lab')

        tests_report_data = []

        tests = TestRequest.browse(Transaction().context.get('active_ids'))

        for lab_test_order in tests:

            test_cases = []
            test_report_data = {}

            if lab_test_order.state == 'ordered':
                raise LabOrderExists(
                    gettext('health_lab.msg_lab_order_exists')
                    )

            test_report_data['test'] = lab_test_order.name.id
            test_report_data['patient'] = lab_test_order.patient_id.id
            if lab_test_order.doctor_id:
                test_report_data['requestor'] = lab_test_order.doctor_id.id
            test_report_data['date_requested'] = lab_test_order.date
            test_report_data['request_order'] = lab_test_order.request
            test_report_data['symptoms'] = lab_test_order.symptoms
            test_report_data['origin_request'] = lab_test_order.origin_request

            for critearea in lab_test_order.name.critearea:
                test_cases.append(('create', [{
                        'name': critearea.name,
                        'sequence': critearea.sequence,
                        'lower_limit': critearea.lower_limit,
                        'upper_limit': critearea.upper_limit,
                        'normal_range': critearea.normal_range,
                        'units': critearea.units and critearea.units.id,
                    }]))
            test_report_data['critearea'] = test_cases

            tests_report_data.append(test_report_data)

        Lab.create(tests_report_data)
        TestRequest.write(tests, {'state': 'ordered'})

        return 'end'

# Añadimos nuevo campo de texto libre "context_detail"
# Añadimos nuevo campo de texto libre "origin_request"
# El campo "servicio" es obligatorio si la petición se hace desde hospitalización
class RequestPatientImagingTestStart(ModelView):
    'Request Patient Imaging Test Start'
    __name__ = 'gnuhealth.patient.imaging.test.request.start'

    context_detail = fields.Char("Anatomical Detail")

    origin_request = fields.Char("Origin of the request")

    @staticmethod
    def default_patient():
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        Surgery = pool.get('gnuhealth.surgery')
        Inpatient = pool.get('gnuhealth.inpatient.registration')

        if Transaction().context.get('active_model') == 'gnuhealth.patient':
            patient_id = Transaction().context.get('active_id')
            return patient_id

        if Transaction().context.get('active_model') == 'gnuhealth.appointment':
            appointment_id = Transaction().context.get('active_id')
            appointment = Appointment.search([('id','=',appointment_id)])[0]
            if appointment.patient:
                return appointment.patient.id

        if Transaction().context.get('active_model') == 'gnuhealth.surgery':
            surgery_id = Transaction().context.get('active_id')
            surgery = Surgery.search([('id','=',surgery_id)])[0]
            if surgery.patient:
                return surgery.patient.id

        if Transaction().context.get('active_model') == 'gnuhealth.inpatient.registration':
            inpatient_id = Transaction().context.get('active_id')
            inpatient = Inpatient.search([('id','=',inpatient_id)])[0]
            if inpatient.patient:
                return inpatient.patient.id

    @staticmethod
    def default_origin_request():

        if Transaction().context.get('active_model') == 'gnuhealth.patient':
            return 'Secrétariat'

        if Transaction().context.get('active_model') == 'gnuhealth.appointment':
            if Transaction().context.get('urgency') == 'c':
                return 'Urgences'
            else:
                return 'Consultation'

        if Transaction().context.get('active_model') == 'gnuhealth.surgery':
            return 'Chirurgie'

        if Transaction().context.get('active_model') == 'gnuhealth.inpatient.registration':
            return 'Hospitalisation'

        return 'Consultation'

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.date.required = True
        cls.ungroup_tests.readonly = True
        cls.service.required = True

        cls.tests.context.update({
            'modelo': 'gnuhealth.patient.imaging.test.request.start',
            'patient': Eval('patient', 0)
            })
        cls.tests.depends.append('patient')

        cls.tests.domain=[('product.template.account_category', '>', 0),
                          ('OR',
                           [('catalog_name', '=', 'Assurance'),
                            ('product.template.is_for_insured', '=', True)
                           ],
                           [('catalog_name', '=', 'Visite'),
                            ('product.template.is_for_visit', '=', True)
                           ],
                           [('catalog_name', '=', 'Particular'),
                            ('product.template.is_for_particular', '=', True)
                           ]
                          )
                         ]

class RequestPatientImagingTest(Wizard):
    'Request Patient Imaging Test'
    __name__ = 'gnuhealth.patient.imaging.test.request'

    start = StateView('gnuhealth.patient.imaging.test.request.start',
        'health_imaging.patient_imaging_test_request_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Request', 'request', 'tryton-ok', default=True),
            ])

    request = StateTransition()

    def generate_code(self, **pattern):
        Config = Pool().get('gnuhealth.sequences')
        config = Config(1)
        sequence = config.get_multivalue(
            'imaging_req_seq', **pattern)
        if sequence:
            return sequence.get()

    def append_services(self, imgtest, service):
        """ If the ungroup flag is not set, append the img test
            to the associated health service
        """
        HealthService = Pool().get('gnuhealth.health_service')

        hservice = []

        service_data = {}
        service_lines = []

        # Add the imgtest to the service document

        service_lines.append(('create', [{
            'product': imgtest.product.id,
            'desc': imgtest.product.rec_name,
            'qty': 1
            }]))

        hservice.append(service)
        service_data['service_line'] = service_lines

        HealthService.write(hservice, service_data)

    def transition_request(self):
        ImagingTestRequest = Pool().get('gnuhealth.imaging.test.request')
        request_number = self.generate_code()
        imaging_tests = []
        for test in self.start.tests:
            imaging_test = {}
            imaging_test['request'] = request_number
            imaging_test['requested_test'] = test.id
            imaging_test['patient'] = self.start.patient.id
            if self.start.doctor:
                imaging_test['doctor'] = self.start.doctor.id
            if self.start.context:
                imaging_test['context'] = self.start.context.id
            imaging_test['context_detail'] = self.start.context_detail
            imaging_test['origin_request'] = self.start.origin_request
            imaging_test['date'] = self.start.date
            imaging_test['urgent'] = self.start.urgent
            if self.start.service:
                imaging_test['service'] = self.start.service.id
                # Append the test directly to the health service document
                # if the Ungroup flag is not set (default).
                if not self.start.ungroup_tests:
                    self.append_services(test, self.start.service)

            imaging_tests.append(imaging_test)
        ImagingTestRequest.create(imaging_tests)
        return 'end'

# Se modifica para pasar los nuevos campos de la solicitud al resultado del test
class WizardGenerateResult(Wizard):
    'Generate Results'
    __name__ = 'wizard.generate.result'
    start_state = 'open_'
    open_ = StateAction('health_imaging.act_imaging_test_result_view')

    def do_open_(self, action):
        pool = Pool()
        Request = pool.get('gnuhealth.imaging.test.request')
        Result = pool.get('gnuhealth.imaging.test.result')

        request_data = []
        requests = Request.browse(Transaction().context.get('active_ids'))
        for request in requests:
            request_data.append({
                'patient': request.patient.id,
                'date': datetime.now(),
                'request_date': request.date,
                'requested_test': request.requested_test,
                'request': request.id,
                'order': request.request,
                'doctor': request.doctor,
                'origin_request': request.origin_request})
        results = Result.create(request_data)

        action['pyson_domain'] = PYSONEncoder().encode(
            [('id', 'in', [r.id for r in results])])

        Request.requested(requests)
        Request.done(requests)
        return action, {}

# Se añade nombre del paciente a la cabecera de la vista de resultados
class OpenAppointmentLabResults(Wizard):
    'Open Appointment Lab Results'
    __name__ = 'wizard.gnuhealth.appointment.lab_results'

    start_state = 'appointment_lab_results'
    appointment_lab_results = StateAction('health_ebome.act_app_lab_results_ebome')

    def do_appointment_lab_results(self, action):
        pool = Pool()
        LabResults = pool.get('gnuhealth.lab')

        appointment = Transaction().context.get('active_id')

        try:
            app_id = \
                Pool().get('gnuhealth.appointment').browse([appointment])[0]
        except:
            raise NoRecordSelected(
                gettext('health_cameroun.msg_no_record_selected'))

        try:
            patient_id = app_id.patient.id
        except:
            raise NoPatient(gettext('health_cameroun.msg_no_patient'))

        labresults = LabResults.search(['patient','=',patient_id])

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',app_id.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': app_id.patient.id,
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': app_id.patient.rec_name,
            }

        data = {'res_id': [x.id for x in labresults]}
        return action, data

# Se añade consulta de resultados de test de rayos a las acciones de rendez_vous
# Se añade nombre del paciente a la cabecera de la vista de resultados
class OpenAppointmentImgResults(Wizard):
    'Open Appointment Imaging Results'
    __name__ = 'wizard.gnuhealth.appointment.img_results'

    start_state = 'appointment_img_results'
    appointment_img_results = StateAction('health_ebome.act_app_img_results')

    def do_appointment_img_results(self, action):
        pool = Pool()
        ImgResults = pool.get('gnuhealth.imaging.test.result')

        appointment = Transaction().context.get('active_id')

        try:
            app_id = \
                Pool().get('gnuhealth.appointment').browse([appointment])[0]
        except:
            raise NoRecordSelected(
                gettext('health_ebome.msg_no_record_selected'))

        try:
            patient_id = app_id.patient.id
        except:
            raise NoPatient(gettext('health_ebome.msg_no_patient'))

        imgresults = ImgResults.search(['patient','=',patient_id])

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',app_id.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': app_id.patient.id,
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': app_id.patient.rec_name,
            }

        data = {'res_id': [x.id for x in imgresults]}
        return action, data

#   SURGERY #

# Las peticiones de test de laboratorio y de imagen son las mismas que para Appointment

# Se añade consulta de resultados de test de laboratorio a acciones de surgery
class OpenSurgeryLabResults(Wizard):
    'Open Surgery Lab Results'
    __name__ = 'wizard.gnuhealth.surgery.lab_results'

    start_state = 'surgery_lab_results'
    surgery_lab_results = StateAction('health_ebome.act_surgery_lab_results')

    def do_surgery_lab_results(self, action):
        pool = Pool()
        LabResults = pool.get('gnuhealth.lab')

        surgery = Transaction().context.get('active_id')

        try:
            surgery_id = \
                Pool().get('gnuhealth.surgery').browse([surgery])[0]
        except:
            raise NoRecordSelected(
                gettext('health_ebome.msg_no_record_selected'))

        try:
            patient_id = surgery_id.patient.id
        except:
            raise NoPatient(gettext('health_ebome.msg_no_patient'))

        labresults = LabResults.search(['patient','=',patient_id])

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',surgery_id.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': surgery_id.patient.id,
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': surgery_id.patient.rec_name,
            }

        data = {'res_id': [x.id for x in labresults]}
        return action, data

# Se añade consulta de resultados de test de rayos a las acciones de surgery
class OpenSurgeryImgResults(Wizard):
    'Open Surgery Imaging Results'
    __name__ = 'wizard.gnuhealth.surgery.img_results'

    start_state = 'surgery_img_results'
    surgery_img_results = StateAction('health_ebome.act_surgery_img_results')

    def do_surgery_img_results(self, action):
        pool = Pool()
        ImgResults = pool.get('gnuhealth.imaging.test.result')

        surgery = Transaction().context.get('active_id')

        try:
            surgery_id = \
                Pool().get('gnuhealth.surgery').browse([surgery])[0]
        except:
            raise NoRecordSelected(
                gettext('health_ebome.msg_no_record_selected'))

        try:
            patient_id = surgery_id.patient.id
        except:
            raise NoPatient(gettext('health_ebome.msg_no_patient'))

        imgresults = ImgResults.search(['patient','=',patient_id])

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',surgery_id.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': surgery_id.patient.id,
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': surgery_id.patient.rec_name,
            }

        data = {'res_id': [x.id for x in imgresults]}
        return action, data

#   HOSPITALIZACIÓN #

# Realiza en un único paso la petición de test de laboratorio y la generación de registro de resultado del test,
# para que éste pase directamente a laboratorio sin pasar por secretaría
class RequestInpatientLabTest(Wizard):
    'Request Patient Lab Test'
    __name__ = 'gnuhealth.inpatient.lab.test.request'

    start = StateView(
        'gnuhealth.patient.lab.test.request.start',
        'health_lab.patient_lab_test_request_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Request', 'request', 'tryton-ok', default=True),
            ])
    request = StateTransition()

    def generate_code(self, **pattern):
        Config = Pool().get('gnuhealth.sequences')
        config = Config(1)
        sequence = config.get_multivalue(
            'lab_request_sequence', **pattern)
        if sequence:
            return sequence.get()

    def append_services(self, labtest, service):
        """ If the ungroup flag is not set, append the lab test
            to the associated health service
        """
        HealthService = Pool().get('gnuhealth.health_service')

        hservice = []

        service_data = {}
        service_lines = []

        # Add the labtest to the service document

        service_lines.append(('create', [{
            'product': labtest.product_id.id,
            'desc': labtest.product_id.rec_name,
            'qty': 1
            }]))

        hservice.append(service)
        service_data['service_line'] = service_lines

        HealthService.write(hservice, service_data)

    def transition_request(self):
        PatientLabTest = Pool().get('gnuhealth.patient.lab.test')
        Lab = Pool().get('gnuhealth.lab')

        request_number = self.generate_code()
        lab_tests = []
        for test in self.start.tests:
            lab_test = {}
            lab_test['request'] = request_number
            lab_test['name'] = test.id
            lab_test['patient_id'] = self.start.patient.id
            if self.start.doctor:
                lab_test['doctor_id'] = self.start.doctor.id
            lab_test['symptoms'] = self.start.symptoms
            lab_test['origin_request'] = self.start.origin_request
            lab_test['date'] = self.start.date
            lab_test['urgent'] = self.start.urgent

            if self.start.service:
                lab_test['service'] = self.start.service.id
                # Append the test directly to the health service document
                # if the Ungroup flag is not set (default).
                if not self.start.ungroup_tests:
                    self.append_services(test, self.start.service)
            lab_tests.append(lab_test)

        requests = PatientLabTest.create(lab_tests)

        tests_report_data = []

        for lab_test_order in requests:

            test_cases = []
            test_report_data = {}

            if lab_test_order.state == 'ordered':
                raise LabOrderExists(
                    gettext('health_lab.msg_lab_order_exists')
                    )

            test_report_data['test'] = lab_test_order.name.id
            test_report_data['patient'] = lab_test_order.patient_id.id
            if lab_test_order.doctor_id:
                test_report_data['requestor'] = lab_test_order.doctor_id.id
            test_report_data['date_requested'] = lab_test_order.date
            test_report_data['request_order'] = lab_test_order.request
            test_report_data['symptoms'] = lab_test_order.symptoms
            test_report_data['origin_request'] = lab_test_order.origin_request

            for critearea in lab_test_order.name.critearea:
                test_cases.append(('create', [{
                        'name': critearea.name,
                        'sequence': critearea.sequence,
                        'lower_limit': critearea.lower_limit,
                        'upper_limit': critearea.upper_limit,
                        'normal_range': critearea.normal_range,
                        'units': critearea.units and critearea.units.id,
                    }]))
            test_report_data['critearea'] = test_cases

            tests_report_data.append(test_report_data)

        Lab.create(tests_report_data)
        PatientLabTest.write(requests, {'state': 'ordered'})

        return 'end'

# Se añade consulta de resultados de test de laboratorio a acciones de hospitalización
class OpenInpatientLabResults(Wizard):
    'Open Inpatient Lab Results'
    __name__ = 'wizard.gnuhealth.inpatient.lab_results'

    start_state = 'inpatient_lab_results'
    inpatient_lab_results = StateAction('health_ebome.act_inpatient_lab_results')

    def do_inpatient_lab_results(self, action):
        pool = Pool()
        LabResults = pool.get('gnuhealth.lab')

        inpatient = Transaction().context.get('active_id')

        try:
            inpatient_id = \
                Pool().get('gnuhealth.inpatient.registration').browse([inpatient])[0]
        except:
            raise NoRecordSelected(
                gettext('health_ebome.msg_no_record_selected'))

        try:
            patient_id = inpatient_id.patient.id
        except:
            raise NoPatient(gettext('health_ebome.msg_no_patient'))

        labresults = LabResults.search(['patient','=',patient_id])

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',inpatient_id.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': inpatient_id.patient.id,
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': inpatient_id.patient.rec_name,
            }

        data = {'res_id': [x.id for x in labresults]}
        return action, data

# Realiza en un único paso la petición de test de imagen y la generación de registro de resultado del test,
# para que éste pase directamente a rayos sin pasar por secretaría
class RequestInpatientImagingTest(Wizard):
    'Request Patient Imaging Test'
    __name__ = 'gnuhealth.inpatient.imaging.test.request'

    start = StateView('gnuhealth.patient.imaging.test.request.start',
        'health_imaging.patient_imaging_test_request_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Request', 'request', 'tryton-ok', default=True),
            ])
    request = StateTransition()

    def generate_code(self, **pattern):
        Config = Pool().get('gnuhealth.sequences')
        config = Config(1)
        sequence = config.get_multivalue(
            'imaging_req_seq', **pattern)
        if sequence:
            return sequence.get()

    def append_services(self, imgtest, service):
        HealthService = Pool().get('gnuhealth.health_service')

        hservice = []

        service_data = {}
        service_lines = []

        # Add the imgtest to the service document

        service_lines.append(('create', [{
            'product': imgtest.product.id,
            'desc': imgtest.product.rec_name,
            'qty': 1
            }]))

        hservice.append(service)
        service_data['service_line'] = service_lines

        HealthService.write(hservice, service_data)

    def transition_request(self):
        ImagingTestRequest = Pool().get('gnuhealth.imaging.test.request')
        Result = Pool().get('gnuhealth.imaging.test.result')
        request_number = self.generate_code()
        imaging_tests = []
        for test in self.start.tests:
            imaging_test = {}
            imaging_test['request'] = request_number
            imaging_test['requested_test'] = test.id
            imaging_test['patient'] = self.start.patient.id
            imaging_test['state'] = 'done'
            if self.start.doctor:
                imaging_test['doctor'] = self.start.doctor.id
            if self.start.context:
                imaging_test['context'] = self.start.context.id
            imaging_test['context_detail'] = self.start.context_detail
            imaging_test['origin_request'] = self.start.origin_request
            imaging_test['date'] = self.start.date
            imaging_test['urgent'] = self.start.urgent
            if self.start.service:
                imaging_test['service'] = self.start.service.id
                # Append the test directly to the health service document
                # if the Ungroup flag is not set (default).
                if not self.start.ungroup_tests:
                    self.append_services(test, self.start.service)

            imaging_tests.append(imaging_test)
        requests = ImagingTestRequest.create(imaging_tests)

        request_data = []
        for request in requests:
            request_data.append({
                'patient': request.patient.id,
                'date': datetime.now(),
                'request_date': request.date,
                'requested_test': request.requested_test,
                'request': request.id,
                'order': request.request,
                'doctor': request.doctor,
                'origin_request': request.origin_request})
        Result.create(request_data)

        return 'end'

# Se añade consulta de resultados de test de rayos a acciones de hospitalización
class OpenInpatientImgResults(Wizard):
    'Open Inpatient Imaging Results'
    __name__ = 'wizard.gnuhealth.inpatient.img_results'

    start_state = 'inpatient_img_results'
    inpatient_img_results = StateAction('health_ebome.act_inpatient_img_results')

    def do_inpatient_img_results(self, action):
        pool = Pool()
        ImgResults = pool.get('gnuhealth.imaging.test.result')

        inpatient = Transaction().context.get('active_id')

        try:
            inpatient_id = \
                Pool().get('gnuhealth.inpatient.registration').browse([inpatient])[0]
        except:
            raise NoRecordSelected(
                gettext('health_ebome.msg_no_record_selected'))

        try:
            patient_id = inpatient_id.patient.id
        except:
            raise NoPatient(gettext('health_ebome.msg_no_patient'))

        imgresults = ImgResults.search(['patient','=',patient_id])

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',inpatient_id.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': inpatient_id.patient.id,
            })
        action['name'] += ' (%(patient)s)' % {
            'patient': inpatient_id.patient.rec_name,
            }

        data = {'res_id': [x.id for x in imgresults]}
        return action, data
