from datetime import datetime
from trytond.model import ModelView, fields
from trytond.pyson import Eval, Or
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateAction, Button, StateReport
from pprint import pprint

class CreateDebtorsReportEbomeStart(ModelView):
    'Debtors Report Create Start'
    __name__ = 'debtors.report.create.start'

    start_date = fields.Date('Start Date', required=True)

    end_date = fields.Date('End Date', required=True)

    report_type = fields.Selection([
        (None, ''),
        ('prenatal', 'Mother-Child Program'),
        ('rest', 'Rest')
        ], 'Report Type', sort=False, required=True)

    @classmethod
    def default_start_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def default_end_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

class CreateDebtorsReportEbome(Wizard):
    'Debtors Report Create'
    __name__ = 'debtors.report.create'

    start = StateView('debtors.report.create.start',
        'health_ebome.view_create_debtors_ebome_start', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_ebome.report_debtors_ebome_from_wizard')

    def default_start(self, fields):
        res = { }
        return res

    def do_create_report(self, action):
        start = self.start.start_date
        end = self.start.end_date

        data = {
            'start_date' : start.strftime("%d/%m/%Y"),
            'end_date' : end.strftime("%d/%m/%Y")
            }

        if self.start.report_type == "prenatal":
            data['title'] = 'Rapport de patients débiteurs de PMI'
        elif self.start.report_type == "rest":
            data['title'] = 'Rapport de patients débiteurs, autres que PMI'

        results = {}
        total = {}
        total_with_values = {}
        errors = []
        total_amount = 0
        pool = Pool()

        Tarif_types = pool.get('gnuhealth.insurance.plan')

        selected_tarif_types = Tarif_types.search([])
        for tarif_type in selected_tarif_types:
            total[tarif_type.id] = {'tarif_type' : tarif_type.company.name + ':' + tarif_type.name.template.name, 'amount' : 0}

        # Get invoices between dates in state posted
        Invoice = pool.get('account.invoice')
        selected_invoices = Invoice.search([
            ('invoice_date','>=',start),
            ('invoice_date','<=',end),
            ('state','=', 'posted')],
            order=[('party', 'ASC'), ('invoice_date', 'ASC')])

        for invoice in selected_invoices:
            p_type = False
            if invoice.party:
                if len(invoice.party.insurance) > 0:
                    p_type = invoice.party.insurance[0].plan_id
                else:
                    errors.append("Without Insurance " + str(invoice.party.code))
            else:
                errors.append("Without party " + str(invoice.rec_name))
            if invoice.amount_to_pay and\
               invoice.amount_to_pay > 0:
                amount_336 = 0
                amount_to_pay = 0
                for line in invoice.lines:
                    if line.type == 'line':
                        if line.product.code == '336': #código correspondiente al servicio "PMI - Consultation prénatale + accouchement"
                            amount_336 += line.amount
                if self.start.report_type == 'rest':
                    if amount_336 == 0:
                        amount_to_pay = invoice.amount_to_pay
                    elif invoice.total_amount > amount_336:
                        rest_amount = invoice.total_amount - amount_336
                        if invoice.amount_to_pay > rest_amount:
                            amount_to_pay = rest_amount
                        elif invoice.amount_to_pay <= rest_amount:
                            amount_to_pay = invoice.amount_to_pay
                elif self.start.report_type == 'prenatal':
                    if amount_336 != 0:
                        if invoice.total_amount > amount_336:
                            rest_amount = invoice.total_amount - amount_336
                            amount_to_pay = invoice.amount_to_pay - rest_amount
                        elif invoice.total_amount <= amount_336:
                            amount_to_pay = invoice.amount_to_pay
                if amount_to_pay != 0:
                    results[invoice.number] = {'invoice_date' : 0, 'invoice_party_code' : ' ', 'invoice_party' : ' ', 'invoice_party_tlf' : ' ', 'tarif_type' : ' ', 'invoice_total_amount' : 0, 'invoice_amount_to_pay' : 0}
                    results[invoice.number]['invoice_date'] = invoice.invoice_date.strftime("%d/%m/%Y")
                    results[invoice.number]['invoice_party_code'] = invoice.party.code
                    results[invoice.number]['invoice_party'] = invoice.party.rec_name
                    if invoice.party.contact_mechanisms:
                        results[invoice.number]['invoice_party_tlf'] = invoice.party.contact_mechanisms[0].value \
                            or ''
                    if p_type:
                        results[invoice.number]['tarif_type'] = p_type.company.name + ':' + p_type.name.template.name
                    results[invoice.number]['invoice_total_amount'] = invoice.total_amount
                    results[invoice.number]['invoice_amount_to_pay'] = amount_to_pay
                    if p_type:
                        total[p_type.id]['amount'] += amount_to_pay
                    total_amount += amount_to_pay

        for tarif_type in selected_tarif_types:
            if total[tarif_type.id]['amount'] > 0:
                total_with_values[tarif_type.id] = {'tarif_type' : total[tarif_type.id]['tarif_type'],
                                                    'amount' : total[tarif_type.id]['amount']}

        data['results'] = results
        data['total'] = total_with_values
        data['total_amount'] = total_amount
        data['errors'] = errors

        return action, data
