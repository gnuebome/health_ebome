# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2022 Luis Falcon <lfalcon@gnuhealth.org>
#    Copyright (C) 2013  Sebastián Marro <smarro@thymbra.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from decimal import Decimal
from datetime import *

from trytond.i18n import gettext
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateAction, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.pyson import Eval, PYSONEncoder
from trytond.pool import Pool

from trytond.modules.health.core import get_health_professional

from ..exceptions import NoInvoiceAddress, NoPaymentTerm, NoLineToInvoice, ServiceAlreadyInvoiced, ServiceWithoutDiagnosis, InsuranceTarifNotFound, InvoceTypeError

# Se hace nuevo wizard de crear factura desde vista de servicios para que la cree directamente, sin presentar pantalla preguntando si se quiere crear la factura
# El servicio queda en borrador
# Además, si se trata de un paciente asegurado, se generará otra factura por el importe no cubierto por el seguro cuyo pagador será el titular del seguro
class SetPricesProduct(Wizard):
    'Set Prices For A Product'
    __name__ = 'gnuhealth.set.prices.product'

#  Se obtienen los descuentos por defecto de los planes que no estén ya en gnuhealth.insurance.plan.product.policy
#  para el producto pasado como argumento

    def set_discounts(self, product):
        pool = Pool()
        Plan = pool.get('gnuhealth.insurance.plan')
        Policy = pool.get('gnuhealth.insurance.plan.product.policy')

        discount_policies = []
        discount_plans = []
        policies = Policy.search([('product', '=', product.id)])
        for policy in policies:
            discount_policies.append(policy)
            discount_plans.append(policy.plan.id)

        created_discounts = {}
        product_discounts = []

        created_discounts['product'] = product.id

        plans = Plan.search([])
        for plan in plans:
            if plan.id in discount_plans:
                i = discount_plans.index(plan.id)
                if plan.company.name == 'Assurance':
                    Policy.delete([discount_policies[i]])
                else:
                    if product.template.list_price == 0:
                        Policy.delete([discount_policies[i]])
                    else:
                        pass
            else:
                if plan.company.name == 'Assurance':
                    pass
                else:
                    if plan.company.insurance_company_type == 't':
                        if product.template.is_for_particular == True:
                            if plan.discount_by_default == None:
                                plan_discount = 0
                            else:
                                plan_discount = plan.discount_by_default
                            Policy.create([{
                                            'discount': plan_discount,
                                            'plan': plan.id,
                                            'product': product.id,
                                            'product_category': None
                                         }])
                            product_discounts.append([{
                                                       'discount' : plan_discount,
                                                       'plan' : plan.id
                                                    }])
                    if plan.company.insurance_company_type == 'x':  #Para aseguradoras sólo se trata el plan de tarifa única
                        if product.template.is_for_insured == True:
                            if plan.discount_by_default == None:
                                plan_discount = 0
                            else:
                                plan_discount = plan.discount_by_default
                            Policy.create([{
                                            'discount': plan_discount,
                                            'plan': plan.id,
                                            'product': product.id,
                                            'product_category': None
                                         }])
                            product_discounts.append([{
                                                       'discount' : plan_discount,
                                                       'plan' : plan.id
                                                    }])
                    if plan.company.insurance_company_type == 'v':
                        if product.template.is_for_visit == True:
                            if plan.discount_by_default == None:
                                plan_discount = 0
                            else:
                                plan_discount = plan.discount_by_default
                            Policy.create([{
                                            'discount': plan_discount,
                                            'plan': plan.id,
                                            'product': product.id,
                                            'product_category': None
                                        }])
                            product_discounts.append([{
                                                       'discount' : plan_discount,
                                                       'plan' : plan.id
                                                    }])

        created_discounts['discounts'] = product_discounts
        return created_discounts

    start_state = 'set_prices_product'
    set_prices_product = StateAction('health_ebome.act_set_discounts_form')

    def do_set_prices_product(self, action):

        pool = Pool()
        Product = pool.get('product.product')
        Policy = pool.get('gnuhealth.insurance.plan.product.policy')

        products = Product.browse(Transaction().context.get(
            'active_ids'))
        discounts = []

        for product in products:
            created_discounts = self.set_discounts(product)
            discounts.append(created_discounts)

        action['pyson_domain'] = PYSONEncoder().encode([
            ('product', '=', created_discounts['product'])
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'product': created_discounts['product'],
            })
        action['name'] += ' (%(product)s)' % {
            'product': product.rec_name,
            }

        return action, {}


# Wizard igual que el anterior pero para plantillas de productos (templates)
# Se tratan todos los productos con la misma plantilla
class SetPricesTemplate(Wizard):
    'Set Prices For A Product Template'
    __name__ = 'gnuhealth.set.prices.template'


    start_state = 'set_prices_template'
    set_prices_template = StateAction('health_ebome.act_set_discounts_form')

    def do_set_prices_template(self, action):

        pool = Pool()
        Template = pool.get('product.template')
        Product = pool.get('product.product')
        Policy = pool.get('gnuhealth.insurance.plan.product.policy')

        templates = Template.browse(Transaction().context.get(
            'active_ids'))

        discounts = []

        for template in templates:
            products = Product.search([('template', '=', template.id)])
            for product in products:
                created_discounts = SetPricesProduct.set_discounts(self, product)
                discounts.append(created_discounts)

        action['pyson_domain'] = PYSONEncoder().encode([
            ('product', '=', created_discounts['product'])
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'product': created_discounts['product'],
            })
        action['name'] += ' (%(template)s)' % {
            'template': template.rec_name,
            }

        return action, {}
