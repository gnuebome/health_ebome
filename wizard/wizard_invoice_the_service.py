# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2022 Luis Falcon <lfalcon@gnuhealth.org>
#    Copyright (C) 2013  Sebastián Marro <smarro@thymbra.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from decimal import Decimal
from datetime import *

from trytond.i18n import gettext
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateAction, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.pyson import Eval, PYSONEncoder
from trytond.pool import Pool

from trytond.modules.health.core import get_health_professional

from ..exceptions import NoInvoiceAddress, NoPaymentTerm, NoLineToInvoice, ServiceAlreadyInvoiced, ServiceWithoutDiagnosis, InsuranceTarifNotFound, InvoceTypeError

# Se hace nuevo wizard de crear factura desde vista de servicios para que la cree directamente, sin presentar pantalla preguntando si se quiere crear la factura
# El servicio queda en borrador
# Además, si se trata de un paciente asegurado, se generará otra factura por el importe no cubierto por el seguro cuyo pagador será el titular del seguro
class InvoiceTheService(Wizard):
    'Create Service Invoice'
    __name__ = 'gnuhealth.invoice.the.service'

#  Si el tipo de tipo de tarifa es "i" (es un seguro), se busca la tarifa común a todos los seguros
#  (tipo de tarifa con tipo de tipo de tarifa igual a "x"
#  name: CreateServiceInvoice.discount_policy
#  @param : Insurance, service line product
#  @return : Policy applied to that service line

    def discount_policy(self, tarif_type, product, company):
        pool = Pool()
        Party = pool.get('party.party')
        Insurance_plan = pool.get('gnuhealth.insurance.plan')
        Code_category = pool.get('product.template-product.category')

        discount = {}
        if tarif_type.company.insurance_company_type == 'i'\
        or tarif_type.company.insurance_company_type == 'c':
            insurance_tarifs = Party.search(['insurance_company_type', '=', 'x'])
            if insurance_tarifs:
                insurance_tarif = insurance_tarifs[0]
                insurance_plan, = Insurance_plan.search([('company', '=', insurance_tarif.id)])
                plan_id = insurance_plan
            else:
                raise InsuranceTarifNotFound(
                    gettext('health_ebome.msg_insurance_tarif_not_found'))
            if product.template.account_category.name == 'Radiologie'\
            or product.template.account_category.name == 'Echographie':
                code_categories = Code_category.search(['template', '=', product.template.id])
                for code_category in code_categories:
                    if code_category.category.name == 'HG Douala (K)':
                        discount['k_code_factor'] = company.k_code_factor
            if product.template.account_category.name == 'Laboratoire':
                code_categories = Code_category.search(['template', '=', product.template.id])
                for code_category in code_categories:
                    if code_category.category.name == 'Syndilab (B)':
                        discount['b_code_factor'] = company.b_code_factor
            if product.template.account_category.name == 'Hospitalisation':
                code_categories = Code_category.search(['template', '=', product.template.id])
                for code_category in code_categories:
                    if code_category.category.name == 'AMI':
                        discount['ami_code_factor'] = company.ami_code_factor
        elif tarif_type.plan_id:
            plan_id = tarif_type.plan_id

        if plan_id.product_policy:
            for policy in plan_id.product_policy:
                # Check first for product
                if (product == policy.product):
                    if policy.discount:
                        discount['value'] = policy.discount
                        discount['type'] = 'pct'
                        return discount
                    else:
                        discount['value'] = 0
                        discount['type'] = 'pct'
                        return discount

            for policy in plan_id.product_policy:
                # Then, if there's no product, check for the category
                if (policy.product_category in product.categories):
                    if policy.discount:
                        discount['value'] = policy.discount
                        discount['type'] = 'pct'
                        return discount
                    else:
                        discount['value'] = 0
                        discount['type'] = pct
                        return discount

        return discount

#  Cálculo de precios y construcción de la descripción en función de descuentos y cobertura de seguros

    def price_and_desc(self, service, base_price, discount, currency, desc, step):

        price_and_desc = {}

        if discount['value']:
            unit_price = base_price * (1 - Decimal(str(discount['value']/100)))
        else:
            unit_price = base_price

        if step == 'first':
            if 'b_code_factor' in list(discount.keys()):
                if discount['b_code_factor']:
                    b_code_units = Decimal(unit_price/discount['b_code_factor'])
                    b_code_units = round(b_code_units, 0)
                    desc = desc + ' (Unités de code B = ' + str(b_code_units) + ')'
            if 'k_code_factor' in list(discount.keys()):
                if discount['k_code_factor']:
                    k_code_units = Decimal(unit_price/discount['k_code_factor'])
                    k_code_units = round(k_code_units, 0)
                    desc = desc + ' (Unités de code K = ' + str(k_code_units) + ')'
            if 'ami_code_factor' in list(discount.keys()):
                if discount['ami_code_factor']:
                    ami_code_units = Decimal(unit_price/discount['ami_code_factor'])
                    ami_code_units = round(ami_code_units, 0)
                    desc = desc + ' (Unités de code AMI = ' + str(ami_code_units) + ')'

        if service.insurance_plan.company.insurance_company_type == 'i'\
        or service.insurance_plan.company.insurance_company_type == 'c':
            base_price = unit_price
            if step == 'first':     # En el primer paso se genera la factura para la compañía aseguradora
                if service.insurance_plan.plan_id.coverage < 100:
                    unit_price *= Decimal(str(service.insurance_plan.plan_id.coverage/100))
            else:                   # En el segundo paso se genera la factura por la parte que no cubre la compañía aseguradora
                if service.insurance_plan.plan_id.coverage < 100:
                    unit_price *= 1 - Decimal(str(service.insurance_plan.plan_id.coverage/100))

        # Round to avoid error on sig figs at invoice.
        base_price = currency.round(base_price)
        unit_price = currency.round(unit_price)

        price_and_desc['base_price'] = base_price
        price_and_desc['unit_price'] = unit_price
        price_and_desc['desc'] = desc
        price_and_desc['step'] = step
        return price_and_desc

#  Generación de facturas
#  Si paso igual a 1, se genera la factura principal
#  Si paso igual a 2 (solo para seguros), se genera la factura por el porcentaje que no cubre el seguro

    def create_invoice(self, service, party, step, two_invoices):
        pool = Pool()
        Config = pool.get('account.configuration')
        Journal = pool.get('account.journal')
        Party = pool.get('party.party')

        currency_id = Transaction().context.get('currency')

        config = Config(1)

        invoice = {}
        invoice_data = {}

        invoice_data['description'] = service.desc
        invoice_data['party'] = party.id
        invoice_data['type'] = 'out'
        invoice_data['invoice_date'] = date.today()
        invoice_data['account'] = \
            party.account_receivable and \
            party.account_receivable.id or \
            config.default_account_receivable.id
        invoice_data['company'] = service.company.id
        invoice_data['diagnosis'] = service.diagnosis or ' '
        invoice_data['date_of_entry'] = service.date_of_entry or\
            service.service_date
        invoice_data['exit_date'] = service.exit_date or\
            service.service_date

        ctx = {}
        sale_price_list = None
        if hasattr(party, 'sale_price_list'):
            sale_price_list = party.sale_price_list

        if sale_price_list:
            ctx['price_list'] = sale_price_list.id
            ctx['sale_date'] = date.today()
            ctx['currency'] = currency_id
            ctx['customer'] = party.id

        journals = Journal.search([
            ('type', '=', 'revenue'),
            ], limit=1)

        if journals:
            journal, = journals
        else:
            journal = None

        invoice_data['journal'] = journal.id

        party_address = Party.address_get(party, type='invoice')
        if not party_address:
            raise NoInvoiceAddress(
                gettext('health_ebome.msg_no_invoice_address'))

        invoice_data['invoice_address'] = party_address.id
        invoice_data['reference'] = service.name

        if not (party.customer_payment_term or
            config.default_customer_payment_term):
            raise NoPaymentTerm(
                gettext('health_ebome.msg_no_payment_term'))

        invoice_data['payment_term'] = \
            party.customer_payment_term and \
            party.customer_payment_term.id or \
            config.default_customer_payment_term.id

        #Invoice Lines
        seq = 0
        if step == 'last'\
        or two_invoices == False:
            all_invoiced = True
            none_to_invoice = True

        invoice_lines = []
        account_category = ""
        subtotal = 0
        for line in service.service_line:
            seq = seq + 1
            account = line.product.template.account_revenue_used.id

            if sale_price_list:
                with Transaction().set_context(ctx):
                    base_price = line.product.list_price
                    unit_price = sale_price_list.compute(party,
                        line.product, line.product.list_price,
                        line.qty, line.product.default_uom)
            else:
                base_price = line.product.list_price
                unit_price = line.product.list_price

            if line.to_invoice:
                if step == 'last'\
                or two_invoices == False:
                    none_to_invoice = False
                taxes = []
                desc = line.desc

                #Include taxes related to the product on the invoice line
                for product_tax_line in line.product.customer_taxes_used:
                    taxes.append(product_tax_line.id)

                # Check the Insurance policy for this service
                if service.insurance_plan:
                    discount = InvoiceTheService.discount_policy(self, service.insurance_plan, line.product, service.company)
                    currency = service.company.currency

                    if discount:
                        if 'value' in list(discount.keys()):
                            if discount['value']:
                                if (discount['type'] == 'pct'):
                                    price_and_desc = InvoiceTheService.price_and_desc(self, service, base_price, discount, currency, desc, step)
                                else:
                                    discount['value'] = 0
                                    price_and_desc = InvoiceTheService.price_and_desc(self, service,base_price, discount, currency, desc,step)
                            else:
                                discount['value'] = 0
                                price_and_desc = InvoiceTheService.price_and_desc(self, service,base_price, discount, currency, desc,step)
                        else:
                            discount['value'] = 0
                            price_and_desc = InvoiceTheService.price_and_desc(self, service,base_price, discount, currency, desc,step)
                    else:
                        discount['value'] = 0
                        price_and_desc = InvoiceTheService.price_and_desc(self, service,base_price, discount, currency, desc,step)
                    base_price = price_and_desc['base_price']
                    unit_price = price_and_desc['unit_price']
                    desc = price_and_desc['desc']

                if account_category != line.product.template.account_category.name:
                    if account_category != "" and \
                        subtotal > 0:
                        invoice_lines.append(('create', [{
                                'origin': str(line),
                                'type': 'subtotal',
                                'description': account_category,
                                'quantity': 1,
                                'unit_price': subtotal,
                                'sequence': seq
                            }]))
                        seq = seq + 1
                    account_category = line.product.template.account_category.name
                    subtotal = 0
                    invoice_lines.append(('create', [{
                            'origin': str(line),
                            'type': 'title',
                            'description': account_category,
                            'sequence': seq
                        }]))
                    seq = seq + 1
                invoice_lines.append(('create', [{
                        'origin': str(line),
                        'type': 'line',
                        'product': line.product.id,
                        'description': desc,
                        'quantity': line.qty,
                        'account': account,
                        'unit': line.product.default_uom.id,
                        'unit_price': unit_price,
                        'sequence': seq,
                        'taxes': [('add', taxes)],
                        'base_price' : base_price,
                    }]))
                subtotal += (line.qty * unit_price)
                if step == 'last'\
                or two_invoices == False:
                    line.to_invoice = False
                    line.action_required = True
                    line.save()
            elif line.action_required == False:
                if step == 'last'\
                or two_invoices == False:
                    all_invoiced = False

        if account_category != "" and \
            subtotal > 0:
            seq = seq + 1
            invoice_lines.append(('create', [{
                    'origin': str(line),
                    'type': 'subtotal',
                    'description': account_category,
                    'quantity': 1,
                    'unit_price': subtotal,
                    'sequence': seq
                }]))

        invoice_data['lines'] = invoice_lines

        if (step == 'last' or two_invoices == False) and none_to_invoice:
            raise NoLineToInvoice(
                gettext('health_ebome.msg_no_line_to_invoice'))

        invoice['invoice_data'] = invoice_data

        # Change to invoiced the status on the service document if all lines invoiced
        if (step == 'last' or two_invoices == False) and all_invoiced:
            invoice['all_invoiced'] = True
        else:
            invoice['all_invoiced'] = False

        return invoice

    start_state = 'invoice_the_service'
    invoice_the_service = StateAction('health_ebome.act_invoice_related_form2')

    def do_invoice_the_service(self, action):

        pool = Pool()
        HealthService = pool.get('gnuhealth.health_service')
        Invoice = pool.get('account.invoice')
        Party = pool.get('party.party')

        services = HealthService.browse(Transaction().context.get(
            'active_ids'))
        invoices = []

        #Invoice Header
        for service in services:
            if service.state == 'invoiced':
                raise ServiceAlreadyInvoiced(
                    gettext('health_ebome.msg_service_already_invoiced'))

            #Determinar quién paga la factura
            if service.insurance_plan.company.insurance_company_type == 'i'\
            or service.insurance_plan.company.insurance_company_type == 'c':
                if service.insurance_plan.plan_id.coverage == 100 \
                or service.insurance_plan.plan_id.remainder_payer != 'p':
                    raise InvoceTypeError(
                        gettext('health_ebome.msg_invoice_type_error1'))
            if service.invoice_to:
                party, = Party.search([('id', '=', service.invoice_to)])
            else:
                party, = Party.search([('id', '=', service.patient.name)])

            invoice = self.create_invoice(service, party, 'last', False)
            invoices.append(invoice['invoice_data'])

        Invoice.update_taxes(Invoice.create(invoices))

        action['pyson_domain'] = PYSONEncoder().encode([
            ('invoice_date', '=', invoice['invoice_data']['invoice_date']),
            ('reference', '=', invoice['invoice_data']['reference']),
            ('state', '=', 'draft'),
            ])

        # Change to invoiced the status on the service document if all lines invoiced
#        if invoice['all_invoiced'] == True:
#            HealthService.write(services, {'state': 'invoiced'})

        return action, {}


# Nuevo Wizard igual que el anterior pero para aseguradoras y compañías que dan cobertura de salud a sus empleados
# El pagador de la factura será la compañía aseguradora
# Se aplicará la tarifa única para todas las aseguradoras
# Una vez aplicada dicha tarifa, se recalculará el precio según la cobertura del seguro
# Se generará otra factura por el importe no cubierto por el seguro cuyo pagador será quien corresponda (el hospital o su empresa). La factura para
# el paciente se genera en el Wizard anterior
class InvoiceTheServiceForInsurance(Wizard):
    'Create Service Invoice For Insurance'
    __name__ = 'gnuhealth.invoice.the.service.for.insurance'


    start_state = 'invoice_the_service_for_insurance'
    invoice_the_service_for_insurance = StateAction('health_ebome.act_invoice_related_form2')

    def do_invoice_the_service_for_insurance(self, action):

        pool = Pool()
        HealthService = pool.get('gnuhealth.health_service')
        Invoice = pool.get('account.invoice')
        Party = pool.get('party.party')

        services = HealthService.browse(Transaction().context.get(
            'active_ids'))
        invoices = []

        #Invoice Header
        for service in services:
            if service.state == 'invoiced':
                raise ServiceAlreadyInvoiced(
                    gettext('health_ebome.msg_service_already_invoiced'))

            #Determinar quién paga la factura
            if service.insurance_plan.company.insurance_company_type == 'i'\
            or service.insurance_plan.company.insurance_company_type == 'c':
                party, = Party.search([('id', '=', service.insurance_plan.company.id)])
            else:
                raise InvoceTypeError(
                    gettext('health_ebome.msg_invoice_type_error2'))

            if not service.diagnosis:
                raise ServiceWithoutDiagnosis(
                    gettext('health_ebome.msg_service_without_diagnosis'))

            #Determinar si se van a generar 2 facturas o una en el caso de los pacientes asegurados
            if service.insurance_plan.plan_id.coverage < 100 \
            and service.insurance_plan.plan_id.remainder_payer == 's':
                two_invoices = True
            else:
                two_invoices = False

            invoice = InvoiceTheService.create_invoice(self, service, party, 'first', two_invoices)
            invoices.append(invoice['invoice_data'])

            if two_invoices == True:
                party, = Party.search([('id', '=', service.insurance_plan.plan_id.insurance_subscriber)])
                invoice = InvoiceTheService.create_invoice(self, service, party, 'last', two_invoices)
                invoices.append(invoice['invoice_data'])

        Invoice.update_taxes(Invoice.create(invoices))

        action['pyson_domain'] = PYSONEncoder().encode([
            ('invoice_date', '=', invoice['invoice_data']['invoice_date']),
            ('reference', '=', invoice['invoice_data']['reference']),
            ('state', '=', 'draft'),
            ])

        # Change to invoiced the status on the service document if all lines invoiced
        if invoice['all_invoiced'] == True:
            HealthService.write(services, {'state': 'invoiced'})

        return action, {}
