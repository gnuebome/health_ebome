from datetime import datetime, date
from trytond.model import ModelView, fields
from trytond.pyson import Eval, Or
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateAction, Button, StateReport
from pprint import pprint

class CreateJournalEbomeStart(ModelView):
    'Journal Ebome Create Start'
    __name__ = 'journal.ebome.start'

    journal_date = fields.Date('Date', required=True)

    @classmethod
    def default_journal_date(cls):
        return Pool().get('ir.date').today()


class CreateJournalEbome(Wizard):
    'Journal Ebome Create'
    __name__ = 'journal.ebome.create'

    start = StateView('journal.ebome.start',
        'health_ebome.view_create_journal_ebome', [
            Button('Create', 'create_report', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    create_report = StateAction('health_ebome.report_journal_ebome_from_wizard')

    def default_start(self, fields):
        res = { }
        return res

    def do_create_report(self, action):
        journal_date = self.start.journal_date.strftime("%d/%m/%Y")

        data = {
            'journal_date' : journal_date,
            }

        data['title'] = 'Caisse Journalière'
        results = {}
        total = {}
        total_credit = 0
        origin = ''

        pool = Pool()
        Moves = pool.get('account.move')
        Invoice = pool.get('account.invoice')
        Payment_methods = pool.get('account.journal')

        selected_payment_methods = Payment_methods.search([
            ('code', 'not in', ['EXP', 'REV', 'STO'])])
        for payment_method in selected_payment_methods:
            total[payment_method.name] = {'credit' : 0}

        selected_moves = Moves.search([
            ('date','=',self.start.journal_date),
            ('journal.code', 'not in', ['EXP', 'REV', 'STO']),
            ('state', 'in', ['draft', 'posted'])],
            order=[('number', 'ASC')])

        for move in selected_moves:
            if isinstance(move.origin, Invoice) and\
                move.state == 'draft':
                pass
            else:
                for line in move.lines:
                    if line.account.name == 'Compte client':
                        results[move.number] = {'party_code' : ' ', 'party' : ' ', 'credit' : ' ', 'payment_method' : ' ', 'comment' : ' ', 'invoice' : ' ','invoice_date' : ' '}
                        results[move.number]['party_code'] = line.party.code
                        results[move.number]['party'] = line.party.rec_name
                        results[move.number]['credit'] = (line.credit - line.debit)
                        results[move.number]['payment_method'] = move.journal.name
                        results[move.number]['comment'] = move.description
                        if isinstance(move.origin, Invoice):
                            results[move.number]['invoice'] = move.origin.number
                            results[move.number]['invoice_date'] = move.origin.invoice_date.strftime("%d/%m/%Y")
                        total[move.journal.name]['credit'] += (line.credit - line.debit)
                        total_credit += (line.credit - line.debit)


        data['results'] = results
        data['total'] = total
        data['total_credit'] = total_credit

        return action, data
