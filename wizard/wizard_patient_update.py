# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys
import unicodedata
from uuid import uuid4
import string
import random

from datetime import datetime

from trytond.i18n import gettext
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, StateAction, \
    Button
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Not
from trytond.transaction import Transaction
from ..exceptions import BadTarifType

# Se crea nueva vista que se diferencia de PatientCreateManualStart en que
# se sustituye el campo Insurance de party_party por current_insurance de gnuhealth_insurance
# De esa forma se presenta un solo campo que si se quiere modificar se accede a las pantallas de alta de nuevo tipo de tarifa-régimen

class PatientUpdateEbomeStart(ModelView):
    'Patient Update Ebome'
    __name__ = 'gnuhealth.patient.update.ebome.start'

    name = fields.Char("Name", required=True)
    lastname = fields.Char("Lastname", required=True)
    puid = fields.Char("IDUP")
    code = fields.Char("Code", readonly=True)
    patient_id = fields.Integer('Patient id')
    party_id = fields.Integer('Party id')
    gender = fields.Selection([
        (None, ''),
        ('m', 'Male'),
        ('f', 'Female')
        ], "Gender", required=True)
    marital_status = fields.Selection([
        (None, ''),
        ('s', 'Single'),
        ('m', 'Married'),
        ('c', 'Concubinage'),
        ('w', 'Widowed'),
        ('d', 'Divorced'),
        ('x', 'Separated'),
        ], 'Marital Status', sort=False,
        help="Marital Status")
    dob = fields.Date("Birth date", required=True)
    is_newborn = fields.Boolean("New born")
    mother = fields.Many2One('gnuhealth.patient',"Mother",
        states={
            'invisible':Not(Bool(Eval('is_newborn'))),
            'required': Bool(Eval('is_newborn')),
            })
    ethnic_group = fields.Many2One('gnuhealth.ethnicity', 'Ethnicity')

    current_insurance = fields.Many2One(
        'gnuhealth.insurance', 'Insurance',
        domain=[('name', '=', Eval('party_id'))],
        depends=['party_id'],
        required = True,
        help='Insurance information. You may choose from the different'
        ' insurances belonging to the patient')

    citizenship = fields.Many2One('country.country', 'Citizenship',
        help="Country of Citizenship")
    residence = fields.Many2One('country.country', 'Residence',
        help="Country of Residence")
    phone = fields.Char("Telephone", required=True)
    phone_contact = fields.Char("Telephone contact")
    emergency_phone = fields.Boolean("Emergency phone")
    phone_aux = fields.Char("Telephone Aux",
        states={
            'invisible':Not(Bool(Eval('phone'))),
            })
    phone_aux_contact = fields.Char("Telephone Aux Contact",
        states={
            'invisible':Not(Bool(Eval('phone'))),
            },
        help="Contact name or social reason")
    emergency_phone_aux = fields.Boolean("Emergency phone aux",
        states={
            'invisible':Not(Bool(Eval('phone'))),
            })
    desc = fields.Char('Desc')
    address_street = fields.Char('Street')
    address_street_number = fields.Integer('Number')
    address_street_bis = fields.Char('Apartment')
    du = fields.Many2One('gnuhealth.du','DU',
        states={
        'readonly': Bool(Eval('update_operational_sector')),
            },
        help="Ask if he/she is \n living with another patient (same ud)")
    operational_sector = fields.Many2One('gnuhealth.operational_sector',
        "Operational Sector",
        states={
            'readonly': Bool(Eval('update_operational_sector')),
            },
        help="Neighboard")
    operational_area = fields.Function(
        fields.Char('Operational Area'),
        'get_operational_area')
    update_operational_sector = fields.Boolean("Update operational")
    occupation = fields.Many2One('gnuhealth.occupation', 'Occupation',
        states={
            'readonly': Bool(Eval('update_occupation')),
            })
    update_occupation = fields.Boolean("Update occupation")
    education = fields.Selection([
        (None, ''),
        ('0', 'None'),
        ('1', 'Incomplete Primary School'),
        ('2', 'Primary School'),
        ('3', 'Incomplete Secondary School'),
        ('4', 'Secondary School'),
        ('5', 'University'),
        ], 'Education', help="Education Level", sort=False,
        states={
            'readonly': Bool(Eval('update_education')),
            })
    update_education = fields.Boolean("Update occupation")
    photo = fields.Binary('Picture')

    @fields.depends('is_newborn','mother')
    def on_change_mother(self):
        STRSIZE = 9
        puid = ''
        if self.is_newborn and self.mother:
            for x in range(STRSIZE):
                if ( x < 3 or x > 5 ):
                    puid = puid + random.choice(string.ascii_uppercase)
                else:
                    puid = puid + random.choice(string.digits)
            self.puid = u"NN fils de "+self.mother.name.ref+ ' ' + '-' + puid
            self.name = "fils de : "+self.mother.name.name+' '+self.mother.name.lastname
            self.lastname = '*******'

    @fields.depends('du')
    def on_change_with_operational_sector(self):
        if self.du and self.du.operational_sector:
            return self.du.operational_sector.id
        return None

    @fields.depends('operational_sector')
    def on_change_with_operational_area(self):
        if self.operational_sector and self.operational_sector.operational_area:
            return self.operational_sector.operational_area.name
        return ''

    @fields.depends('citizenship')
    def on_change_with_residence(self, name=None):
        if self.citizenship:
            return self.citizenship.id
        return None

    def get_operational_area(self):
        if self.operational_sector and self.operational_sector.operational_area:
            return self.operational_sector.operational_area.name
        return ''

class PatientUpdateEbome(Wizard):
    'Patient Ebome'
    __name__ = 'gnuhealth.patient.update.ebome'

    start = StateView('gnuhealth.patient.update.ebome.start',
        'health_ebome.view_patient_update_ebome_start', [
            Button('Update', 'update_manually', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])

    def default_start(self, fields):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')
        res = {}
        patients = Patient(Transaction().context['active_id'])

        if not patients:
            return res
        res = {
            'name': patients.name.name,
            'lastname': patients.name.lastname,
            'dob': patients.name.dob,
            'puid' : patients.name.ref,
            'code': patients.name.code,
            'gender': patients.name.gender,
            'marital_status': patients.name.marital_status,
            'ethnic_group': patients.name.ethnic_group and patients.name.ethnic_group.id,
            'citizenship': patients.name.citizenship and patients.name.citizenship.id,
            'residence': patients.name.residence and patients.name.residence.id,
            'current_insurance': patients.current_insurance.rec_name and patients.current_insurance.id,
            'phone': patients.name.contact_mechanisms and patients.name.contact_mechanisms[0].value,
            'phone_contact': patients.name.contact_mechanisms and patients.name.contact_mechanisms[0].remarks,
            'emergency_phone': patients.name.contact_mechanisms and patients.name.contact_mechanisms[0].emergency,
            'phone_aux': patients.name.contact_mechanisms[1].value\
                if len(patients.name.contact_mechanisms) > 1 else None,
            'phone_aux_contact': patients.name.contact_mechanisms[1].remarks\
                if len(patients.name.contact_mechanisms) > 1 else None,
            'emergency_phone_aux': patients.name.contact_mechanisms[1].emergency\
                if len(patients.name.contact_mechanisms) > 1 else None,
            'occupation': patients.name.occupation and patients.name.occupation.id,
            'update_occupation': True,
            'education': patients.name.education,
            'update_education': True,
            'du': patients.name.du and patients.name.du.id,
            'operational_sector': patients.name.du and patients.name.du.operational_sector\
                and patients.name.du.operational_sector.id,
            'update_operational_sector': True,
            'photo': patients.photo,
            'patient_id': patients.id,
            'party_id' : patients.name.id
            }
        return res

    update_manually = StateAction(
        'health.action_gnuhealth_patient_view')

    def do_update_manually(self, action):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')
        Contact_mechanisms = pool.get('party.contact_mechanism')
        DU = pool.get('gnuhealth.du')
        SES = pool.get('gnuhealth.ses.assessment')
        Insurance = pool.get('gnuhealth.insurance')
        Plan = pool.get('gnuhealth.insurance.plan')

        if self.start.current_insurance and \
            self.start.current_insurance.company.name == 'Assurance':
                raise BadTarifType(
                    gettext('health_ebome.'
                            'msg_bad_tarif_type'))

        patients = Patient.search([('id','=',self.start.patient_id)])

        for patient in patients:
            patient.name.name = self.start.name
            patient.name.lastname = self.start.lastname
            patient.name.ref = self.start.puid
            patient.name.code = self.start.code
            patient.name.gender = self.start.gender
            patient.name.marital_status = self.start.marital_status
            patient.name.ethnic_group = self.start.ethnic_group and \
                self.start.ethnic_group.id
            patient.name.citizenship = self.start.citizenship and \
                self.start.citizenship.id
            patient.name.residence = self.start.residence and \
                self.start.residence.id
            patient.name.dob = self.start.dob
            patient.name.education = self.start.education
            patient.name.occupation = self.start.occupation
            patient.name.photo = self.start.photo
            patient.name.save()
            # update contact_mechanisms
            if patient.name.contact_mechanisms and patient.name.contact_mechanisms[0]:
                patient.name.contact_mechanisms[0].value = self.start.phone
                patient.name.contact_mechanisms[0].remarks = self.start.phone_contact
                patient.name.contact_mechanisms[0].emergency = self.start.emergency_phone
                patient.name.contact_mechanisms[0].save()
            else:
                Contact_mechanisms.create([{
                    'type': 'mobile',
                    'value': self.start.phone,
                    'remarks': self.start.phone_contact,
                    'emergency':self.start.emergency_phone,
                    'party': patient.name.id,
                    }])
            # update insurance
            if self.start.current_insurance and (patient.current_insurance and \
                patient.current_insurance.id != self.start.current_insurance.id or\
                not patient.current_insurance):
                patient.current_insurance =  self.start.current_insurance.id
                patient.save()
            if patient.name.contact_mechanisms and len(patient.name.contact_mechanisms)>1:
                patient.name.contact_mechanisms[1].value = self.start.phone_aux
                patient.name.contact_mechanisms[1].remarks = self.start.phone_aux_contact
                patient.name.contact_mechanisms[1].emergency = self.start.emergency_phone_aux
                patient.name.contact_mechanisms[1].save()
            elif self.start.phone_aux:
                Contact_mechanisms.create([{
                    'type': 'mobile',
                    'value': self.start.phone_aux,
                    'remarks': self.start.phone_aux_contact,
                    'emergency':self.start.emergency_phone_aux,
                    'party': patient.name.id,
                    }])
            patient.name.save()
            patient.save()

            if not self.start.update_operational_sector:
                if patient.name.du:
                    du = DU.search([('name','=',patient.name.du.name)])
                    if du and self.start.operational_sector:
                        patient.name.du.operational_sector = self.start.operational_sector.id
                        patient.name.du.save()
                else:
                    patient.name.du = DU.create([{
                           'name': str(self.start.operational_sector.name)+' '+self.start.name+' '
                                + self.start.lastname
                                +' '+ str(self.start.dob)+' '+self.start.puid,
                            'operational_sector':self.start.operational_sector.id,
                            }])[0]
            patient.name.save()
            patient.save()
            if not self.start.update_operational_sector or not self.start.update_education\
                or not self.start.update_occupation:
                ses = SES.create([{
                    'education': self.start.education,
                    'occupation': self.start.occupation,
                    'du': patient.name.du and patient.name.du.id,
                    'patient': patient.id,
                    }])
                SES.end_assessment(ses)

        data = {'res_id': [p.id for p in patients]}
        if len(patients) == 1:
            action['views'].reverse()
        return action, data
