##############################################################################
#
#    GNU Health: Reporting Module
#
#
#    Copyright (C) 2012-2014  Sebastian Marro <smarro@gnusolidario.org>
#    Copyright (C) 2013-2022 Luis Falcon <lfalcon@gnusolidario.org>
#    Copyright (C) 2011-2022 GNU Solidario <health@gnusolidario.org>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import *
from dateutil.relativedelta import relativedelta

from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard, StateView, StateAction, Button
from trytond.pyson import PYSONEncoder
from trytond.pool import Pool
from trytond.transaction import Transaction


class OpenInsuranceInvoicesStart(ModelView):
    'Open Insurance Invoices'
    __name__ = 'gnuhealth.insurance.invoices.open.start'

    invoices_month = fields.Many2One('ir.calendar.month', "Month", required=True)

    invoices_year = fields.Numeric('Year', required=True,
                                  digits=(4, 0),
                                  domain=['OR',
                                        ('invoices_year', '=', None),
                                        [('invoices_year', '>=', 2024), ('invoices_year', '<=', 2099)],
                                        ])

    insurance = fields.Many2One('party.party', 'Insurance', required=True,
                                 domain=[('insurance_company_type', 'in', ['i', 'v', 'c', 's'])],
                                 search_order=[('insurance_company_type', 'ASC'),
                                               ('name', 'ASC')])

    @classmethod
    def default_invoices_month(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        mes = Date.today().month
        if mes == 1:
            return 12
        else:
            return mes - 1

    @classmethod
    def default_invoices_year(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        mes = Date.today().month
        year = Date.today().year
        if mes == 1:
            return year - 1
        else:
            return year

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        Month = pool.get('ir.calendar.month')

class OpenInsuranceInvoices(Wizard):
    'Open Insurance Invoices'
    __name__ = 'gnuhealth.insurance.invoices.open'

    start = StateView(
        'gnuhealth.insurance.invoices.open.start',
        'health_ebome.insurance_invoices_open_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Open', 'open_', 'tryton-ok', default=True),
            ])
    open_ = StateAction('health_ebome.act_invoice_ins_form')

    def do_open_(self, action):
        pool = Pool()
        Invoices = pool.get('account.invoice')

        # obtener el primer día del mes
        month=int(self.start.invoices_month.index)
        start_date = date(self.start.invoices_year, month, 1)

        # obtener el último día del mes. Suma 4 al día 28 del mes actual para obtener un día del próximo mes
        # y resta los días de la fecha resultante para obtener el último día del mes actual
        next_month = start_date.replace(day=28) + timedelta(days=4)
        end_date = next_month - timedelta(days=next_month.day)

        invoices = Invoices.search([('invoice_date','>=',start_date),
                                    ('invoice_date','<=',end_date),
                                    ('party','=',self.start.insurance.id)])

        action['pyson_domain'] = PYSONEncoder().encode([
            ('invoice_date','>=',start_date),
            ('invoice_date','<=',end_date),
            ('party','=',self.start.insurance.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'invoice_date': start_date,
            'invoice_date': end_date,
            'party': self.start.insurance.id
            })
        action['name'] += ' (%(party)s)' % {
            'party': self.start.insurance.name,
            }
        data = {'res_id': [x.id for x in invoices]}

        return action, data

    def transition_open_(self):
        return 'end'
