# This file is part of GNU Health Cameroun modeul.  The COPYRIGHT file at the
# top level of this repository contains the full copyright notices and
# license terms.
from trytond.exceptions import UserError, UserWarning
from trytond.model.exceptions import ValidationError

class ResultTextRequired(UserError):
    pass

class RemarksRequired(UserError):
    pass

class ResultRequired(UserError):
    pass

class PathologistRequired(UserError):
    pass

class TechnicianRequired(UserError):
    pass

class NoDiagnosis(UserError):
    pass

class NoPatient(UserError):
    pass

class NoRecordSelected(UserError):
    pass

class AlreadyHospializedPatient(UserError):
    pass

class DischargeReasonNeeded(UserError):
    pass

class BadTarifType(ValidationError):
    pass

class PayInvoiceError(UserError):
    pass

class InsuranceTarifNotFound(ValidationError):
    pass

class ServiceAlreadyInvoiced(ValidationError):
    pass

class NoInvoiceAddress(UserError):
    pass

class NoPaymentTerm(UserError):
    pass

class NoLineToInvoice(UserError):
    pass

class LineToInvoice(UserError):
    pass

class ServiceWithoutDiagnosis(ValidationError):
    pass

class InvoceTypeError(ValidationError):
    pass

class CoverageOutOfRange(ValidationError):
    pass

class BadRemainderPayer(ValidationError):
    pass

class DraftInvoicesExist(ValidationError):
    pass
