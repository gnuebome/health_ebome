import sys
import unicodedata
from uuid import uuid4
import string
import random
import re

from decimal import Decimal
#from datetime import datetime, timedelta, date
from datetime import *

from trytond.i18n import gettext
from trytond.pool import Pool, PoolMeta
from trytond.model import (Workflow, ModelView, ModelSQL, fields)
from trytond.pyson import (Eval, Not, Bool, Equal, PYSONEncoder, Or, And, If)
from trytond.wizard import Wizard, StateTransition, StateView, Button, StateAction
from trytond.transaction import Transaction
from trytond.tools import grouped_slice, reduce_ids
from trytond.modules.account.tax import TaxableMixin
from trytond.modules.product import price_digits
from trytond.modules.health.core import get_health_professional
from trytond.modules.health_crypto.health_crypto import HealthCrypto

from sql import Literal

from .exceptions import *
from trytond.modules.account_invoice.exceptions import InvoicePaymentTermDateWarning, InvoiceNumberError
from trytond.modules.health_inpatient.exceptions import DischargeBeforeAdmission, BedIsNotAvailable, NoAssociatedHealthProfessional, \
    ManyRecordsChosen,DestinationBedNotAvailable
from trytond.modules.health_services.exceptions import NoServiceAssociated

class Pathology(metaclass = PoolMeta):

    __name__ = 'gnuhealth.pathology'

    @classmethod
    def check_xml_record(cls, records,values):
            return True

    # Se saca el código del rec_name
    def get_rec_name(self, name):
        return (self.name)

    # Search by the the description
    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('name',) + tuple(clause[1:])
                ]

class PatientEvaluation(metaclass = PoolMeta):
    __name__ = 'gnuhealth.patient.evaluation'

    STATES = {'readonly': Or(Eval('state') == 'signed',
                             Eval('state') == 'finished')}

    pregnant = fields.Selection([
        (None, ''),
        ('pregnant', 'Pregnant'),
        ('not pregnant', 'Not Pregnant')
        ], 'Pregnant', sort=False,
        required=True,
        states=STATES)

    hospitalized = fields.Boolean(
        'Hospitalized', help="Check this option if the patient is hospitalized after the evaluation",
        states=STATES)

    supplementary_tests = fields.Text('Suplementary Tests', states=STATES)

    patient_code = fields.Function(fields.Char('Code'),'get_patient_code')

    tarif_type = fields.Many2One(
        'gnuhealth.insurance',
        'Plan',
        states = {'readonly': Bool(Eval('tarif_type'))})

    occupation = fields.Function(fields.Char('Occupation'),'get_occupation')

    contact_information = fields.Function(fields.Char('Contact Information'),'get_contact_information')

    diagnosis_precision = fields.Char('Main Condition Precision',
        help='Presumptive Diagnosis Precision.',
        states=STATES)

    last_updater = fields.Function(fields.Char('Health Professional'),'get_last_updater')

    @fields.depends('patient')
    def get_contact_information(self, name):
        if self.patient.name.contact_mechanisms:
            return self.patient.name.contact_mechanisms[0].value

    @fields.depends('patient')
    def on_change_with_tarif_type(self):
        if self.patient and self.patient.current_insurance:
            return self.patient.current_insurance.id


    @fields.depends('patient')
    def get_occupation(self, name):
        if self.patient.name.occupation:
            return self.patient.name.occupation.rec_name

    @fields.depends('patient')
    def get_patient_code(self, name):
        return self.patient.code

#    @fields.depends('signed_by', 'write_uid')
    def get_last_updater(self, name):
        Party=Pool().get('party.party')

        if self.signed_by:
            return self.signed_by.rec_name

        if self.write_uid:
            party = Party.search([
                ('internal_user.id', '=', self.write_uid.id),
                ])
            if party:
                return party[0].rec_name

    # Se modifica la función para que además de cerrar la evaluación y dar de alta al paciente,
    # firme la evaluación y la finalice, es decir, se agrupan 2 pasos en uno;
    # y traslade fecha de evaluación y diagnosis a la hoja de facturación asociada
    @classmethod
    @ModelView.button
    def end_evaluation(cls, evaluations):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        HealthService = pool.get('gnuhealth.health_service')

        evaluation_id = evaluations[0]

        if not evaluation_id.diagnosis:
            raise NoDiagnosis(
                gettext('health_ebome.msg_no_diagnosis'))

        patient_app = []

        # Change the state of the evaluation to "Done"
        signing_hp = get_health_professional()

        serial_doc = cls.get_serial(evaluation_id)

        cls.write(evaluations, {
            'state': 'signed',
            'signed_by': signing_hp,
            'evaluation_endtime': datetime.now(),
            'serializer': serial_doc,
            'document_digest': HealthCrypto().gen_hash(serial_doc)
            })

        # If there is an appointment associated to this evaluation
        # set it to state "Done"

        if evaluation_id.appointment:
            patient_app.append(evaluation_id.appointment)
            Appointment.write(patient_app, {
                'state': 'done',
                })

        # Create an entry in the page of life
        # It will create the entry at the moment of
        # discharging the patient
        # The patient needs to have a federation account
        if (evaluation_id.patient.name.federation_account):
            cls.create_evaluation_pol(evaluation_id)

        hservice = []
        if not evaluation_id.service:
            raise NoServiceAssociated(
                    gettext('health_services.msg_no_service_associated'))

        service_data = {}

        hservice.append(evaluation_id.service)

        if evaluation_id.diagnosis.code == 'Z76.8' and \
            evaluation_id.diagnosis_precision != None:
                service_data['diagnosis'] = evaluation_id.diagnosis_precision
        else:
            service_data['diagnosis'] = evaluation_id.diagnosis.name

        service_data['date_of_entry'] = datetime.date(evaluation_id.evaluation_start)
        service_data['exit_date'] = datetime.date(evaluation_id.evaluation_endtime)

        HealthService.write(hservice, service_data)

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.visit_type = fields.Selection([
        (None, ''),
        ('new', 'New health condition'),
        ('followup', 'Followup'),
        ], 'Visit', sort=False, required=True,
        states = cls.STATES)

        cls.product.states = {'invisible' : True}

        cls.service = fields.Many2One(
            'gnuhealth.health_service', 'Service',
            domain=[('patient', '=', Eval('patient')),
                    ('insurance_plan', '=', Eval('tarif_type')),
                    ('state', '=', 'draft')],
            depends=['patient', 'insurance_plan', 'state'],
            required=True,
            states = {'readonly': Equal(Eval('state'), 'signed')},
            help="Service document associated to this evaluation")

class PatientData(metaclass = PoolMeta):
    __name__ = 'gnuhealth.patient'

# Eliminamos los campos financial_situation y financial_situation_icon
# y se sustituyen por receivable (deuda del paciente) y debtor_ind (semáforo rojo si el paciente tiene deuda)
# El nuevo campo receivable será visible solo si el paciente tiene deuda
    STATES = {'invisible': Eval('receivable') == ' '}

    receivable = fields.Function(fields.Char('Receivable', states=STATES), 'get_receivable')
    debtor_ind = fields.Function(fields.Char('Ind'), 'get_debtor_ind')


    ethnic_group = fields.Function(fields.Many2One('gnuhealth.ethnicity', 'Ethnicity'),'get_ethnic_group')

    phone = fields.Function(fields.Char('Phone'),'get_phone')

    phone_contact = fields.Function(fields.Char('Phone Contact'),'get_phone_contact')

    operational_sector = fields.Function(fields.Many2One(
        'gnuhealth.operational_sector', 'Operational Sector'),'get_operational_sector')

    education = fields.Function(fields.Selection([
        (None, ''),
        ('0', 'None'),
        ('1', 'Incomplete Primary School'),
        ('2', 'Primary School'),
        ('3', 'Incomplete Secondary School'),
        ('4', 'Secondary School'),
        ('5', 'University'),
        ], 'Education Level'), 'get_education')

    @fields.depends('name')
    def get_ethnic_group(self, _name):
        return self.name.ethnic_group

    @fields.depends('name')
    def get_phone(self, _name):
        if self.name.contact_mechanisms and self.name.contact_mechanisms[0]:
            #return self.name.contact_mechanisms[0].value
            return self.name.contact_mechanisms and self.name.contact_mechanisms[0].value

    @fields.depends('name')
    def get_phone_contact(self, _name):
        if self.name.contact_mechanisms and self.name.contact_mechanisms[0]:
            #return self.name.contact_mechanisms[0].remarks
            return self.name.contact_mechanisms and self.name.contact_mechanisms[0].remarks

    @fields.depends('name')
    def get_operational_sector(self, _name):
        if self.name.du and self.name.du.operational_sector and self.name.du.operational_sector.id:
            #return self.name.du.operational_sector
            return self.name.du and self.name.du.operational_sector and self.name.du.operational_sector.id

    @fields.depends('name')
    def get_education(self, name):
        return self.name.education

    @fields.depends('name')
    def get_receivable(self, name):
        if self.name.receivable and self.name.receivable != 0:
            return str(self.name.receivable)
        return ' '

    @fields.depends('name')
    def get_debtor_ind(self, name):
        if self.name.receivable and self.name.receivable != 0:
            return 'ind_icon_red'
        return ''

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.current_insurance.readonly = True

# Definimos el campo "estado civil" como no obligatorio
# Añadimos el campo "plan" para seleccionar un plan de los posibles para la compañía de seguros (tipo de tarifa) seleccionada
# y lo ponemos como de solo lectura hasta que no se cumplimente el campo "insurance"
class PatientCreateManualStart(metaclass = PoolMeta):
    'Patient Create Manual'
    __name__ = 'gnuhealth.patient.create.manual.start'

    plan = fields.Many2One('gnuhealth.insurance.plan', 'Plan',
        help='Insurance company plan', required=True,
        domain=[('company', '=', Eval('insurance'))],
        depends=['insurance'],
        states = {'readonly': Not(Eval('insurance'))})

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.marital_status.required = False
        cls.insurance.required = True
        cls.insurance.domain=[('is_insurance_company', '=', True),
                              ('insurance_company_type', 'in', ['c', 'i', 't', 'v'])]
        cls.insurance.search_order=[('insurance_company_type', 'DESC'),
                                    ('name', 'ASC')]

        cls.gender = fields.Selection([
        (None, ''),
        ('m', 'Male'),
        ('f', 'Female'),
        ], "Gender", required=True)

# Se añade tratamiento para el nuevo campo de plan (régimen)
# Si se quiere añadir un nuevo tipo de tarifa al paciente, se hará desde el wizard de actualización de datos del paciente
class PatientCreateManual(Wizard):
    'Patient Create'
    __name__ = 'gnuhealth.patient.create.manual'

    def default_start(self, fields):
        STRSIZE = 9
        puid = ''
        for x in range(STRSIZE):
            if ( x < 3 or x > 5 ):
                puid = puid + random.choice(string.ascii_uppercase)
            else:
                puid = puid + random.choice(string.digits)
        res = {
            'puid': puid
            }
        return res

    def do_create_manually(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')
        DU = pool.get('gnuhealth.du')
        SES = pool.get('gnuhealth.ses.assessment')
        Date = pool.get('ir.date')
        Insurance = pool.get('gnuhealth.insurance')
        Plan = pool.get('gnuhealth.insurance.plan')

        if self.start.insurance.name == 'Assurance':
                raise BadTarifType(
                    gettext('health_ebome.'
                            'msg_bad_tarif_type'))

        du = None
        if not self.start.du:
            du = [{
                'name': self.start.name+' '
                    + self.start.lastname
                    +' '+self.start.puid,
                    }]
            du = DU.create(du)
        else:
            du = [self.start.du]

        if self.start.operational_sector and not \
            (self.start.du and self.start.du.operational_sector):
            du[0].operational_sector = self.start.operational_sector.id
            du[0].save()
        contact_mechanisms = [{
            'type': 'mobile',
            'value': self.start.phone,
            'remarks': self.start.phone_contact,
            'emergency': self.start.emergency_phone,
            }]
        if self.start.phone_aux:
            contact_mechanisms.append({
                'type': 'mobile',
                'value': self.start.phone_aux,
                'remarks': self.start.phone_aux_contact,
                'emergency': self.start.emergency_phone_aux,
                })

        party_data = {
            'name': self.start.name,
            'lastname': self.start.lastname,
            'ref': self.start.puid,
            'dob': self.start.dob,
            'gender': self.start.gender,
            'marital_status': self.start.marital_status,
            'ethnic_group': self.start.ethnic_group and self.start.ethnic_group.id,
            'citizenship': self.start.citizenship and self.start.citizenship.id,
            'residence': self.start.residence and self.start.residence.id,
            'is_person': True,
            'is_patient': True,
            'photo': self.start.photo,
            'federation_account': self.start.puid,
            'du': du[0].id,
            'contact_mechanisms': [('create',contact_mechanisms)],
            'occupation': self.start.occupation,
            'education': self.start.education,
            'addresses':  [('create',[{'name':'',}])],
            }

        party = Party.create([party_data])
        patient = None
        if party:
            patient = Patient.create([{
                'name': party[0],
                }])
        if self.start.insurance:
            insurance = Insurance.create([{
                'name': party[0].id,
                'number': f'{self.start.insurance.id}-{self.start.plan.id}-{party[0].code}',
                'company': self.start.insurance.id,
                'plan_id': self.start.plan.id,
                'member_since': date.today(),
                'member_exp': date(2099, 12, 31)
                }])
            patient[0].current_insurance = insurance[0].id
            patient[0].save()
        if self.start.education or self.start.occupation:
            ses = SES.create([{
                'assessment_date': datetime.now(),
                'occupation': self.start.occupation,
                'education': self.start.education,
                'patient': patient[0].id,
                }])
            SES.end_assessment(ses)
        data = {'res_id': [p.id for p in patient]}
        if len(patient) == 1:
            action['views'].reverse()
        return action, data

# Se añade nuevo campo “catalog_name” de tipo Funcional para recoger el catálogo de test de imagen a mostrar
# según el tipo de tarifa del paciente al que se van a hacer los test
# y poder presentar así únicamente los test ofertables a ese tipo de paciente
class ImagingTest(metaclass = PoolMeta):
    'Imaging Test'
    __name__ = 'gnuhealth.imaging.test'

    catalog_name = fields.Function(fields.Char(' Catalog'),
                                   'get_catalog_name',
                                   searcher='search_catalog_name')

    def get_catalog_name(self, name):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')

        if Transaction().context.get('modelo') == 'gnuhealth.patient.imaging.test.request.start':
            patient, = Patient.search([('id','=',Transaction().context.get('patient'))])
            if patient:
                if patient.current_insurance.company.insurance_company_type == "i"\
                or patient.current_insurance.company.insurance_company_type == "c":
                    return "Assurance"
                elif patient.current_insurance.company.insurance_company_type == "v":
                    return "Visite"
                else:
                    return "Particular"

        return None

    @classmethod
    def search_catalog_name(cls, name, clause):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')
        t = pool.get('product.template')
        p = pool.get('product.product')

        temp = t.__table__()
        prod = p.__table__()
        test = cls.__table__()

        j1 = prod.join(temp, condition=prod.template == temp.id)
        j2 = test.join(j1, condition=test.product == prod.id)
        i = j2.select(test.id, where=temp.is_for_insured == True)
        v = j2.select(test.id, where=temp.is_for_visit == True)
        p = j2.select(test.id, where=temp.is_for_particular == True)

        if Transaction().context.get('modelo') == 'gnuhealth.patient.imaging.test.request.start':
            patient, = Patient.search([('id','=',Transaction().context.get('patient'))])
            if patient:
                if patient.current_insurance.company.insurance_company_type == "i"\
                or patient.current_insurance.company.insurance_company_type == "c":
                    return [('id', 'in', i)]
                elif patient.current_insurance.company.insurance_company_type == "v":
                    return [('id', 'in', v)]
                else:
                    return [('id', 'in', p)]

        return []

# Añadimos nuevo campo de texto libre "context_detail"
# Añadimos nuevo campo de texto libre "origin_request"
class ImagingTestRequest(metaclass = PoolMeta):
    __name__ = 'gnuhealth.imaging.test.request'

    context_detail = fields.Char("Anatomical Detail")

    origin_request = fields.Char("Origin of the request")

# Añadimos nuevo campo de texto libre "context_detail"
# Añadimos nuevo campo de texto libre "origin_request"
# Añadimos nuevo campo de texto libre "image_ind" para indicar si hay alguna imagen asociada en el test
# Añadimos nuevo campo de texto libre "imaging_technician"
# Añadimos nuevo campo de estado para distinguir los test finalizados, semáforo para resaltarlos y botón para sar de "en curso" a "finalizado"
class ImagingTestResult(metaclass = PoolMeta):
    __name__ = 'gnuhealth.imaging.test.result'

    STATES = {'invisible': Eval('origin_request') != 'Hospitalisation-Urgences'}

    context_detail = fields.Function(fields.Char("Détail Anatomique"), 'get_context_detail')

    origin_request = fields.Char('Demandé dans')

    image_ind = fields.Function(fields.Char('Image ind'), 'get_image_ind')

    imaging_technician = fields.Many2One(
        'gnuhealth.healthprofessional', 'Imaging Technician',
        help="Le technicien d'imagerie qui fait le rapport", select=True,
        domain=[('main_specialty.specialty.name', '=', 'Imagerie')])

    bed = fields.Function(fields.Char('Bed', states = STATES),
                          'get_bed')

    ward = fields.Function(fields.Char('Ward', states = STATES),
                           'get_ward')

    state = fields.Selection([
        (None, ''),
        ('finished', 'Finished')
        ], 'Test state', select=False,
        states = {'readonly' : True,
                  'invisible' : Eval('state') != 'finished'})

    state_ind = fields.Function(fields.Char('Estate indicator'), 'get_state_ind')

    def get_context_detail(self, name):
        if (self.request):
            return self.request.context_detail

#    @fields.depends('name')
    def get_image_ind(self, name):
        if self.images:
            return 'ind_image_icon'
        return ''

    @fields.depends('patient')
    def get_bed(self, name):
        Inpatient = Pool().get('gnuhealth.inpatient.registration')

        inpatient = Inpatient.search([('patient', '=', self.patient.id),
                  ('state', '=', 'hospitalized')])

        if inpatient:
            return inpatient[0].bed.rec_name

        return None

    @fields.depends('patient', 'bed')
    def get_ward(self, name):
        Inpatient = Pool().get('gnuhealth.inpatient.registration')
        Bed = Pool().get('gnuhealth.hospital.bed')

        inpatient = Inpatient.search([('patient', '=', self.patient.id),
                  ('state', '=', 'hospitalized')])

        if inpatient:
            bed = Bed.search([('id', '=', inpatient[0].bed.id)])
            return bed[0].ward.name
        return None

#    @fields.depends('name')
    def get_state_ind(self, name):
        try:
            if self.state == 'finished':
                return 'ind_test_finished_icon'
        except:
            return ''

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'button_finish': {
                'invisible': Equal(Eval('state'), 'finished')}
            })

# Botón para pasar el test a estado finalizado
    @classmethod
    @ModelView.button
    def button_finish(cls, image_tests):
        cls.write(image_tests, {
                    'state': 'finished'})

# Se añade validación para que se obligue a seleccionar un técnico de rayos
    @classmethod
    def validate(cls, tests):
        super(ImagingTestResult, cls).validate(tests)
        for test in tests:
            test.check_technician()

    def check_technician(self):
        if self.state:
            if self.state == 'finished':
                if not self.imaging_technician:
                    raise TechnicianRequired(
                        gettext('health_ebome.msg_technician_required')
                        )

# Se añade nuevo campo “catalog_name” de tipo Funcional para recoger el catálogo de tipos de test a mostrar
# según el tipo de tarifa del paciente al que se van a realizar los test
# y poder presentar así únicamente los test ofertables a ese tipo de paciente
class TestType(metaclass = PoolMeta):
    'Type of Lab test'
    __name__ = 'gnuhealth.lab.test_type'

    catalog_name = fields.Function(fields.Char(' Catalog'),
                                   'get_catalog_name',
                                   searcher='search_catalog_name')

    def get_catalog_name(self, name):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')

        if Transaction().context.get('modelo') == 'gnuhealth.patient.lab.test.request.start':
            patient, = Patient.search([('id','=',Transaction().context.get('patient'))])
            if patient:
                if patient.current_insurance.company.insurance_company_type == "i"\
                or patient.current_insurance.company.insurance_company_type == "c":
                    return "Assurance"
                elif patient.current_insurance.company.insurance_company_type == "v":
                    return "Visite"
                else:
                    return "Particular"

        return None

    @classmethod
    def search_catalog_name(cls, name, clause):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')
        t = pool.get('product.template')
        p = pool.get('product.product')

        temp = t.__table__()
        prod = p.__table__()
        test = cls.__table__()

        j1 = prod.join(temp, condition=prod.template == temp.id)
        j2 = test.join(j1, condition=test.product_id == prod.id)
        i = j2.select(test.id, where=temp.is_for_insured == True)
        v = j2.select(test.id, where=temp.is_for_visit == True)
        p = j2.select(test.id, where=temp.is_for_particular == True)

        if Transaction().context.get('modelo') == 'gnuhealth.patient.lab.test.request.start':
            patient, = Patient.search([('id','=',Transaction().context.get('patient'))])
            if patient:
                if patient.current_insurance.company.insurance_company_type == "i"\
                or patient.current_insurance.company.insurance_company_type == "c":
                    return [('id', 'in', i)]
                elif patient.current_insurance.company.insurance_company_type == "v":
                    return [('id', 'in', v)]
                else:
                    return [('id', 'in', p)]

        return []

# El campo "result_text" pasa a ser de selección, modificable sólo si el campo "units" contiene el valor "selection"
#   Los valores del combo se obtienen del campo "normal_range"
# El campo "remarks" es modificable sólo si el campo "units" contiene el valor "text"
# El campo "result" es modificable sólo si el campo "units" contiene una unidad de medida
class GnuHealthTestCritearea(metaclass = PoolMeta):
    'Lab Test Critearea'
    __name__ = 'gnuhealth.lab.test.critearea'

    is_selection = fields.Function(fields.Boolean('Is selection field?'),
                                   'get_is_selection')

    is_text = fields.Function(fields.Boolean('Is text field?'),
                                   'get_is_text')

    def get_is_selection(self, name):
        if self.units:
            if self.units.name == 'Selection'\
            or self.units.name == 'SelectionO'\
            or self.units.name == 'Selection-text':
                return True

        return False

    def get_is_text(self, name):
        if self.units:
            if self.units.name == 'Text'\
            or self.units.name == 'TextO'\
            or self.units.name == 'Selection-text':
                return True

        return False

    @fields.depends('normal_range', 'name')
    def result_options(self):
        if self.normal_range:
            reference = self.normal_range.replace(r'\n', ' ')
            selection_options = re.split(r",", reference)
            return [(x.strip(), x.strip()) for x in selection_options] + [(None, '')]

        return [(None, '')]

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.result.states= {'readonly': Or(Bool(Eval('is_text')),
                                           Bool(Eval('is_selection')),
                                           Not(Eval('units')))}
        cls.result_text = fields.Selection('result_options',
                                           'Result - Selection', sort=True,
                                           states = {'readonly': Not(Bool(Eval('is_selection')))})
        cls.remarks.states= {'readonly': Not(Bool(Eval('is_text')))}

# Añadimos nuevo campo de texto libre "symptoms"
# Añadimos nuevo campo de texto libre "origin_request"
class GnuHealthPatientLabTest(metaclass = PoolMeta):
    'Lab Test Request'
    __name__ = 'gnuhealth.patient.lab.test'

    symptoms = fields.Char("Symptoms", readonly=True)

    origin_request = fields.Char("Origin of the request")

# En el campo Pathologyst mostramos únicamente los técnicos de laboratorio
# Añadimos nuevo campo de texto libre "symptoms"
# Añadimos nuevo campo de texto libre "origin_request"
# Añadimos el contenido del campo "remarks" al campo "analytes_summary"
class Lab(metaclass = PoolMeta):
    'Patient Lab Test Results'
    __name__ = 'gnuhealth.lab'

    STATES = {'invisible': Eval('origin_request') != 'Hospitalisation-Urgences'}

    symptoms = fields.Char("Symptoms", readonly=True)

    origin_request = fields.Char("Demandé dans")

    bed = fields.Function(fields.Char('Bed', states = STATES),
                          'get_bed')

    ward = fields.Function(fields.Char('Ward', states = STATES),
                           'get_ward')

    state = fields.Selection([
        (None, ''),
        ('finished', 'Finished')
        ], 'Test state', select=False,
        states = {'readonly' : True,
                  'invisible' : Eval('state') != 'finished'})

    state_ind = fields.Function(fields.Char('Estate indicator'), 'get_state_ind')

    @fields.depends('patient')
    def get_bed(self, name):
        Inpatient = Pool().get('gnuhealth.inpatient.registration')

        inpatient = Inpatient.search([('patient', '=', self.patient.id),
                  ('state', '=', 'hospitalized')])

        if inpatient:
            return inpatient[0].bed.rec_name

        return None

    @fields.depends('patient', 'bed')
    def get_ward(self, name):
        Inpatient = Pool().get('gnuhealth.inpatient.registration')
        Bed = Pool().get('gnuhealth.hospital.bed')

        inpatient = Inpatient.search([('patient', '=', self.patient.id),
                  ('state', '=', 'hospitalized')])

        if inpatient:
            bed = Bed.search([('id', '=', inpatient[0].bed.id)])
            return bed[0].ward.name
        return None

#    @fields.depends('name')
    def get_state_ind(self, name):
        try:
            if self.state == 'finished':
                return 'ind_test_finished_icon'
        except:
            return ''

    def get_analytes_summary(self, name):
        summ = ""
        for analyte in self.critearea:
            if analyte.result or analyte.result_text or analyte.remarks:
                res = ""
                if analyte.result:
                    res = str(analyte.result) + " "
                if analyte.result_text:
                    res = res + analyte.result_text + " "
                if analyte.remarks:
                    res = res + analyte.remarks
                summ = summ + analyte.rec_name + " " + res.strip() + "\n"
        return summ

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.pathologist.domain=[('main_specialty.specialty.name', '=', 'Laborantin')]

        cls._buttons.update({
            'button_finish': {
                'invisible': Equal(Eval('state'), 'finished')}
            })

# Botón para pasar el test a estado finalizado
# Se valida que los campos "result", "result_text" y "remarks" tengan contenido en función del valor del campo "units"
    @classmethod
    @ModelView.button
    def button_finish(cls, lab_tests):

        lab_test = lab_tests[0]

        for test_critearea in lab_test.critearea:
            if test_critearea.units:
                if test_critearea.units.name == 'Selection'\
                or test_critearea.units.name == 'Selection-text':
                    if not test_critearea.result_text:
                        raise ResultTextRequired(
                            gettext('health_ebome.msg_result_text_required'))

                if test_critearea.units.name == 'Text':
                    if not test_critearea.remarks\
                    or test_critearea.remarks == '':
                        raise RemarksRequired(
                            gettext('health_ebome.msg_remarks_required'))

                if test_critearea.units.name != 'Selection'\
                and test_critearea.units.name != 'SelectionO'\
                and test_critearea.units.name != 'Selection-text'\
                and test_critearea.units.name != 'Text'\
                and test_critearea.units.name != 'TextO'\
                and test_critearea.units.name != '':
                    if not test_critearea.result:
                        if test_critearea.result == 0:
                            pass
                        else:
                            raise ResultRequired(
                                gettext('health_ebome.msg_result_required'))

        cls.write(lab_tests, {
                    'state': 'finished'})

# Se añade validación para que se obligue a seleccionar un técnico de laboratorio
    @classmethod
    def validate(cls, tests):
        super(Lab, cls).validate(tests)
        for test in tests:
            test.check_pathologist()

    def check_pathologist(self):
        if self.state:
            if self.state == 'finished':
                if not self.pathologist:
                    raise PathologistRequired(
                        gettext('health_ebome.msg_pathologist_required')
                        )

# Se define nuevo campo para mostrar los planes asociados a tipos de tarifa, compañías aseguradoras y suscriptores de seguros
# Se dan valores por defecto a cuenta cliente y condiciones de pago si se trata de una compañía o un suscriptor de un seguro
# Se definen nuevos valores para el campo de selección "insurance_company_type"
# Se ocultan los campos de cuenta para los tipos de tarifa
class Party(metaclass=PoolMeta):
    'Party'
    __name__ = 'party.party'

    plan = fields.One2Many(
        'gnuhealth.insurance.plan',
        'company', 'Plan',
        states = {'invisible': Eval('insurance_company_type') == 's'})

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls.insurance_company_type = fields.Selection([
            (None, ''),
            ('t', 'Tarif type'),
            ('i', 'Insurance'),
            ('v', 'Systematic visit'),
            ('c', 'Company'),
            ('s', 'Insurance subscriber'),
            ('x', ' ')
            ], 'Type of tarif type', sort=False)
        cls.addresses.states= {
                    'invisible': Or(Eval('insurance_company_type') == 't',
                                    Eval('insurance_company_type') == 'v')}

    @classmethod
    def view_attributes(cls):
        # Ocultar el grupo accounting cuando se trata de un tipo de tarifa
        return super(Party, cls).view_attributes() + [
                ('//group[@id="accountig"]', 'states', {
                    'invisible': Or(Eval('insurance_company_type') == 't',
                                    Eval('insurance_company_type') == 'v')
                })]

# Cumplimentamos el campo 'number' automáticamente con el número de tipo de tarifa, el número de régimen y el número de dossier
# Cumplimentamos el campo 'member_since' con la fecha de hoy
# Cumplimentamos el campo 'member_exp' con la fecha 31-12-2099
# Modificamos el _rec_name para que muestre el tipo de tarifa y el régimen
# Definimos el campo 'number' como no modificable (readonly) una vez se ha cumplimentado
# Definimos el campo 'plan_id' (régimen) como obligatorio
# y lo ponemos como no modificable (readonly) hasta que no se cumplimente el campo 'company'
class Insurance(metaclass = PoolMeta):
    'Insurance'
    __name__ = 'gnuhealth.insurance'
    _rec_name = 'name'

    @fields.depends('name', 'company', 'plan_id')
    def on_change_with_number(self):
        if (self.name and self.name.code and\
            self.company and self.plan_id):
            number = f'{self.company.id}-{self.plan_id.id}-{self.name.code}'
            return number

    @classmethod
    def default_member_since(cls):
        return date.today()

    @classmethod
    def default_member_exp(cls):
        return date(2099, 12, 31)

    def get_rec_name(self, name):
#        return (self.company.name + ' : ' + self.plan_id.name.template.name)

        if self.company.name and\
            self.plan_id:
            if self.plan_id.name.suffix_code:
                return (self.company.name + ' : ' + self.plan_id.name.name + ' - ' + self.plan_id.name.suffix_code)
            else:
                return (self.company.name + ' : ' + self.plan_id.name.name)
        else:
            pass

    # Buscar por tipo de tarifa o plan
    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('company.name',) + tuple(clause[1:]),
                ('plan_id.name.template.name',) + tuple(clause[1:]),
                ]

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.plan_id.required = True
        cls.plan_id.states = {'readonly': Not(Eval('company'))}

        cls.number.states = {'readonly': Bool(Eval('number'))}

        cls.company.domain=[('is_insurance_company', '=', True),
                            ('insurance_company_type', 'in', ['c', 'i', 't', 'v'])]
        cls.company.search_order=[('insurance_company_type', 'DESC'),
                                  ('name', 'ASC')]


# Se cambia el rec_name para que, si el producto tiene sufijo en el código, el rec_name sea el nombre del producto más el sufijo
# Definimos nuevos campos para tratamiento de las aseguradoras:
#   coverage: recoge el porcentaje de la tarifa que paga la aseguradora
#   remainder_payer: campo de selección para recoger si lo que no paga la aseguradora, lo paga el paciente, el hospital o la empresa del asegurado
#   insurance_subscriber: recoge el identificador de la empresa suscriptora del seguro
# Definimos nuevo campo "discount_by_default" para recoger el descuento por defecto de un plan para un producto
#   Sólo será visible para tipos de tarifa, visitas sistemáticas y la tarifa única de seguros
class InsurancePlan(metaclass = PoolMeta):
    'Insurance Plan'
    __name__ = 'gnuhealth.insurance.plan'
    _rec_name = 'name'

    insurance_company_type = fields.Function(fields.Char('Insurance company type',
                                             states={'invisible': True}),
                                             'on_change_with_insurance_company_type')

    insurance_subscriber = fields.Many2One(
        'party.party', 'Insurance subscriber',
        domain=[('insurance_company_type', '=', 's')],
        help="Insurance underwriting company",
        states={'required': Eval('insurance_company_type') == 'i',
                'invisible': And(Eval('insurance_company_type'),
                                 Eval('insurance_company_type') != 'i')},)

    coverage = fields.Float(
        'Coverage', digits=(3, 2),
        help="Insurance coverage on the price in Percentage",
        states={'required': Or(Eval('insurance_company_type') == 'c',
                               Eval('insurance_company_type') == 'i'),
                'invisible': And(Eval('insurance_company_type'),
                                 Eval('insurance_company_type') != 'c',
                                 Eval('insurance_company_type') != 'i')})

    remainder_payer = fields.Selection([
        (None, ''),
        ('h', 'Hospital'),
        ('p', 'Patient'),
        ('s', 'Insurance subscriber'),
        ], 'Remainder payer', select=True,
        help="Who pays what insurance does not cover",
        states={'invisible': Or(Not(Eval('coverage')),
                                Eval('coverage', 0) == 0,
                                Eval('coverage', 0) == 100),
                'required': And(Eval('coverage', 0) > 0,
                                Eval('coverage', 0) < 100)})

    discount_by_default = fields.Float(
        'Discount by default', digits=(3, 4),
        help="Discount in Percentage",
        states={'invisible': Or(Eval('insurance_company_type') == 'c',
                               Eval('insurance_company_type') == 'i',
                               Eval('insurance_company_type') == 's')})

    @fields.depends('company')
    def on_change_with_insurance_company_type(self, name=None):
        if self.company:
            return self.company.insurance_company_type
        return None

    def get_rec_name(self, name):
        if self.name.suffix_code:
            return self.name.name + ' - ' + self.name.suffix_code
        else:
            return self.name.name

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('name.template.name',) + tuple(clause[1:]),
                ('name.suffix_code',) + tuple(clause[1:]),
                ]

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.company.search_order=[('insurance_company_type', 'DESC'),
                                  ('name', 'ASC')]

# Se añade validación para que no se pueda hospitalizar a un paciente ya hospitalizado
    @classmethod
    def validate(cls, plans):
        super(InsurancePlan, cls).validate(plans)
        for plan in plans:
            plan.check_coverage()
            plan.check_remainder_payer()

    def check_coverage(self):
        if self.coverage:
            if self.coverage < 0\
            or self.coverage >100:
                raise CoverageOutOfRange(
                    gettext('health_ebome.msg_coverage_out_of_range')
                    )
            if self.coverage == 100:
                self.remainder_payer = None

    def check_remainder_payer(self):
        if self.remainder_payer:
            if self.insurance_company_type == 'c'\
            and self.remainder_payer == 's':
                raise BadRemainderPayer(
                    gettext('health_ebome.msg_bad_remainder_payer')
                    )

    @classmethod
    def view_attributes(cls):
        # Ocultar el grupo insurance cuando se trata de un tipo de tarifa distinto de 'i' o 'c'
        return super(InsurancePlan, cls).view_attributes() + [
                ('//group[@id="group_insurance_plan_insurance"]', 'states', {
                    'invisible': And(Eval('insurance_company_type'),
                                     Eval('insurance_company_type') != 'i',
                                     Eval('insurance_company_type') != 'c')
                })]

# Añadimos los campos funcionales tipo de tarifa, precio base y precio final
# Añadimos los campos funcionales unidades de código B para productos de laboratorio. Recoge el número de unidades de código Syndilab (B) que multiplicadas por el factor de multiplicación de dicho código da como resultado el precio final
# Añadimos los campos funcionales unidades de código K para productos de imagerie. Recoge el número de unidades de código HG Douala (K) que multiplicadas por el factor de multiplicación de dicho código da como resultado el precio final
class InsurancePlanProductPolicy(metaclass = PoolMeta):
    'Policy associated to the product on the insurance plan'
    __name__ = "gnuhealth.insurance.plan.product.policy"

    tarif_type = fields.Function(fields.Char('Tarif Type'),
                                 'get_tarif_type',
                                 searcher='search_tarif_type')

    start_price = fields.Function(fields.Numeric('Start price'),
                                  'get_start_price')

    final_price = fields.Function(fields.Numeric('Final price'), 'get_final_price')

    @fields.depends('plan')
    def get_tarif_type(self, name):
        return self.plan.company.rec_name

    @fields.depends('plan')
    def on_change_with_tarif_type(self):
        if self.plan:
            return self.plan.company.rec_name

    @classmethod
    def search_tarif_type(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('plan.company.rec_name', clause[1], value))
        return res

    @fields.depends('product')
    def on_change_with_start_price(self):
        if self.product:
            return self.product.list_price

    def get_start_price(self, name):
        return self.on_change_with_start_price()

    @fields.depends('discount', 'product')
    def on_change_with_final_price(self):
        Company = Pool().get('company.company')

        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)

        if self.product:
            if self.product.list_price:
                final_price = self.product.list_price
                if self.discount:
                    unit_price = self.product.list_price
                    final_price = unit_price * (1 - Decimal(str(self.discount/100)))
                    final_price = company.currency.round(final_price)
            else:
                final_price = 0
        else:
            final_price = None
        return final_price

    def get_final_price(self, name):
        return self.on_change_with_final_price()

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.plan.domain=[('company.insurance_company_type', 'not in', ['c', 'i', 's'])
                         ]
        cls.product.context.update({
            'modelo': 'gnuhealth.insurance.plan.product.policy',
            'id': Eval('id', 0),
            'plan': Eval('plan', 0)
            })
        cls.product.depends.append('plan')
        cls.product.domain=[('template.account_category', '>', 0),
                            ('OR',
                            [('catalog_name', '=', 'Assurance'),
                             ('template.is_for_insured', '=', True)
                            ],
                            [('catalog_name', '=', 'Visite'),
                             ('template.is_for_visit', '=', True)
                            ],
                            [('catalog_name', '=', 'Particular'),
                             ('template.is_for_particular', '=', True)
                            ])
                           ]
        cls.product.states = {'readonly': Not(Bool(Eval('plan')))}

        cls.discount = fields.Float(
            'Discount', digits=(3, 4),
            help="Discount in Percentage", required=True)


# Sustituimos el campo "tipo de visita" por el campo "tipo de tarifa", donde mostramos el tipo de tarifa y el régimen del paciente,
# recogidos en el campo _rec_name de gnuhealth.insurance
# En el campo "Especialidad" mostramos únicamente los valores "medicina general", "ginecología" y "maternidad"
# en el campo "Profesional de la salud" mostramos únicamente los profesionales que tengan como especialidad principal la seleccionada en el campo "Especialidad"
# En el campo "Prioridad" ponemos el valor "normal" por defecto
class Appointment(metaclass = PoolMeta):
    __name__ = 'gnuhealth.appointment'

    tarif_type = fields.Many2One('gnuhealth.insurance', 'Tarif type',
                                 states = {'readonly': Bool(Eval('tarif_type'))})

    @fields.depends('patient')
    def on_change_patient(self):
        if self.patient:
            self.tarif_type = self.patient.current_insurance.id

    @staticmethod
    def default_state():
        return 'confirmed'

    @fields.depends('speciality')
    def on_change_with_speciality(self):
        if self.speciality:
            old_speciality = self.speciality
            super().on_change_with_speciality()
            return old_speciality.id

    @staticmethod
    def default_speciality():
        return None

    @staticmethod
    def default_healthprof():
        return None

    @staticmethod
    def default_urgency():
        if Transaction().context.get('urgency') == 'c':
            return 'c'
        return 'a'

    @staticmethod
    def default_visit_type():
        if Transaction().context.get('urgency') == 'c':
            return 'new'
        return None

    @fields.depends('name')
    def urgency_level(self):
        if Transaction().context.get('urgency') == 'c':
            return [(None, ''),
                    ('c', 'Medical Emergency')]
        else:
            return [(None, ''),
                    ('a', 'Normal'),
                    ('b', 'Urgent')
                    ]

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.state.states = {'invisible' : True}
#
        cls.urgency = fields.Selection(
            'urgency_level',
            'Urgency', sort=False,
            states={'invisible' : Eval('context', {}).get('urgency') == 'c'})

        cls.visit_type = fields.Selection([
        (None, ''),
        ('new', 'New health condition'),
        ('followup', 'Followup'),
        ], 'Visit', sort=False,
        states = {'required' : True,
                  'readonly' : Eval('context', {}).get('urgency') == 'c'})

        cls.speciality = fields.Many2One(
            'gnuhealth.specialty', 'Specialty',
            domain=[('code', 'in', ['GENERAL', 'GYN', 'INTERNAL', 'Maternité', 'PEDIATRICS'])],
            search_order=[
                ('name', 'ASC')],
            help='Medical Specialty / Sector')

        cls.healthprof = fields.Many2One(
            'gnuhealth.healthprofessional', 'Health Prof',
            domain=[('specialties', 'where', ['specialty', '=', Eval('speciality')])],
            depends=['speciality'],
            search_order=[
                ('name.lastname', 'ASC'),
                ('name.name', 'ASC')],
            help='Health Professional',
            states = {'readonly': Not(Eval('speciality'))})

        cls.inpatient_registration_code = fields.Many2One(
        'gnuhealth.inpatient.registration', 'Inpatient Registration',
            domain=[('patient', '=', Eval('patient'))],
            depends=['patient'],
            help="Enter the patient hospitalization code")

#    @classmethod
#    def view_attributes(cls):
#        if Transaction().context.get('urgency') == 'c':
#            return super().view_attributes() + [
#                ('/form/group/label[@name="name"]', 'string', "Identification de l'urgence:"
#                )
#               ]
#        return super().view_attributes()

class ProcedureCode(metaclass = PoolMeta):
    __name__ = 'gnuhealth.procedure'

    active = fields.Boolean('Active')

    @classmethod
    def default_active(cls):
        return True


class Surgery(metaclass = PoolMeta):
    __name__ = 'gnuhealth.surgery'

    STATES = {'readonly': Eval('state') == 'signed'}

    patient_code = fields.Function(fields.Char('Code'),'get_patient_code')

    procedure = fields.Many2One(
            'gnuhealth.procedure','Procedure', required=True,
            search_order=[
                ('description', 'ASC')],
            help="The main procedure in the surgery")

    assistant = fields.Many2One(
        'gnuhealth.healthprofessional', 'Assistant',
        help="Assistant who worked in the procedure", states = STATES)

    circulating = fields.Many2One(
        'gnuhealth.healthprofessional', 'Circulating',
        help="Circulating who worked in the procedure", states = STATES)

    anesthesia = fields.Selection([
        (None, ''),
        ('R/A', 'Spinal'),
        ('A/G', 'General'),
        ('LOC', 'Local'),
        ('SED', 'Sedation'),
        ('R/A + A/G', 'Spinal + General'),
        ('R/A + SED', 'General + Sedation'),
        ('A/G + IOT', 'General + IOT'),
        ], 'Anesthesia', sort=False, states = STATES)

    operation = fields.Char('Operation', states=STATES)

    incident = fields.Char('Incident', states=STATES)

    diabete  = fields.Boolean('Diabète', states=STATES)

    asthme  = fields.Boolean('Asthme', states=STATES)

    hypertension  = fields.Boolean('Hypertension', states=STATES)

    bronchite  = fields.Boolean('Bronchite', states=STATES)

    cardiaque  = fields.Boolean('Cardiaque', states=STATES)

    tabaquisme = fields.Selection([
        (None, ''),
        ('0', 'Non'),
        ('1', 'Rarement'),
        ('3', 'Fréquemment'),
        ('5', 'Tous les jours'),
        ], 'Tabaquisme', sort=False, states = STATES)


    alcoolisme = fields.Selection([
        (None, ''),
        ('0', 'Non'),
        ('1', 'Rarement'),
        ('3', 'Fréquemment'),
        ('5', 'Tous les jours'),
        ], 'Alcoolisme', sort=False, states = STATES)

    poids = fields.Float('Poids', states=STATES)

    alergie = fields.Char('Alergie', states=STATES)

    autres = fields.Char('Autres', states=STATES)

    evaluation = fields.Char('Evaluation général', states=STATES)

    premedication = fields.Char('Prémédication', states=STATES)

    rester = fields.Char('Rester à jeun', states=STATES)

    traitement = fields.Char('Traitement en cours', states=STATES)

    fr = fields.Char('FR', states=STATES)

    temp = fields.Char('Temp', states=STATES)

    ta = fields.Char('TA', states=STATES)

    pouls = fields.Char('Pouls', states=STATES)

    conjonctives = fields.Char('Conjonctives', states=STATES)

    dentition = fields.Char('Dentition', states=STATES)

    cou = fields.Char('Cou', states=STATES)

    poumons = fields.Char('Poumons', states=STATES)

    coeur = fields.Char('Auscult. coeur', states=STATES)

    abdomen = fields.Char('Abdomen', states=STATES)

    oedemes = fields.Char('OEdemes', states=STATES)

    asa_text = fields.Char('ASA', states=STATES)

    mallampati_text = fields.Char('Mallampati', states=STATES)

    autres_examen = fields.Char('Autres', states=STATES)

    @fields.depends('patient')
    def get_patient_code(self, name):
        return self.patient.code

    @fields.depends('surgery_date')
    def on_change_with_surgery_end_date(self):
        return self.surgery_date + timedelta(hours=1)

    @classmethod
    def default_operating_room(cls):
        Operating_room = Pool().get('gnuhealth.hospital.or')(1)
        if Operating_room:
            return Operating_room.id

    @classmethod
    def default_classification(cls):
        return 'p'

    @classmethod
    def default_anesthetist(cls):
        anesthetist = get_health_professional()
        return anesthetist

    @classmethod
    def default_surgeon(cls):
        return None

    # Show the gender and age upon entering the patient
    # These two are function fields (don't exist at DB level)
    @fields.depends('patient')
    def on_change_patient(self):
        if (self.patient):
            if (self.patient.gender):
                self.gender = self.patient.gender
            if (self.patient.age):
                self.computed_age = self.patient.age
            if (self.patient.code):
                self.patient_code = self.patient.code
        else:
            self.gender = None
            self.computed_age = None
            self.patient_code = None

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.classification = fields.Selection([
            (None, ''),
            ('p', 'Programmed'),
            ('u', 'Urgent'),
            ], 'Urgency', help="Urgency level for this surgery", sort=False)
        cls.patient.states = cls.STATES
        cls.admission.states = cls.STATES
        cls.operating_room.states = cls.STATES
        cls.main_procedure.states = cls.STATES
        cls.supplies.states = cls.STATES
        cls.pathology.states = cls.STATES
        cls.classification.states = cls.STATES
        cls.surgeon.states = cls.STATES
        cls.anesthetist.states = cls.STATES
        cls.surgery_date.states = cls.STATES
        cls.surgery_end_date.states = cls.STATES
        cls.surgery_length.states = cls.STATES
        cls.age.states = cls.STATES
        cls.computed_age.states = cls.STATES
        cls.gender.states = cls.STATES
        cls.description.states = cls.STATES
        cls.preop_mallampati.states = cls.STATES
        cls.preop_bleeding_risk.states = cls.STATES
        cls.preop_oximeter.states = cls.STATES
        cls.preop_site_marking.states = cls.STATES
        cls.preop_antibiotics.states = cls.STATES
        cls.preop_sterility.states = cls.STATES
        cls.preop_asa.states = cls.STATES
        cls.preop_rcri.states = cls.STATES
        cls.surgical_wound.states = cls.STATES
        cls.extra_info.states = cls.STATES
        cls.anesthesia_report.states = cls.STATES
        cls.institution.states = cls.STATES
        cls.report_surgery_date.states = cls.STATES
        cls.report_surgery_time.states = cls.STATES
        cls.surgery_team.states = cls.STATES
        cls.postoperative_dx.states = cls.STATES
        #cls.on_change_patient = on_change_patient

# Se valida que el paciente que se quiere hospitalizar no esté ya hospitalizado
# La fecha de admisión puede ser una fecha pasada y se pasa de tipo datetime a tipo date
# Se cambian las opciones del tipo de admisión
# Se pone el médico tratante como obligatorio y sólo se pueden seleccionar los que tengan especialidad de medicina general
# Se ponen los campos "motivo de admisión" y "médico tratante" obligatorios
# Se pone la fecha de salida como no obligatoria y al dar al botón de salida se carga con la fecha del día. Se pasa de tipo datetime a tipo date
# Se añade nuevo campo para recoger el tipo de tarifa
# Se añaden campos de función para mostrar los datos: dosier, sexo, edad y servicio
# Se añaden búsquedas para los datos: dosier y servicio
class InpatientRegistration(metaclass = PoolMeta):
    'Patient admission History'
    __name__ = 'gnuhealth.inpatient.registration'

    STATES = {'readonly': Or(
        Eval('state') == 'done',
        Eval('state') == 'finished')}

    patient_code = fields.Function(fields.Char('Code'),
                                   'get_patient_code',
                                   searcher='search_patient_code')

    gender = fields.Function(fields.Selection([
        (None, ''),
        ('m', 'Male'),
        ('f', 'Female'),
        ('other', 'Other'),
        ('u', 'Unknown'),
        ], 'Gender'), 'get_gender')

    age = fields.Function(fields.Char('Age'),
                                   'get_age')

    insurance_plan = fields.Many2One(
        'gnuhealth.insurance',
        'Plan',
        states = {'readonly': Bool(Eval('insurance_plan'))})

    ward = fields.Function(fields.Char('Ward'),
                                   'get_ward',
                                   searcher='search_ward')

    service = fields.Many2One(
        'gnuhealth.health_service', 'Service',
        domain=[('patient', '=', Eval('patient')),
                ('insurance_plan', '=', Eval('insurance_plan')),
                ('state', '=', 'draft')],
        depends=['patient', 'insurance_plan', 'state'],
        states={'required': True,
                'readonly': Equal(Eval('state'), 'finished')},
        help="Service document associated to this inpatient registration")

    @fields.depends('patient')
    def get_patient_code(self, name):
        return self.patient.code

    @classmethod
    def search_patient_code(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('patient.code', clause[1], value))
        return res

    @fields.depends('patient')
    def get_gender(self, name):
        if self.patient.gender:
            return self.patient.gender
        return None

    @fields.depends('patient')
    def get_age(self, name):
        if self.patient.age:
            return self.patient.age
        return None

    @fields.depends('patient')
    def on_change_patient(self):
        if (self.patient):
            if (self.patient.gender):
                self.gender = self.patient.gender
            if (self.patient.age):
                self.age = self.patient.age
            if (self.patient.code):
                self.patient_code = self.patient.code
        else:
            self.gender = None
            self.age = None
            self.patient_code = None

    @fields.depends('patient')
    def on_change_with_insurance_plan(self):
        if self.patient and self.patient.current_insurance:
            return self.patient.current_insurance.id

    @classmethod
    def order_bed(cls, tables):
        pool = Pool()
        Bed = Pool().get('gnuhealth.hospital.bed')

        table, _ = tables[None]
        if 'bed' not in tables:
            bed = Bed.__table__()
            tables['bed'] = {
                None: (bed, table.bed == bed.id),
                }
        else:
            bed = tables['bed']

        return Bed.rec_name.convert_order(
                'rec_name', tables['bed'], Bed)

    @fields.depends('bed')
    def get_ward(self, name):
        if self.bed:
            if self.bed.ward:
                return self.bed.ward.name
        return None

    @classmethod
    def search_ward(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('bed.ward.name', clause[1], value))
        return res

    @classmethod
    def order_ward(cls, tables):
        pool = Pool()
        Ward = Pool().get('gnuhealth.hospital.ward')
        Bed = Pool().get('gnuhealth.hospital.bed')
        table, _ = tables[None]
        if 'bed' not in tables:
            bed = Bed.__table__()
            tables['bed'] = {
                None: (bed, table.bed == bed.id),
                }
        else:
            bed = tables['bed']

        if 'ward' not in tables:
            ward = Ward.__table__()
            tables['ward'] = {
                None: (ward, bed.ward == ward.id),
                }
        else:
            ward = tables['ward']

        return Ward.name.convert_order(
                'name', tables['ward'], Ward)

    @classmethod
    def default_attending_physician(cls):
        return get_health_professional()

    @classmethod
    def order_attending_physician(cls, tables):
        pool = Pool()
        Attendig_physician = Pool().get('gnuhealth.healthprofessional')
        Party = Pool().get('party.party')

        table, _ = tables[None]
        if 'attending_physician' not in tables:
            attending_physician = Attendig_physician.__table__()
            tables['attending_physician'] = {
                None: (attending_physician, table.attending_physician == attending_physician.id),
                }
        else:
            attending_physician = tables['attending_physician']

        if 'party' not in tables:
            party = Party.__table__()
            tables['party'] = {
                None: (party, attending_physician.name == party.id),
                }
        else:
            party = tables['party']

        return Party.lastname.convert_order(
                'lastname', tables['party'], Party)

    @classmethod
    def default_hospitalization_date(cls):
        return datetime.now()

    @fields.depends('discharge_date')
    def on_change_discharge_date(self):
        if self.discharge_date:
            if not self.discharged_by:
                self.discharged_by = get_health_professional()

    @fields.depends('discharge_reason', 'discharge_date', 'discharged_by')
    def on_change_discharge_reason(self):
        if self.discharge_reason:
            if not self.discharge_date:
                self.discharge_date = datetime.now()
            if not self.discharged_by:
                self.discharged_by = get_health_professional()

    @fields.depends('discharge_reason', 'discharge_date', 'discharged_by')
    def on_change_discharge_dx(self):
        if self.discharge_dx:
            if not self.discharge_date:
                self.discharge_date = datetime.now()
            if not self.discharged_by:
                self.discharged_by = get_health_professional()

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.admission_type = fields.Selection([
            (None, ''),
            ('urgent', 'Urgent'),
            ('consultation', 'Consultation'),
            ('maternity', 'Maternity'),
            ('elective', 'Programmed surgery'),
            ], 'Admission type', required=True, select=True, states=cls.STATES)
        cls.attending_physician = fields.Many2One(
            'gnuhealth.healthprofessional',
            'Attending Physician',
            domain=[('main_specialty.specialty.code', 'in', ['GENERAL', 'GYN', 'INTERNAL', 'PEDIATRICS'])],
            depends=['name'],
            search_order=[
                ('name.lastname', 'ASC'),
                ('name.name', 'ASC')],
            states=cls.STATES, required=True)
        cls.admission_reason.required=True
        cls.bed = fields.Many2One(
            'gnuhealth.hospital.bed', 'Hospital Bed',
            states={
                'required': Not(Bool(Eval('name'))),
                'readonly': Or(
                    Eval('state') == 'done',
                    Eval('state') == 'finished',
                    Bool(Eval('name')),
                            )
                },
            domain=[('state', '=', 'free')],
            depends=['name'],
            search_order=[
                ('ward.name', 'ASC'),
                ('name', 'ASC')]
            )
        cls.discharge_date = fields.DateTime(
            'Discharge Date',
            states=cls.STATES)
        cls.discharge_reason = fields.Selection([
            (None, ''),
            ('discharge', 'Medical discharge'),
            ('transfer', 'Transferred to another institution'),
            ('death', 'Death'),
            ('against_advice', 'Left against medical advice'),
            ('escape', 'Escape'),
            ], 'Discharge Reason',
            states={'readonly': Not(Equal(Eval('state'), 'hospitalized'))},
            help="Reason for patient discharge")
        cls.discharged_by = fields.Many2One(
            'gnuhealth.healthprofessional',
            'Discharged by',
            help="Health Professional that discharged the patient",
            domain=[('main_specialty.specialty.code', 'in', ['GENERAL', 'GYN', 'INTERNAL', 'PEDIATRICS'])],
            depends=['name'],
            search_order=[
                ('name.lastname', 'ASC'),
                ('name.name', 'ASC')],
            states=cls.STATES)
        cls._order.insert(0, ('hospitalization_date', 'DESC'))

        cls._buttons.update({
            'update_service': {'invisible': Not(Equal(Eval('state'), 'done'))
                }
            })

# El botón de confirmar hace 2 pasos: confirmar y hospitalizar.
# Al hospitalizar no se valida que la fecha de admisión sea la de hoy
# ni se tiene en cuenta la fecha de salida
# Tampoco se valida que la cama esté libre, ya que en la selección se muestran únicamente las camas libres
# una vez ocupada la cama, se comprueba si quedan más camas libres en el servicio y si no quedan se cambia el estado de éste
    @classmethod
    @ModelView.button
    def confirmed(cls, registrations):
        registration_id = registrations[0]
        Ward = Pool().get('gnuhealth.hospital.ward')
        Bed = Pool().get('gnuhealth.hospital.bed')
        Health_service = Pool().get('gnuhealth.health_service')
        cursor = Transaction().connection.cursor()
        ward_id = registration_id.bed.ward.id
        insurance_plan = registration_id.insurance_plan
        service_id = registration_id.service

        cls.write(registrations, {'state': 'hospitalized'})         #Estado tras admisión
        Bed.write([registration_id.bed], {'state': 'occupied'})     #Estado tras admisión

        cursor.execute(
            "SELECT COUNT(*) \
            FROM gnuhealth_inpatient_registration \
            INNER JOIN (gnuhealth_hospital_bed \
              INNER JOIN gnuhealth_hospital_ward ON gnuhealth_hospital_bed.ward=gnuhealth_hospital_ward.id) \
              ON gnuhealth_inpatient_registration.bed=gnuhealth_hospital_bed.id \
            WHERE gnuhealth_inpatient_registration.state = %s \
              AND gnuhealth_hospital_ward.id = CAST(%s AS INTEGER) ",
            ('hospitalized', str(ward_id)))

        res = cursor.fetchone()

        if res[0] >= registration_id.bed.ward.number_of_beds:
            Ward.write([registration_id.bed.ward], {'state': 'full'})     #Estado del servicio tras admisión

        health_service, = Health_service.search([('id', '=', service_id)])

        health_service_rec = []
        health_service_rec.append(health_service)
        vals = {'hospitalized_to_invoice': True}
        Health_service.write(health_service_rec, vals)

# Se libera la cama en el alta hospitalaria
# Una vez desocupada la cama, se comprueba si quedan camas libres en el servicio y si quedan se cambia el estado de éste
# Se actualiza el campo hospitalized_to_invoice de gnuhealth.health_service
# Se asigna el diagnóstico, la fecha de hospitalización y la fecha de salida a la hoja de facturación asignada

    @classmethod
    @ModelView.button
    def discharge(cls, registrations):
        registration_id = registrations[0]
        Bed = Pool().get('gnuhealth.hospital.bed')
        Ward = Pool().get('gnuhealth.hospital.ward')
        HealthService = Pool().get('gnuhealth.health_service')
        cursor = Transaction().connection.cursor()
        ward_id = registration_id.bed.ward.id

#        signing_hp = get_health_professional()
#        if not signing_hp:
#            raise NoAssociatedHealthProfessional(
#                gettext('health_inpatient.'
#                        'msg_no_associated_health_professional'))

        cls.write(registrations, {'state': 'done'})

#        Bed.write([registration_id.bed], {'state': 'to_clean'})
        Bed.write([registration_id.bed], {'state': 'free'})

        cursor.execute(
            "SELECT COUNT(*) \
            FROM gnuhealth_inpatient_registration \
            INNER JOIN (gnuhealth_hospital_bed \
              INNER JOIN gnuhealth_hospital_ward ON gnuhealth_hospital_bed.ward=gnuhealth_hospital_ward.id) \
              ON gnuhealth_inpatient_registration.bed=gnuhealth_hospital_bed.id \
            WHERE gnuhealth_inpatient_registration.state = %s \
              AND gnuhealth_hospital_ward.id = CAST(%s AS INTEGER) ",
            ('hospitalized', str(ward_id)))

        res = cursor.fetchone()

        if res[0] < registration_id.bed.ward.number_of_beds:
            Ward.write([registration_id.bed.ward], {'state': 'beds_available'})     #Estado del servicio tras librar la cama

        hservice = []
        service_data = {}

        hservice.append(registration_id.service)

        service_data['diagnosis'] = registration_id.discharge_dx.name
        service_data['date_of_entry'] = datetime.date(registration_id.hospitalization_date)
        service_data['exit_date'] = datetime.date(registration_id.discharge_date)
        service_data['hospitalized_to_invoice'] = True

        HealthService.write(hservice, service_data)

# Se actualiza el campo hospitalized_to_invoice de gnuhealth.health_service
    @classmethod
    @ModelView.button
    def bedclean(cls, registrations):
        registration_id = registrations[0]
#        Ward = Pool().get('gnuhealth.hospital.ward')
        Bed = Pool().get('gnuhealth.hospital.bed')
        Health_service = Pool().get('gnuhealth.health_service')
#        cursor = Transaction().connection.cursor()
#        ward_id = registration_id.bed.ward.id
        patient_id = registration_id.patient
        service_id = registration_id.service
        cls.write(registrations, {'state': 'finished'})

#        Bed.write([registration_id.bed], {'state': 'free'})
#
#        cursor.execute(
#            "SELECT COUNT(*) \
#            FROM gnuhealth_inpatient_registration \
#            INNER JOIN (gnuhealth_hospital_bed \
#              INNER JOIN gnuhealth_hospital_ward ON gnuhealth_hospital_bed.ward=gnuhealth_hospital_ward.id) \
#              ON gnuhealth_inpatient_registration.bed=gnuhealth_hospital_bed.id \
#            WHERE gnuhealth_inpatient_registration.state = %s \
#              AND gnuhealth_hospital_ward.id = CAST(%s AS INTEGER) ",
#            ('hospitalized', str(ward_id)))
#
#        res = cursor.fetchone()
#
#        if res[0] < registration_id.bed.ward.number_of_beds:
#            Ward.write([registration_id.bed.ward], {'state': 'beds_available'})     #Estado del servicio tras librar la cama

        health_service, = Health_service.search([('id', '=', service_id)])

        health_service_rec = []
        health_service_rec.append(health_service)
        vals = {'hospitalized_to_invoice': False}
        Health_service.write(health_service_rec, vals)

# Se añade validación para que no se pueda hospitalizar a un paciente ya hospitalizado
    @classmethod
    def validate(cls, registrations):
        super(InpatientRegistration, cls).validate(registrations)
        for registration in registrations:
            registration.check_patient_hospitalized()
            registration.check_discharge_context()

    def check_patient_hospitalized(self):
        Inpatient = Pool().get('gnuhealth.inpatient.registration')

        if self.state == 'free':
            inpatient = Inpatient.search([('patient', '=', self.patient.id),
                    ('state', '=', 'hospitalized')])

            if inpatient:
                raise AlreadyHospializedPatient(
                    gettext('health_ebome.'
                            'msg_already_hospitalized_patient'))

# Cambia este check ya que ahora se obliga también a informar la fecha de salida
    def check_discharge_context(self):
        if ((not self.discharge_reason or not self.discharge_dx
            or not self.discharge_date or not self.admission_reason
            or not self.discharged_by)
                and self.state == 'done'):
            raise DischargeReasonNeeded(
                gettext('health_ebome.msg_discharge_reason_needed'))

# Se añaden sendos campos booleanos para saber si el producto pertenece al catálogo de productos para particulares,
# para visitas sistemáticas y/o para asegurados
class Template(metaclass = PoolMeta):
    "Product Variant"
    __name__ = "product.template"

    STATES = {'invisible': Not(Eval('account_category', 0) > 0)}

    is_for_particular = fields.Boolean('Is for particular?', states=STATES)

    is_for_insured = fields.Boolean('Is for insured?', states=STATES)

    is_for_visit = fields.Boolean('Is for systematic visit?', states=STATES)

    tarif = fields.One2Many(
        'gnuhealth.insurance.plan.product.policy',
        'name', 'Service Line', help="Service Line")

    @classmethod
    def view_attributes(cls):
        # Ocultar el grupo catalgos cuando el producto no tiene categoría contable
        return super(Template, cls).view_attributes() + [
                ('//group[@id="group_catalogs"]', 'states', cls.STATES)]

# Se añade nuevo campo “catalog_name” de tipo Funcional para recoger el catálogo a mostrar
# según el tipo de tarifa del paciente al que se está ofertando el producto
# y poder presentar así únicamente los productos ofertables a ese tipo de paciente
# Se modifica el rec_name para que,
    # si se trata de una cama, muestre únicamente el sufijo del código (sin código y sin nombre del producto)
    # si se trata de un régimen, muestre el nombre del producto y el sufijo del código (sin el código)
# Se añade un campo funcional para saber qué productos hay que presentar según sea el paciente particular o asegurado
class Product(metaclass = PoolMeta):
    "Product"
    __name__ = "product.product"

    catalog_name = fields.Function(fields.Char(' Catalog'),
                                   'get_catalog_name',
                                   searcher='search_catalog_name')

    tarif = fields.One2Many(
        'gnuhealth.insurance.plan.product.policy',
        'product', 'Tarif')

    def get_catalog_name(self, name):
        pool = Pool()
        Service = pool.get('gnuhealth.health_service')
        Plan = pool.get('gnuhealth.insurance.plan')

        if Transaction().context.get('modelo') == 'gnuhealth.health_service.line':
            service, = Service.search([('id','=',Transaction().context.get('service'))])
            if service:
                if service.insurance_plan.company.insurance_company_type == "i"\
                or service.insurance_plan.company.insurance_company_type == "c":
                    return "Assurance"
                elif service.insurance_plan.company.insurance_company_type == "v":
                    return "Visite"
                else:
                    return "Particular"

        if Transaction().context.get('modelo') == 'gnuhealth.insurance.plan.product.policy':
            plan, = Plan.search([('id','=',Transaction().context.get('plan'))])
            if plan:
                if plan.company.insurance_company_type == "x":
                    return "Assurance"
                elif plan.company.insurance_company_type == "v":
                    return "Visite"
                else:
                    return "Particular"

        return 'no hay línea'

    @classmethod
    def search_catalog_name(cls, name, clause):
        pool = Pool()
        Service = pool.get('gnuhealth.health_service')
        Plan = pool.get('gnuhealth.insurance.plan')
        t = pool.get('product.template')

        temp = t.__table__()
        prod = cls.__table__()

        j = prod.join(temp, condition=prod.template == temp.id)
        i = j.select(prod.id, where=temp.is_for_insured == True)
        v = j.select(prod.id, where=temp.is_for_visit == True)
        p = j.select(prod.id, where=temp.is_for_particular == True)

        if Transaction().context.get('modelo') == 'gnuhealth.health_service.line':
            service, = Service.search([('id','=',Transaction().context.get('service'))])
            if service:
                if service.insurance_plan.company.insurance_company_type == "i"\
                or service.insurance_plan.company.insurance_company_type == "c":
                    return [('id', 'in', i)]
                elif service.insurance_plan.company.insurance_company_type == "v":
                    return [('id', 'in', v)]
                else:
                    return [('id', 'in', p)]

        if Transaction().context.get('modelo') == 'gnuhealth.insurance.plan.product.policy':
            plan, = Plan.search([('id','=',Transaction().context.get('plan'))])
            if plan:
                if plan.company.insurance_company_type == "x":
                    return [('id', 'in', i)]
                elif plan.company.insurance_company_type == "v":
                    return [('id', 'in', v)]
                else:
                    return [('id', 'in', p)]

        return []

    def get_rec_name(self, name):
        if self.is_bed == True:
            if self.suffix_code:
                return self.suffix_code

        if self.is_insurance_plan == True:
            if self.suffix_code:
                return self.name + ' - ' + self.suffix_code
            else:
                return self.name

        if self.code:
            return '[' + self.code + '] ' + self.name
        else:
            return self.name

# Se añade campo "active" para poder activar y desactivar servicios
class HospitalWard(metaclass = PoolMeta):
    'Hospital Ward'
    __name__ = 'gnuhealth.hospital.ward'

    active = fields.Boolean('Active')

    @classmethod
    def default_active(cls):
        return True

# Se añade campo "active" para poder activar y desactivar camas
# Se modifica para que el rec_name sea el rec_name del producto y no su nombre
# Se modifica la busqueda del campo "rec_name" para que busque por el código de la cama en lugar de por el nombre del producto
class HospitalBed(metaclass = PoolMeta):
    'Hospital Bed'
    __name__ = 'gnuhealth.hospital.bed'

    active = fields.Boolean('Active')

    @classmethod
    def default_active(cls):
        return True

    def get_rec_name(self, name):
        if self.name:
            return self.name.suffix_code

    @classmethod
    def search_rec_name(cls, name, clause):
        return [('name.suffix_code',) + tuple(clause[1:])]

    @classmethod
    def order_rec_name(cls, tables):
        pool = Pool()
        Product = pool.get('product.product')
        table, _ = tables[None]
        if 'product' not in tables:
            product = Product.__table__()
            tables['product'] = {
                None: (product, table.name == product.id),
                }
        else:
            product = tables['product']

        return Product.suffix_code.convert_order('suffix_code',
            tables['product'], Product)

#Se ocultan y se ponen como opcionales los campos "reason" y "orig_bed_state"
#Se muestran solo las camas que están libres
class CreateBedTransferInit(metaclass = PoolMeta):
    'Create Bed Transfer Init'
    __name__ = 'gnuhealth.bed.transfer.init'


    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.newbed = fields.Many2One(
            'gnuhealth.hospital.bed', 'New Bed',
            domain=[('state', '=', 'free')],
            required=True, select=True,
            search_order=[
                ('ward.name', 'ASC'),
                ('name', 'ASC')]
            )

        cls.reason = fields.Char('Reason', required=False)

        cls.orig_bed_state = fields.Selection((
            (None, ''),
            ('free', 'Free'),
            ('reserved', 'Reserved'),
            ('occupied', 'Occupied'),
            ('to_clean', 'Needs cleaning'),
            ('na', 'Not available'),
            ), 'Bed of origin Status', sort=False, required=False)

# La cama origen queda libre (status='free') y la destino ocupada (status='occupied')
class CreateBedTransfer(Wizard):
    'Create Bed Transfer'
    __name__ = 'gnuhealth.bed.transfer.create'

    start = StateView(
        'gnuhealth.bed.transfer.init',
        'health_inpatient.view_patient_bed_transfer', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button(
                'Transfer Patient', 'create_bed_transfer', 'tryton-ok',
                True),
            ])
    create_bed_transfer = StateTransition()

    def transition_create_bed_transfer(self):
        inpatient_registrations = Pool(). \
            get('gnuhealth.inpatient.registration')
        bed = Pool().get('gnuhealth.hospital.bed')

        registrations = inpatient_registrations.browse(
            Transaction().context.get('active_ids'))

        # Don't allow mass changes. Work on a single record
        if len(registrations) > 1:
            raise ManyRecordsChosen(
                gettext('health_inpatient.msg_many_records_chosen'))

        registration = registrations[0]
        current_bed = registration.bed
        destination_bed = self.start.newbed

        # Check that the new bed is free
        if (destination_bed.state == 'free'):

           # Update the hospitalization record
            hospitalization_info = {}

            hospitalization_info['bed'] = destination_bed

            # Update the hospitalization data
            transfers = []
            transfers.append(('create', [{
                            'transfer_date': datetime.now(),
                            'bed_from': current_bed,
                            'bed_to': destination_bed,
                        }]))
            hospitalization_info['bed_transfers'] = transfers

            inpatient_registrations.write([registration], hospitalization_info)

            # Update bed status with the one given in the transfer
            bed.write([current_bed], {'state': 'free'})

            # Set as occupied the new bed
            bed.write([destination_bed], {'state': 'occupied'})

        else:
            raise DestinationBedNotAvailable(
                gettext('health_inpatient.msg_destination_bed_not_available'))

        return 'end'

# Definimos el campo "tipo de tarifa" como obligatorio
# Definimos el campo "insurance_holder" visible sólo si el tipo de tarifa es una aseguradora. Para ello definimos un campo funcional que
# recoge el tipo de tipo de tarifa
# Aunque no cambiamos el texto del botón "Set to Draft", lo volvemos a definir
# para que se tome en cuenta su traducción, recogida en fr.po de health_ebome
# Nuevo campo "hospitalized_to_invoice" para identificar las hojas de servicio de pacientes hospitalizados con líneas pendientes de facturar
# Este campo es invisible y se actualiza en las validaciones de las vistas "gnuhealth.inpatient.registration" y "gnuhealth.health_service.line"
# Se redefinen las funciones al cambiar de valor los campos "patient" y "insurance holder"
# Se añade botón para pasar la hoja de facturación de borrador a facturada
class HealthService(metaclass = PoolMeta):
    __name__ = 'gnuhealth.health_service'

    STATE2 = {'invisible': And(Eval('type_tarif_type'),
                               Eval('type_tarif_type') != 'i',
                               Eval('type_tarif_type') != 'c',
                               Eval('patient_is_insurance_holder') == 'Y'),
              'readonly': Bool(Eval('service_line'))}

    hospitalized_to_invoice = fields.Boolean('Hospitalized pending to invoice')

    type_tarif_type = fields.Function(fields.Char('Type de type de tarif'), 'on_change_with_type_tarif_type')

    patient_is_insurance_holder = fields.Function(fields.Char('Patient is insurance_holder?'), 'on_change_with_patient_is_insurance_holder')

    diagnosis = fields.Char('Diagnosis', states = {'invisible' : True})

    date_of_entry = fields.Date('Date of entry', states = {'invisible' : True})

    exit_date = fields.Date('Exit date', states = {'invisible' : True})

    @classmethod
    def default_hospitalized_to_invoice(cls):
        return False

    @fields.depends('patient')
    def on_change_with_insurance_plan(self, name=None):
        if self.patient:
            return self.patient.current_insurance and \
                self.patient.current_insurance.id
        else:
            return None

    @fields.depends('insurance_plan')
    def on_change_with_type_tarif_type(self, name=None):
        if self.insurance_plan:
            return self.insurance_plan.company.insurance_company_type
        return None

    @fields.depends('patient', 'insurance_holder')
    def on_change_with_patient_is_insurance_holder(self, name=None):
        if self.patient and self.insurance_holder:
            if self.patient.name.id == self.insurance_holder.id:
                return "Y"
            else:
                return "N"
        else:
            return "N"

    @fields.depends('patient')
    def on_change_patient(self):
        if self.patient:
            if not self.insurance_holder:
                self.insurance_holder = self.patient.name

    @fields.depends('insurance_holder')
    def on_change_insurance_holder(self):
        pass
#        if self.patient:
#            self.patient.current_insurance and self.patient.current_insurance.id
#            self.type_tarif_type = self.patient.current_insurance.company.insurance_company_type
#        else:
#            self.insurance_plan = None
#        if not self.insurance_holder or \
#            self.insurance_holder and self.patient and \
#            self.insurance_holder.id != self.patient.name.id :
#            self.insurance_plan = None
#        if self.insurance_holder and self.patient and \
#            self.insurance_holder.id == self.patient.name.id:
#            self.insurance_plan = \
#                self.patient.current_insurance and \
#                self.patient.current_insurance.id
#            self.type_tarif_type = self.patient.current_insurance.company.insurance_company_type

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.insurance_plan.domain = [()]
        cls.insurance_plan.states = {'readonly': True, 'required' : True}
        cls.insurance_holder.states = cls.STATE2
        cls._buttons.update({
            'button_set_to_draft': {
                'invisible': Equal(Eval('state'), 'draft')},
            'button_set_to_invoiced': {
                'invisible': Or(Equal(Eval('state'), 'invoiced'),
                                Equal(Eval('type_tarif_type'), 'i'),
                                Equal(Eval('type_tarif_type'), 'c')
                               )}
                })
        cls.service_line.states = {'readonly': Not(Bool(Eval('name')))}

    @classmethod
    @ModelView.button
    def button_set_to_invoiced(cls, services):
#        pool = Pool()
#        HealthService = pool.get('gnuhealth.health_service')

#        services = HealthService.browse(Transaction().context.get(
#            'active_ids'))
        for service in services:
            if service.state == 'invoiced':
                raise ServiceAlreadyInvoiced(
                    gettext('health_ebome.msg_service_already_invoiced'))
            for line in service.service_line:
                if line.to_invoice:
                    raise LineToInvoice(
                        gettext('health_ebome.msg_lines_to_invoice'))

        cls.write(services, {'state': 'invoiced'})

# Se añade validación para que no se pueda generar hoja de facturación para el tipo de tarifa 'Assurance' (obsoleto)
    @classmethod
    def validate(cls, registrations):
        super(HealthService, cls).validate(registrations)
        for registration in registrations:
            registration.check_tarif_type()

    def check_tarif_type(self):
        if self.insurance_plan.company.name == 'Assurance':
                raise BadTarifType(
                    gettext('health_ebome.'
                            'msg_bad_tarif_type'))

# Se modifica el contexto para que desde la clase Product se pueda acceder a la hoja de servicio
# en la que se está seleccionando el producto
# Se muestran únicamente los productos que tienen categoría contable y pertenezcan al catálogo de
# pacientes particulares o pacientes asegurados, según el tipo de tarifa del paciente
# Se ordenan las líneas de servicio por categoría contable del producto y por nombre del producto con el fin de sacar la factura por capítulos
class HealthServiceLine(metaclass = PoolMeta):
    'Health Service'
    __name__ = 'gnuhealth.health_service.line'

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.product.context.update({
            'modelo': 'gnuhealth.health_service.line',
            'id': Eval('id', 0),
            'service': Eval('name', 0)
            })
        cls.product.depends.append('name')
        cls.product.domain=[('template.account_category', '>', 0),
                            ('OR',
                            [('catalog_name', '=', 'Assurance'),
                             ('template.is_for_insured', '=', True)
                            ],
                            [('catalog_name', '=', 'Visite'),
                             ('template.is_for_visit', '=', True)
                            ],
                            [('catalog_name', '=', 'Particular'),
                             ('template.is_for_particular', '=', True)
                            ])
                           ]
        cls._order.insert(0, ('product.template.account_category', 'ASC NULLS LAST'))
        cls._order.insert(1, ('product.template.name', 'ASC'))


# Se controlan las líneas a facturar para actualizar campo "hospitalized_to_invoice" de gnuhealth_health_service
    @classmethod
    def validate(cls, registrations):
        super(HealthServiceLine, cls).validate(registrations)
        pool = Pool()
        Health_service = pool.get('gnuhealth.health_service')
        Inpatient = pool.get('gnuhealth.inpatient.registration')
        healthservice, = Health_service.search([('id', '=', registrations[0].name)])
        inpatients = Inpatient.search([('patient', '=', healthservice.patient),
                                       ('service', '=', healthservice.id),
                                       ('state', 'in' ,['hospitalized', 'done'])])

        lines_to_invoice = 0

        if inpatients:
            if inpatients[0]:
                for registration in registrations:
                    if registration.to_invoice == True:
                        lines_to_invoice += 1

        if healthservice.hospitalized_to_invoice == True:
            if lines_to_invoice == 0:
                health_service_rec = []
                health_service_rec.append(healthservice)

                vals = {'hospitalized_to_invoice': False}

                Health_service.write(health_service_rec, vals)
        else:
            if lines_to_invoice > 0:
                health_service_rec = []
                health_service_rec.append(healthservice)

                vals = {'hospitalized_to_invoice': True}

                Health_service.write(health_service_rec, vals)

# Añadir sendos parámetros para recoger el factor de multiplicación del código Syndilab (B), el código HG Douala (K)
# y el código AMI, utilizados en las facturas para las compañías aseguradoras
class Company(metaclass=PoolMeta):
    __name__ = 'company.company'

    b_code_factor = fields.Numeric('B Code Factor')

    k_code_factor = fields.Numeric('K Code Factor')

    ami_code_factor = fields.Numeric('AMI Code Factor')

    @classmethod
    def default_b_code_factor(cls):
        return 180

    @classmethod
    def default_k_code_factor(cls):
        return 900

    @classmethod
    def default_ami_code_factor(cls):
        return 300

# Añadir campo funcional con el número de dossier del paciente
# Añadir campo funcional con el tipo de tipo de tarifa para poder emitir factura para aseguradoras distinta a la factura normal
# Añadir campos funcionales con el titular y el suscriptor del seguro si el paciente viene con cobertura de una compañía de seguros
# Añadir campo funcional con el importe bruto total de la factura para presentarlo en las facturas para aseguradoras
# Añadir campos con diagnosis, fecha de hospitalización y fecha de salida, que son datos obligatorios en las facturas para aseguradoras
# Quitar el botón de validación y permitir pagos por importes negativos
# Permitir pagos por importes negativos también en facturas totalmente pagadas
# Permitir anular facturas publicadas solo si el importe a pagar es el total de la factura

class Invoice(metaclass = PoolMeta):
    'Invoice'
    __name__ = 'account.invoice'

    patient_code = fields.Function(fields.Char('Code'),
                                   'get_patient_code',
                                   searcher='search_patient_code')

    insurance_holder = fields.Function(
        fields.Many2One('party.party', 'Insurance Holder',  states={'invisible' : And(Eval('type_tarif_type') != 'i',
                                                                                      Eval('type_tarif_type') != 'c')}),
        'get_insurance_holder')

    insurance_coverage = fields.Function(
        fields.Float('Coverage', digits=(3, 2)), 'get_insurance_coverage')

    diagnosis = fields.Char('Diagnosis', states = {'readonly': Eval('state') != 'draft',
                                                   'required': Or(Eval('type_tarif_type') == 'i',
                                                                  Eval('type_tarif_type') == 'c'),
                                                   'invisible' : And(Eval('type_tarif_type') != 'i',
                                                                     Eval('type_tarif_type') != 'c')})

    date_of_entry = fields.Date('Date of entry', states = {'invisible' : True, 'required': Or(Eval('type_tarif_type') == 'i',
                                                                                              Eval('type_tarif_type') == 'c')})

    exit_date = fields.Date('Exit date', states = {'invisible' : True, 'required': Or(Eval('type_tarif_type') == 'i',
                                                                                      Eval('type_tarif_type') == 'c')})

    base_total_amount = fields.Function(fields.Numeric('Base Total', digits=(16,
                Eval('currency_digits', 2)), depends=['currency_digits']),
        'get_base_total_amount')

    @fields.depends('patient')
    def get_patient_code(self, name):
        try:
            return self.patient.code
        except:
            return None

    @classmethod
    def search_patient_code(cls, name, clause):
        return [
            ('lines.origin.name.patient.code',
             clause[1],
             clause[2],
             'gnuhealth.health_service.line')]

    def get_insurance_holder(self, name):
        pool = Pool()
        HealthServiceLine = pool.get('gnuhealth.health_service.line')
        try:
            if isinstance(self.lines[0].origin, HealthServiceLine):
                if self.lines[0].origin.name.insurance_plan.company.insurance_company_type == 'i'\
                or self.lines[0].origin.name.insurance_plan.company.insurance_company_type == 'c':
                    try:
                        return self.lines[0].origin.name.insurance_holder.id
                    except:
                        return None
                else:
                    return None
            else:
                return None
        except:
            return None

    def get_insurance_coverage(self, name):
        pool = Pool()
        HealthServiceLine = pool.get('gnuhealth.health_service.line')
        try:
            if isinstance(self.lines[0].origin, HealthServiceLine):
                if self.lines[0].origin.name.insurance_plan.company.insurance_company_type == 'i'\
                or self.lines[0].origin.name.insurance_plan.company.insurance_company_type == 'c':
                    for line in self.lines:
                        if line.type == 'line' and line.discount_or_coverage:
                            return line.discount_or_coverage
                    return 100
                else:
                    return 100
            else:
                return 100
        except:
            return 100

    @fields.depends('insurance_coverage', 'total_amount')
    def on_change_with_base_total_amount(self):
        base_total_amount = float(self.total_amount * 100) / float(self.insurance_coverage)
        return Decimal(round(base_total_amount))

    def get_base_total_amount(self, name):
        return self.on_change_with_base_total_amount()

#        try:
#            if self.lines[0].origin.name.insurance_plan.company.insurance_company_type == 'i'\
#            or self.lines[0].origin.name.insurance_plan.company.insurance_company_type == 'c':
#                if self.party.insurance_company_type == 'i'\
#                or self.party.insurance_company_type == 'c':
#                    coverage = self.lines[0].origin.name.insurance_plan.plan_id.coverage
#                else:
#                    coverage = 100 - self.lines[0].origin.name.insurance_plan.plan_id.coverage
#            else:
#                coverage = 100
#        except:
#            coverage = 100
#
#        base_total_amount = (self.total_amount * 100) / Decimal(coverage)
#
#        return base_total_amount

    def pay_invoice(self, amount, payment_method, date, description=None,
            amount_second_currency=None, second_currency=None, overpayment=0):
        '''
        Adds a payment of amount to an invoice using the journal, date and
        description.
        If overpayment is set, then only the amount minus the overpayment is
        used to pay off the invoice.
        Returns the payment lines.
        '''
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.move.line')
        Period = pool.get('account.period')

        pay_line = Line(account=self.account)
        counterpart_line = Line()
        lines = [pay_line, counterpart_line]

        pay_amount = amount - overpayment
#
# Se cambia esta sentencia para admitir pagos por importe negativo
#
#        if pay_amount >= 0:
#            if self.type == 'out':
#                pay_line.debit, pay_line.credit = 0, pay_amount
#            else:
#                pay_line.debit, pay_line.credit = pay_amount, 0
#        else:
#            if self.type == 'out':
#                pay_line.debit, pay_line.credit = -pay_amount, 0
#            else:
#                pay_line.debit, pay_line.credit = 0, -pay_amount
        if self.type == 'out':
            pay_line.debit, pay_line.credit = 0, pay_amount
        else:
            pay_line.debit, pay_line.credit = pay_amount, 0

        if overpayment:
            overpayment_line = Line(account=self.account)
            lines.insert(1, overpayment_line)
            overpayment_line.debit = (
                abs(overpayment) if pay_line.debit else 0)
            overpayment_line.credit = (
                abs(overpayment) if pay_line.credit else 0)

#
# Se cambia esta sentencia para admitir pagos por importe negativo
#
#        counterpart_line.debit = abs(amount) if pay_line.credit else 0
#        counterpart_line.credit = abs(amount) if pay_line.debit else 0
        counterpart_line.debit = amount if pay_line.credit else 0
        counterpart_line.credit = amount if pay_line.debit else 0
        if counterpart_line.debit:
            payment_acccount = 'debit_account'
        else:
            payment_acccount = 'credit_account'
        counterpart_line.account = getattr(
            payment_method, payment_acccount).current(date=date)

        for line in lines:
            if line.account.party_required:
                line.party = self.party
            if amount_second_currency:
                line.amount_second_currency = amount_second_currency.copy_sign(
                    line.debit - line.credit)
                line.second_currency = second_currency

        period_id = Period.find(self.company.id, date=date)

        move = Move(
            journal=payment_method.journal, period=period_id, date=date,
            origin=self, description=description,
            company=self.company, lines=lines)
        move.save()
        Move.post([move])

        payment_lines = [l for l in move.lines if l.account == self.account]
        payment_line = [l for l in payment_lines
            if (l.debit, l.credit) == (pay_line.debit, pay_line.credit)][0]
        self.add_payment_lines({self: [payment_line]})
        return payment_lines

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.patient = fields.Function(
            fields.Many2One(
                'gnuhealth.patient', 'Patient',
                help="Patient in the invoice"),
            'get_patient',
            searcher='search_patient')

        cls._buttons.update({
                'validate_invoice': {
                    'pre_validate':
                        ['OR',
                            ('invoice_date', '!=', None),
                            ('type', '!=', 'in'),
                        ],
                    'invisible' : True,
                    },
                'process': {
                    'invisible': True,
                    },
                })

# Se redefine campo "patient" para añadir función de búsqueda
    def get_patient(self, name):
        try:
            return self.lines[0].origin.name.patient.id
        except:
            return None

    @classmethod
    def search_patient(cls, name, clause):
        return [
            ('lines.origin.name.patient.name.name',
             clause[1],
             clause[2],
             'gnuhealth.health_service.line')]

# Se cambia esta función para que la fecha de pago se calcule a partir de la fecha de publicación de la factura y no a partir de la fecha de generación de la misma
    def get_move(self):
        '''
        Compute account move for the invoice and return the created move
        '''
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        Date = pool.get('ir.date')
        Warning = pool.get('res.user.warning')
        Lang = pool.get('ir.lang')

        today = Date.today()

        if self.move:
            return self.move
        self.update_taxes(exception=True)
        move_lines = []
        for line in self.lines:
            move_lines += line.get_move_lines()
        for tax in self.taxes:
            move_lines += tax.get_move_lines()

        total = sum(l.debit - l.credit for l in move_lines)
        if self.payment_term:
#            payment_date = self.payment_term_date or self.invoice_date
            payment_date = self.payment_term_date or today
            term_lines = self.payment_term.compute(
                total, self.company.currency, payment_date)
        else:
            term_lines = [(self.payment_term_date or today, total)]
        if self.currency != self.company.currency:
            remainder_total_currency = self.total_amount.copy_sign(total)
        else:
            remainder_total_currency = 0
        past_payment_term_dates = []
        for date, amount in term_lines:
            line = self._get_move_line(date, amount)
            if line.amount_second_currency:
                remainder_total_currency += line.amount_second_currency
            move_lines.append(line)
            if self.type == 'out' and date < today:
                past_payment_term_dates.append(date)
        if any(past_payment_term_dates):
            lang = Lang.get()
            warning_key = 'invoice_payment_term_%d' % self.id
            if Warning.check(warning_key):
                raise InvoicePaymentTermDateWarning(warning_key,
                    gettext('account_invoice'
                        '.msg_invoice_payment_term_date_past',
                        invoice=self.rec_name,
                        date=lang.strftime(min(past_payment_term_dates))))
        if not self.currency.is_zero(remainder_total_currency):
            move_lines[-1].amount_second_currency -= \
                remainder_total_currency

        accounting_date = self.accounting_date or self.invoice_date
        period_id = Period.find(self.company.id, date=accounting_date)

        move = Move()
        move.journal = self.journal
        move.period = period_id
        move.date = accounting_date
        move.origin = self
        move.company = self.company
        move.lines = move_lines
        return move

# Se cambia esta función para que no se valide si hay facturas de fecha posterior a la actual cuando se intenta publicar ésta
    @classmethod
    def set_number(cls, invoices):
        '''
        Set number to the invoice
        '''
        pool = Pool()
        Date = pool.get('ir.date')
        Lang = pool.get('ir.lang')
        Sequence = pool.get('ir.sequence.strict')
        today = Date.today()

        def accounting_date(invoice):
#            return invoice.accounting_date or invoice.invoice_date or today
            return today

        invoices = sorted(invoices, key=accounting_date)
        sequences = set()

        for invoice in invoices:
            # Posted and paid invoices are tested by check_modify so we can
            # not modify tax_identifier nor number
            if invoice.state in {'posted', 'paid'}:
                continue
            if not invoice.tax_identifier:
                invoice.tax_identifier = invoice.get_tax_identifier()
            # Generated invoice may not fill the party tax identifier
            if not invoice.party_tax_identifier:
                invoice.party_tax_identifier = invoice.party.tax_identifier

            if invoice.number:
                continue

            if not invoice.invoice_date and invoice.type == 'out':
                invoice.invoice_date = today
            invoice.number, invoice.sequence = invoice.get_next_number()
            if invoice.type == 'out' and invoice.sequence not in sequences:
                date = accounting_date(invoice)
                # Do not need to lock the table
                # because sequence.get_id is sequential
                after_invoices = cls.search([
                            ('sequence', '=', invoice.sequence),
                            ['OR',
                                ('accounting_date', '>', date),
                                [
                                    ('accounting_date', '=', None),
                                    ('invoice_date', '>', date),
                                    ],
                                ],
                            ], limit=1, order=[('accounting_date', 'DESC')])
                if after_invoices:
                    after_invoice, = after_invoices
                    raise InvoiceNumberError(
                        gettext('account_invoice.msg_invoice_number_after',
                            invoice=invoice.rec_name,
                            sequence=Sequence(invoice.sequence).rec_name,
                            date=Lang.get().strftime(date),
                            after_invoice=after_invoice.rec_name))
                sequences.add(invoice.sequence)
        cls.save(invoices)

# Se cambia esta función para que el número de factura se calcule con la fecha de publicación de la factura y no con la fecha de generación de la misma
    def get_next_number(self, pattern=None):
        "Return invoice number and sequence id used"
        pool = Pool()
        Date = pool.get('ir.date')
        Period = pool.get('account.period')

        today = Date.today()

        if pattern is None:
            pattern = {}
        else:
            pattern = pattern.copy()

#        accounting_date = self.accounting_date or self.invoice_date
        accounting_date = self.accounting_date or today
        period_id = Period.find(
            self.company.id, date=accounting_date,
            test_state=self.type != 'in')

        period = Period(period_id)
        fiscalyear = period.fiscalyear
        pattern.setdefault('company', self.company.id)
        pattern.setdefault('fiscalyear', fiscalyear.id)
        pattern.setdefault('period', period.id)

        for invoice_sequence in fiscalyear.invoice_sequences:
            if invoice_sequence.match(pattern):
                sequence = getattr(
                    invoice_sequence, self._sequence_field)
                break
        else:
            raise InvoiceNumberError(
                gettext('account_invoice.msg_invoice_no_sequence',
                    invoice=self.rec_name,
                    fiscalyear=fiscalyear.rec_name))
        with Transaction().set_context(date=accounting_date):
            return sequence.get(), sequence.id

# Se cambia esta función para que el botón de anular solo se muestre en facturas publicadas si el importe a pagar es el total de la factura

    def get_allow_cancel(self, name):
        if self.state in {'draft', 'validated'}:
            return True
        if self.state == 'posted':
            if self.type == 'in':
                return True
            elif self.company.cancel_invoice_out and\
                self.amount_to_pay == self.total_amount:
                return True
        return False

# Añadir campos funcionales con precio base, porcentaje de descuento a paciente o cobertura de seguro, e importe bruto total
class InvoiceLine(metaclass = PoolMeta):
    'Invoice Line'
    __name__ = 'account.invoice.line'

    discount_or_coverage = fields.Function(fields.Float(
        'Discount or Coverage', digits=(3, 2),
        domain=[
            ('discount_or_coverage', '>=', 0),
            ('discount_or_coverage', '<=', 100),
            ],
        states={'invisible': Eval('type') != 'line'}),
        'get_discount_or_coverage')

    base_price = fields.Numeric('Base Price', digits=price_digits,
        states={'invisible': Eval('type') != 'line'})

    base_amount = fields.Function(fields.Numeric('Base Amount',
            digits=(16, Eval('_parent_invoice', {}).get('currency_digits',
                    Eval('currency_digits', 2))),
            states={
                'invisible': Eval('type') != 'subtotal'},
            depends=['type', 'currency_digits']), 'get_base_amount')

    @fields.depends('origin', 'invoice', 'product', 'base_price', 'unit_price')
    def on_change_with_discount_or_coverage(self):
        pool = Pool()
        HealthServiceLine = pool.get('gnuhealth.health_service.line')
        if isinstance(self.origin, HealthServiceLine):
            if self.origin.name.insurance_plan.company.insurance_company_type == 'i'\
            or self.origin.name.insurance_plan.company.insurance_company_type == 'c':
                if self.base_price:
                    if self.base_price == self.unit_price:
                        coverage = 100
                    else:
                        coverage = (100 * self.unit_price)/self.base_price
                else:
                    coverage = self.origin.name.insurance_plan.plan_id.coverage
                return round(float(coverage))
            else:
                if self.base_price:
                    base_price = self.base_price
                else:
                    base_price = self.origin.product.template.list_price
                if base_price == self.unit_price:
                    discount = 0
                elif self.unit_price == 0.0:
                    discount = 100
                else:
                    discount = (base_price - self.unit_price)/base_price * 100
                return round(float(discount),4)

        return 0

    def get_discount_or_coverage(self, name):
        if self.type == 'line':
            return self.on_change_with_discount_or_coverage()

        return 0

    @fields.depends(
        'type', 'quantity', 'base_price', 'taxes_deductible_rate', 'invoice',
        '_parent_invoice.currency', 'currency', 'taxes',
        '_parent_invoice.type', 'invoice_type',
        methods=['_get_taxes'])
    def on_change_with_base_amount(self):
        if self.type == 'line':
            currency = (self.invoice.currency if self.invoice
                else self.currency)
            base_amount = (Decimal(str(self.quantity or '0.0'))
                * (self.base_price or Decimal('0.0')))
            invoice_type = (
                self.invoice.type if self.invoice else self.invoice_type)
            if (invoice_type == 'in'
                    and self.taxes_deductible_rate is not None
                    and self.taxes_deductible_rate != 1):
                with Transaction().set_context(_deductible_rate=1):
                    tax_amount = sum(
                        t['amount'] for t in self._get_taxes().values())
                non_deductible_amount = (
                    tax_amount * (1 - self.taxes_deductible_rate))
                base_amount += non_deductible_amount
            if currency:
                return currency.round(base_amount)

        return Decimal('0.0')

    def get_base_amount(self, name):
        if self.type == 'line':
            return self.on_change_with_base_amount()
        elif self.type == 'subtotal':
            subtotal = Decimal(0)
            for line2 in self.invoice.lines:
                if line2.type == 'line':
                    subtotal += line2.on_change_with_base_amount()
                elif line2.type == 'subtotal':
                    if self == line2:
                        break
                    subtotal = Decimal(0)
            return subtotal
        else:
            return Decimal(0)

    @classmethod
    def __setup__(cls):
        super().__setup__()

# Hacer obligatorio el campo "description" si el importe a pagar es negativo (devolución)
class PayInvoiceStart(metaclass = PoolMeta):
    'Pay Invoice'
    __name__ = 'account.invoice.pay.start'

    STATES = {'required': Eval('amount', 0) < 0}

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls.description.states = cls.STATES

# No se contempla más que la opción 'partial' de pago parcial por lo que
# no se permite pagar más de lo que se debe ni devolver más de lo pagado
# En la pantalla de pago, poner "Espèces" como método de pago por defecto
class PayInvoice(Wizard):
    'Pay Invoice'
    __name__ = 'account.invoice.pay'
    start = StateView('account.invoice.pay.start',
        'account_invoice.pay_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'choice', 'tryton-ok', default=True),
            ])

    choice = StateTransition()
    ask = StateView('account.invoice.pay.ask',
        'account_invoice.pay_ask_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'pay', 'tryton-ok', default=True),
            ])

    pay = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PayInvoice, cls).__setup__()
        cls.__rpc__['create'].fresh_session = True

    def get_reconcile_lines_for_amount(self, invoice, amount):
        if invoice.type == 'in':
            amount *= -1
        return invoice.get_reconcile_lines_for_amount(amount)

    def default_start(self, fields):
        default = {}
        invoice = self.record
        default['company'] = invoice.company.id
        default['currency'] = invoice.currency.id
        default['currency_digits'] = invoice.currency.digits
        default['amount'] = (invoice.amount_to_pay_today
            or invoice.amount_to_pay)
        default['invoice_account'] = invoice.account.id
        default['payment_method'] = 1 #id correspondiente al método de pago "Espèces"
        return default

    def transition_choice(self):
        pool = Pool()
        Currency = pool.get('currency.currency')

        invoice = self.record

        with Transaction().set_context(date=self.start.date):
            amount = Currency.compute(self.start.currency,
                self.start.amount, invoice.company.currency)
            amount_invoice = Currency.compute(
                self.start.currency, self.start.amount, invoice.currency)
        _, remainder = self.get_reconcile_lines_for_amount(invoice, amount)
        if (remainder == Decimal('0.0')
                and amount_invoice <= invoice.amount_to_pay):
            return 'pay'
        return 'ask'

    def default_ask(self, fields):
        pool = Pool()
        Currency = pool.get('currency.currency')

        default = {}
        invoice = self.record
        default['lines_to_pay'] = [x.id for x in invoice.lines_to_pay
                if not x.reconciliation]

        default['amount'] = self.start.amount
        default['date'] = self.start.date
        default['currency'] = self.start.currency.id
        default['currency_digits'] = self.start.currency_digits
        default['company'] = invoice.company.id

        with Transaction().set_context(date=self.start.date):
            amount = Currency.compute(self.start.currency,
                self.start.amount, invoice.company.currency)
            amount_invoice = Currency.compute(
                self.start.currency, self.start.amount, invoice.currency)

        if invoice.company.currency.is_zero(amount):
            lines = invoice.lines_to_pay
        else:
            lines, _ = self.get_reconcile_lines_for_amount(invoice, amount)
        default['lines'] = [x.id for x in lines]

        for line_id in default['lines'][:]:
            if line_id not in default['lines_to_pay']:
                default['lines'].remove(line_id)

        default['payment_lines'] = [x.id for x in invoice.payment_lines
                if not x.reconciliation]

        default['currency_writeoff'] = invoice.company.currency.id
        default['currency_digits_writeoff'] = invoice.company.currency.digits
        default['invoice'] = invoice.id

        if (amount_invoice > invoice.amount_to_pay
                or invoice.company.currency.is_zero(amount)):
            default['type'] = 'writeoff'
        return default

    def transition_pay(self):
        pool = Pool()
        Currency = pool.get('currency.currency')
        MoveLine = pool.get('account.move.line')
        Lang = pool.get('ir.lang')

        invoice = self.record

        with Transaction().set_context(date=self.start.date):
            amount = Currency.compute(self.start.currency,
                self.start.amount, invoice.company.currency)
            amount_invoice = Currency.compute(
                self.start.currency, self.start.amount, invoice.currency)

        reconcile_lines, remainder = \
            self.get_reconcile_lines_for_amount(invoice, amount)

        amount_second_currency = None
        second_currency = None
        if self.start.currency != invoice.company.currency:
            amount_second_currency = self.start.amount
            second_currency = self.start.currency

        overpayment = 0
#        if (0 <= invoice.amount_to_pay < amount_invoice
#                or amount_invoice < invoice.amount_to_pay <= 0):
        if 0 <= invoice.amount_to_pay < amount_invoice:
#            if self.ask.type == 'partial':
            lang = Lang.get()
            raise PayInvoiceError(
                gettext('account_invoice'
                    '.msg_invoice_pay_amount_greater_amount_to_pay',
                    invoice=invoice.rec_name,
                    amount_to_pay=lang.currency(
                        invoice.amount_to_pay, invoice.currency)))
#            else:
#                overpayment = amount_invoice - invoice.amount_to_pay

        if amount_invoice < 0:
            amount_payed = invoice.total_amount - invoice.amount_to_pay
            if abs(amount_invoice) > amount_payed:
#                if self.ask.type == 'partial':
                lang = Lang.get()
                raise PayInvoiceError(
                    gettext('health_ebome.msg_invoice_payback_amount_greater_amount_payed',
                        invoice=invoice.rec_name,
                    amount_to_backpay=lang.currency(
                        amount_payed, invoice.currency)))

        lines = []
        if not invoice.company.currency.is_zero(amount):
            lines = invoice.pay_invoice(amount,
                self.start.payment_method, self.start.date,
                self.start.description, amount_second_currency,
                second_currency, overpayment)

        if remainder:
            if self.ask.type != 'partial':
                to_reconcile = {l for l in self.ask.lines}
                to_reconcile.update(
                    l for l in invoice.payment_lines
                    if not l.reconciliation)
                if self.ask.type == 'writeoff':
                    to_reconcile.update(lines)
                if to_reconcile:
                    MoveLine.reconcile(
                        to_reconcile,
                        writeoff=self.ask.writeoff,
                        date=self.start.date)
        else:
            reconcile_lines += lines
            if reconcile_lines:
                MoveLine.reconcile(reconcile_lines)
        return 'end'
