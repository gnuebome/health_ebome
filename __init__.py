from trytond.pool import Pool
from . import health_ebome
from .report import *
from .wizard import *

def register():
    Pool.register(
        health_ebome.Pathology,
        health_ebome.PatientEvaluation,
        health_ebome.PatientData,
        health_ebome.ImagingTest,
        health_ebome.ImagingTestRequest,
        health_ebome.ImagingTestResult,
        health_ebome.Lab,
        health_ebome.TestType,
        health_ebome.GnuHealthTestCritearea,
        health_ebome.GnuHealthPatientLabTest,
        health_ebome.PatientCreateManualStart,
        health_ebome.Surgery,
        health_ebome.Party,
        health_ebome.Insurance,
        health_ebome.InsurancePlan,
        health_ebome.InsurancePlanProductPolicy,
        health_ebome.Appointment,
        health_ebome.ProcedureCode,
        health_ebome.InpatientRegistration,
        health_ebome.Template,
        health_ebome.Product,
        health_ebome.HospitalWard,
        health_ebome.HospitalBed,
        health_ebome.CreateBedTransferInit,
        health_ebome.HealthService,
        health_ebome.HealthServiceLine,
        health_ebome.Company,
        health_ebome.Invoice,
        health_ebome.InvoiceLine,
        health_ebome.PayInvoiceStart,
        PatientUpdateEbomeStart,
        CreatePatientEvaluationReportEbomeStart,
        RequestPatientLabTestStart,
        RequestPatientImagingTestStart,
        CreateInvoiceReportEbomeStart,
        CreateInvoicePerTarifTypeStart,
        CreateInvoiceForInsurancesStart,
        CreateJournalEbomeStart,
        CreateDebtorsReportEbomeStart,
        CreateDebtPaymentsReportEbomeStart,
        CreatePricesPerTarifTypeStart,
        CreatePricesForInsurancesStart,
        CreateConsultationPaludismReportEbomeStart,
        OpenInsuranceInvoicesStart,
        CreateInvoiceSummaryStart,
        module='health_ebome', type_='model')
    Pool.register(
        CreatePatientEvaluationReportEbome,
        CreateInvoiceReportEbome,
        CreateInvoicePerTarifType,
        CreateInvoiceForInsurances,
        CreateJournalEbome,
        CreateDebtorsReportEbome,
        CreateDebtPaymentsReportEbome,
        CreatePricesPerTarifType,
        CreatePricesForInsurances,
        CreateConsultationPaludismReportEbome,
        health_ebome.PatientCreateManual,
        PatientUpdateEbome,
        CreateAppointmentEvaluation,
        OpenAppointmentEvaluations,
        OpenAppointmentLabResults,
        OpenAppointmentImgResults,
        CreatePatientHospitalisation,
        CreateEvalPatientHospitalisation,
        OpenSurgeryLabResults,
        OpenSurgeryImgResults,
        OpenInpatientLabResults,
        OpenInpatientImgResults,
        CreateLabTestOrder,
        RequestPatientLabTest,
        WizardGenerateResult,
        RequestPatientImagingTest,
        RequestInpatientLabTest,
        RequestInpatientImagingTest,
        OpenInpatientEvaluations,
        CreateInpatientEvaluation,
        health_ebome.CreateBedTransfer,
        CreateInpatientReportEbome,
        health_ebome.PayInvoice,
        InvoiceTheService,
        InvoiceTheServiceForInsurance,
        OpenInsuranceInvoices,
        CreateInvoiceSummary,
        SetPricesProduct,
        SetPricesTemplate,
        module='health_ebome', type_='wizard')
    Pool.register(
        module='health_ebome', type_='report')
